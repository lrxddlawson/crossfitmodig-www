<?php
    get_header();
 //   the_post();
?>
        <!-- begin Content -->
    <section id="single-trainer" >
    <?php if(of_get_option('show_banner_single_trainer')) { ?>
        <!-- begin Sub Header -->
        <div class="sub-header" style="<?php echo 'background:url('.of_get_option('trainer_banner_single_image').') no-repeat;'?>">
        
            <div class="container">
            
                <div class="row" >
                
                    <ul class="sub-header-container" >
                    
                        <li>
                        
                            <h3 <?php echo colors('h3');?> class="title"><?php echo of_get_option('trainer_banner_single_text'); ?></h3>
                            
                        </li>
                        
                        <li>
                            <?php if (of_get_option('show_breadcumbs_single_trainer')) { ?>
                            <ul class="custom-breadcrumb" >
                            
                                <li><h6 <?php echo colors('h6');?>><a <?php echo colors('a');?> href="<?php echo home_url();?>tml">Home</a></h6></li>
                                
                                <li><i class="separator entypo-play" ></i></li>
                                
                                <li><h6 <?php echo colors('h6');?>><a <?php echo colors('a');?> href="#">Trainers</a></h6></li>
                                
                                <li><i class="separator entypo-play" ></i></li>
                                
                                <li><h6 <?php echo colors('h6');?>>Single Trainer</h6></li>
                                
                            </ul> 
                            <?php }?>                   
                        </li>
                        
                    </ul>
                    
                </div>
                
            </div>
            
        </div>
        <!-- end Sub Header -->
     <?php }?>   
        <!-- begin Trainer Profile -->
        <article class="article-container">
        
            <div class="container" >
            
                <div class="row" >
                
                    <div class="col-md-12">
                    
                        <h2 <?php echo colors('h2');?> class="article-title" ><?php echo get_the_title();?></h2>    
                                  
                        <span <?php echo colors('h1s');?> class="line" >
                        
                            <span <?php echo colors('h1s');?> class="sub-line" ></span>
                            
                        </span>
                    
                    </div>
                    
                </div>
                <?php
                    $value = get_field('show_figure');
                    if($value[0] == '1'){     
                ?>
                <div class="row">
                
                    <div class="col-sm-5 col-md-3">
                    
                        <figure>
                        
                            <img src="<?php echo get_field('photo');?>" alt="image" >
                            
                        </figure>
                        
                        <div class="trainer-photo" >
                        
                            <h3 <?php echo colors('h3');?>><?php echo get_field('name');?></h3>
                            
                        </div>
                        
                    </div>
                    
                    <div class="sol-sm-7 col-md-5">
                    
                        <h2 <?php echo colors('h2');?>><?php echo get_field('name');?> <?php echo get_field('title_biografy');?></h2>
                        
                        <p <?php echo colors('p');?>><?php echo get_field('biografy');?></p>
                    
                    </div>
                    
                    <div class="col-md-4">
                    
                        <h2 <?php echo colors('h2');?>><?php echo get_field('opinion_title');?></h2>
                        
                        <div class="testimonial-container" >
                            <p <?php echo colors('p');?>><?php echo get_field('opinion');?></p>
                            <span <?php echo colors('h1s');?> class="border">
                                <span <?php echo colors('h1s');?> class="back" ></span>
                            </span>
                        </div>
                        <h4 <?php echo colors('h4');?> class="testimonials-name" ><?php echo get_field('name');?></h4>
                        <h6 <?php echo colors('h6');?> class="testimonials-position" ><?php echo get_field('specialist');?></h6>
                    </div>    
                </div>
               <?php
                }           
               ?>     
                
                <div class="row">
                <?php
                    $value = get_field('show_trainer_experience');
                   if($value[0] == '1'){      
                ?>
                    <div class="col-md-8">
                        <h2 <?php echo colors('h2');?>><?php echo get_field('class_experience_title');?></h2>
                        
                        <div class="row class-experience">
                        <?php
                             foreach(get_field('class_experience') as $key){
                        ?>
                            <div class="col-sm-6 col-md-3">
                                <div class="chart" data-percent="<?php echo $key['percent'];?>"><?php echo $key['percent'];?>%</div>
                                <h3 <?php echo colors('h3');?>><?php echo $key['class_title'];?></h3>
                                <p <?php echo colors('p');?>><?php echo $key['class_description'];?></p>
                            </div>
                         <?php
                             }
                         ?>                            
                        </div>
                    </div>
                <?php
                     }
                 ?>
                 <?php
                     $value = get_field('show_other_experiences');
                     if($value[0] == '1'){ 
                  ?>
                    <div class="col-md-4">
                        <h2 <?php echo colors('h2');?> class="headers" >Progress Bar</h2>
                        <?php
                             foreach(get_field('progres_bar') as $key){
                        ?>
                            <div class="progress">
                                <div class="bar" style="width: <?php echo $key['percent'];?>%;">
                                <?php echo $key['percent'];?>% - <?php echo $key['title'];?>
                                </div>
                            </div>
                        <?php
                             }
                         ?>
                           
                    </div>    
                  </div>
                  <?php
                     }
                 ?>
                <div class="row">
                <?php
                    $value = get_field('show_grafic');
                     if($value[0] == '1'){ 
                  ?>
                    <div class="col-md-8">
                    <?php
                        $pt_features = get_field('points_features');
                        if(isset($pt_features)){
                           foreach(get_field('points_features') as $key){
                                echo '<div id="linear-chart-ptfeatures" style="display:none;" ptext="'.$key['point_text'].'" pinicial="'.$key['point_ini'].'" psalto="'.$key['point_step'].'" pcant="'.$key['point_count'].'"></div>';
                            }
                        }
                        $lines = get_field('lines');
                        $c=0;
                        if(isset($lines)){
                            foreach(get_field('lines') as $key){
                                $c++;
                                echo '<input id="linear-chart-point'.$c.'" class="linear-chart-point" type="hidden" green="'.$key['green_point'].'" orange="'.$key['orange_point'].'">';                    
                            }                             
                        }
                    ?>
                        <canvas id="linear-chart" width="750" height="250"></canvas>
                    </div>
                <?php
                     }
                  ?>
                 <?php
                    $value = get_field('show_pay');
                     if($value[0] == '1'){ 
                  ?>  
                    <div class="col-md-4">
                        <div class="row ">
                            <?php
                                $c = 0;
                                foreach(get_field('pay') as $key){
                                    $c++;
                             ?>   
                            <div class="col-xs-6 col-sm-3 col-sms-offset-2  col-md-6">
                                <canvas id="pie-chart<?php echo $c;?>" width="170" height="170" percent="<?php echo $key['pay_percent'];?>"></canvas>
                                <p <?php echo colors('p');?>><?php echo $key['pay_description'];?></p>
                            </div>
                            <?php
                                }
                             ?>                            
                        </div>
                    </div>
                  <?php
                     }
                  ?>    
                </div>
            </div>
            
        </article>
        <!-- end Trainer Profile -->


    </section>

<?php 
    get_footer();
?>