<?php
    get_header();
    the_post();
    if(of_get_option('show_contact1') == '1'){
?>
    <!-- begin Content -->
    <section id="contact" >
        <!-- begin Sub Header -->
    <?php if(of_get_option('show_banner_contact')){?>
        <div class="sub-header" style="<?php echo 'background:url('.of_get_option('contact_banner_image').') no-repeat;'?>">
            <div class="container">
                <div class="row" >
                    <ul class="sub-header-container" >
                        <li>
                            <h3 <?php echo colors('h3');?> class="title"><?php echo of_get_option('contact_banner_text') ?></h3>
                        </li>
                        <li>
                        <?php if (of_get_option('show_breadcumbs_contact')) { ?>
                            <ul class="custom-breadcrumb" >
                                <li><h6 <?php echo colors('h6');?>><a <?php echo colors('a');?> href="<?php echo home_url();?>">Home</a></h6></li>
                                <li><i class="separator entypo-play" ></i></li>
                                <li><h6 <?php echo colors('h6');?>>Contact</h6></li>
                            </ul>                    
                        <?php }?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    <?php }?>
        <!-- end Sub Header -->

        <!-- begin Contanct -->
        <article class="article-container">
            <div class="container" >
                       
               
                <div class="row" >
                    <div class="col-md-12">
                    <h2 <?php echo colors('h2');?> class="article-title" ><?php echo of_get_option('contact_title');?></h2>
                    <span <?php echo colors('h1s');?> class="line" >
                        <span <?php echo colors('h1s');?> class="sub-line" ></span>
                    </span>
                    </div>
                </div>

                <div class="row">
                
                   
                             
                      <div class="col-md-7">
                         
                          <div class="entry-content">

							<!-- START OF WODIFY INTEGRATION -->

                            <script type='text/javascript'>
                            function validateWodifyLeadForm()
                            {
                            var reEmail = /^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$/;
                            var returnValue = true;

                            var fieldname = document.forms['wodifyLeadForm']['name'].value;
                            if(fieldname == null || fieldname == '')
                            {
                            document.getElementById('errorname').style.display = 'inline';
                            returnValue = false;
                            }
                            else
                            {
                            document.getElementById('errorname').style.display = 'none';
                            }



                            var fieldemail = document.forms['wodifyLeadForm']['email'].value;
                            if(fieldemail == null || fieldemail == '' || !reEmail.test(fieldemail))
                            {
                            document.getElementById('erroremail').style.display = 'inline';
                            returnValue = false;
                            }
                            else
                            {
                            document.getElementById('erroremail').style.display = 'none';
                            }



                            var fieldgender = document.forms['wodifyLeadForm']['gender'].options[document.forms['wodifyLeadForm']['gender'].selectedIndex].value;
                            if(fieldgender == null || fieldgender == '')
                            {
                            document.getElementById('errorgender').style.display = 'inline';
                            returnValue = false;
                            }
                            else
                            {
                            document.getElementById('errorgender').style.display = 'none';
                            }



                            var fieldphone = document.forms['wodifyLeadForm']['phone'].value;
                            if(fieldphone == null || fieldphone == '')
                            {
                            document.getElementById('errorphone').style.display = 'inline';
                            returnValue = false;
                            }
                            else
                            {
                            document.getElementById('errorphone').style.display = 'none';
                            }



                            var validationUrl = '//app.wodify.com/API/WebToLeadCaptchaVerificationEntry.aspx?ChallengeField=' + document.getElementById('recaptcha_challenge_field').value + String.fromCharCode(38) + 'ResponseField=' + document.getElementById('recaptcha_response_field').value;

                            var xmlhttp = new XMLHttpRequest();
                            xmlhttp.open('GET', validationUrl, false);
                            xmlhttp.send();
                            var captchaResult = JSON.parse(xmlhttp.responseText);
                            if (captchaResult.result)
                            {
                            document.getElementById('errorCAPTCHA').style.display = 'none';
                            document.getElementById('usecaptcha').value = 'false';
                            }
                            else
                            {
                            document.getElementById('errorCAPTCHA').style.display = 'inline';
                            document.getElementById('usecaptcha').value = 'true';
                            returnValue = false;
                            }


                            return returnValue;
                            };
                            </script>


                            <form action="https://app.wodify.com/API/WebToLeadFormPostEntry_v1.aspx?" method="POST" id="wodifyLeadForm" name="wodifyLeadForm" onsubmit="return validateWodifyLeadForm();">
                            <label for="name">Name</label><br /><input type="text" name="name" maxlength="50"></input> *<span id="errorname" name="errorname" style="display: none; color: #cc2626;"> Required field!</span><br />
                            <label for="email">Email</label><br /><input type="email" name="email" maxlength="250"></input> *<span id="erroremail" name="erroremail" style="display: none; color: #cc2626;"> Required field!</span><br />
                            <label for="gender">Gender</label><br /><select name="gender">
                            <option value="">-</option>
                            <option value="1">Female</option>
                            <option value="2">Male</option>
                            </select> *<span id="errorgender" name="errorgender" style="display: none; color: #cc2626;"> Required field!</span><br />
                            <label for="phone">Phone</label><br /><input type="text" name="phone" maxlength="20"></input> *<span id="errorphone" name="errorphone" style="display: none; color: #cc2626;"> Required field!</span><br />
                            <label for="city">City</label><br /><input type="text" name="city" maxlength="50"></input><br />
                            <label for="state">State</label><br /><select name="state">
                            <option value="">-</option>
                            <option value="24">Alaska</option>
                            <option value="22">Alabama</option>
                            <option value="20">Arkansas</option>
                            <option value="11">Arizona</option>
                            <option value="21">California</option>
                            <option value="32">Colorado</option>
                            <option value="12">Connecticut</option>
                            <option value="44">District of Columbia</option>
                            <option value="9">Delaware</option>
                            <option value="39">Florida</option>
                            <option value="34">Georgia</option>
                            <option value="19">Hawaii</option>
                            <option value="49">Iowa</option>
                            <option value="40">Idaho</option>
                            <option value="23">Illinois</option>
                            <option value="5">Indiana</option>
                            <option value="25">Kansas</option>
                            <option value="30">Kentucky</option>
                            <option value="48">Louisiana</option>
                            <option value="6">Massachusetts</option>
                            <option value="1">Maryland</option>
                            <option value="50">Maine</option>
                            <option value="33">Michigan</option>
                            <option value="27">Minnesota</option>
                            <option value="2">Missouri</option>
                            <option value="36">Mississippi</option>
                            <option value="18">Montana</option>
                            <option value="8">North Carolina</option>
                            <option value="29">North Dakota</option>
                            <option value="35">Nebraska</option>
                            <option value="31">New Hampshire</option>
                            <option value="38">New Jersey</option>
                            <option value="43">New Mexico</option>
                            <option value="10">Nevada</option>
                            <option value="3">New York</option>
                            <option value="47">Ohio</option>
                            <option value="42">Oklahoma</option>
                            <option value="28">Oregon</option>
                            <option value="51">Pennsylvania</option>
                            <option value="13">Rhode Island</option>
                            <option value="45">South Carolina</option>
                            <option value="17">South Dakota</option>
                            <option value="37">Tennessee</option>
                            <option value="15">Texas</option>
                            <option value="41">Utah</option>
                            <option value="16">Virginia</option>
                            <option value="4">Vermont</option>
                            <option value="7">Washington</option>
                            <option value="26">Wisconsin</option>
                            <option value="46">West Virginia</option>
                            <option value="14">Wyoming</option>
                            </select><br />
                            <label for="subscribe">Subscribe to SMS</label><br /><input type="checkbox" name="subscribe" value="True"><br />
                            <label for="leadsource">How Did You Hear About Us</label><br /><select name="leadsource">
                            <option value="">-</option>
                            <option value="33736">Friend/Family</option>
                            <option value="33737">Facebook</option>
                            <option value="33738">Google/Search Engine</option>
                            <option value="45498">SMS</option>
                            <option value="52690">Online Sales</option>
                            <option value="33739">Other</option>
                            </select><br />
                            <label for="referredby">Referred By</label><br /><input type="text" name="referredby" maxlength="1000"></input><br />
                            <label for="comments">Comments</label><br /><textarea name="comments" maxlength="2000" rows="4" cols="50"></textarea><br />
                            <script type="text/javascript"
                                src="http://www.google.com/recaptcha/api/challenge?k=6Lc59ucSAAAAAEd8XfTJJBULRcUyfPC6_DvtkXFo">
                            </script>
                            <noscript>
                                <iframe src="http://www.google.com/recaptcha/api/noscript?k=6Lc59ucSAAAAAEd8XfTJJBULRcUyfPC6_DvtkXFo" height="300" width="500" frameborder="0"></iframe><br>
                                <textarea name="recaptcha_challenge_field" rows="3" cols="40">
                                </textarea>
                                <input type="hidden" name="recaptcha_response_field" value="manual_challenge">
                            </noscript>
                            <span id="errorCAPTCHA" name="errorCAPTCHA" style="display: none; color: #cc2626;"> Incorrect answer!</span><input type="hidden" id="usecaptcha" name="usecaptcha" value="true"></input><br />
                            <input type="hidden" name="apikey" value="1loc806rku">
                            <input type="submit" value="Submit"></form>

                            <div>
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3075.733822418467!2d-104.90711399999999!3d39.565608999999995!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x876c84470a5ddc63%3A0x5fb2cee755be4755!2sCrossFit+MODIG!5e0!3m2!1sen!2sus!4v1409067238031" width="600" height="450" frameborder="0" style="border:0"></iframe>
                            </div>

                            <div><?php the_content(); ?></div>

					        </div><!-- .entry-content -->
                        
                    </div>
                    
  
                    <!-- YOU MIGHT BE ABLE TO REUSE THESE STYLES -->             
                  <!--   <div class="col-md-5 contact" >
                        <form id="form" action="<?php echo get_template_directory_uri();?>/send_mail.php">
                          <div class="form-group">
                            <label for="contact_name"><i <?php echo font_awesome_icon_style('name_icon');?> ></i> <?php echo of_get_option('contact_form_name');?> <em>(required)</em></label>
                            <input type="text" name="name" class="form-control" id="contact_name">
                          </div>
                          <div class="form-group">
                            <label for="contact_e-mail"><i <?php echo font_awesome_icon_style('mail_icon');?> ></i> <?php echo of_get_option('contact_form_mail');?> <em>(required)</em></label>
                            <input type="email" name="email" class="form-control" id="contact_e-mail">
                          </div>
                          <div class="form-group">
                            <label for="contact_subject"><i <?php echo font_awesome_icon_style('subject_icon');?> ></i> <?php echo of_get_option('contact_form_subject');?> <em>(required)</em></label>
                            <input type="text" name="subject" class="form-control" id="contact_subject">
                          </div>
                          <div class="form-group">
                            <label for="contact_message"><i <?php echo font_awesome_icon_style('msg_icon');?> ></i> <?php echo of_get_option('contact_form_msg');?></label>
                            <textarea id="contact_message" name="message" class="form-control" rows="6"></textarea>
                          </div>
                          <div id="result"></div>
                          <input name="from" type="hidden" value="<?php echo of_get_option('main_mail');?>">
                          <button type="submit" name="Submit" class="btn btn-default"><?php echo of_get_option('contact_form_btn');?></button>
                        </form>
                        
                    </div> -->
                    
                    
                    
                </div>
                
             
              
            </div>
        </article>
        <!-- end Contact -->


        
    </section>
    <!-- end Content -->
<?php
    }else{
        echo get_the_content();
    }
    get_footer(); ?>