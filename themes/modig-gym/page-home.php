<?php 
    get_header();
    the_post();
    if(of_get_option('show_home')){
?>
    <section id="home" >
        <div class="sub-header" >
            <!-- Slider -->
            <?php if(of_get_option('show_slider_opt')){?>
            <div class="bannercontainer">
                <div class="banner ">
                    <?php putRevSlider(of_get_option('slider_opt')); ?>
                </div>
            </div>
            <?php }?>
            <!-- End Slider -->
        </div>
    <?php if(of_get_option('show_home_z1')){?>
        <article class="article-container" id="intro">
			<div class="container">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<h2 <?php echo colors('h2');?>><strong><?php echo of_get_option('home_z1_title');?></strong></h2>
					<p <?php echo colors('p');?>><?php echo insert_br(of_get_option('home_z1_desc'));?></p>
					<a <?php echo colors('a');?> href="<?php echo of_get_option('home_z1_btn_link');?>"><button class="button-gym big-button"><?php echo of_get_option('home_z1_btn');?></button></a>
				</div>
			</div>
			</div>
		</article>
		<!-- end Intro -->
    <?php }?>
    <?php
         if(of_get_option('show_home_z2')){
                $args=array(
                    'post_type' => of_get_option('home_z2_galleries'),
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                );
                $my_query = null;
                $my_query = new WP_Query($args);
                $gallery = array();
                
                while ($my_query->have_posts()) {
                    $my_query->the_post();
                    if(of_get_option('home_z2_galleries') == 'classes'){
                        $gallery[] = array(
                            'image' => get_field('previus_image'),
                            'title' => get_the_title(),
                            'desc' => get_field('content'),
                            'link' => get_permalink()
                        );
                    }else{
                        $gallery[] = array(
                            'image' => get_field('photo'),
                            'title' => get_field('name'),
                            'desc' => get_field('fast_description'),
                            'link' => get_permalink()
                        );
                    }
                }
     ?>
		<!-- begin new classes -->
		<article class="article-container">
			<div class="container" >
				<div class="row" >
					<div class="col-md-12">

					<h2 <?php echo colors('h2');?> class="article-title" ><?php if(of_get_option('home_z2_galleries') == 'classes'){echo 'New Classes';}else{echo 'New Trainers';}?></h2>
					<span <?php echo colors('h1s');?> class="line" >
						<span <?php echo colors('h1s');?> class="sub-line" ></span>
					</span>
					<a <?php echo colors('a');?> class="slider-control pull-right next" href="#new-classes" data-slide="next"></a>
					<a <?php echo colors('a');?> class="slider-control pull-right prev" href="#new-classes" data-slide="prev"></a>
					<a <?php echo colors('a');?> class="button-gym normal-button view-all pull-right" href="<?php echo of_get_option('home_z2_btn_link');?>" ><?php echo of_get_option('home_z2_btn');?></a>
					</div>
				</div>

				<div class="row">
					<div class="new-classes" >
					<div id="new-classes" class="carousel slide">
					<!-- Wrapper for slides -->
					    <div class="carousel-inner">
                        <?php
                            for($i=0; $i<count($gallery); $i++){
                                if(($i+1)%5 == 0 || $i == 0){
                                    echo '<div class="item ';
                                    if($i==0){echo 'active';}
                                    echo '">';
                                }
                        ?>
							    <div class="col-sm-6 col-md-3" >
								    <div class="new-class" >
									    <img src="<?php echo $gallery[$i]['image']; ?>" alt="//" />
									    <div class="class-title" >
										    <div class="occult" >
                                                <a <?php echo colors('a');?> href="<?php echo $gallery[$i]['link']; ?>" class="link" ></a>
                                            </div>
										    <h3 <?php echo colors('h3');?>><?php echo $gallery[$i]['title']; ?></h3>
										    <p <?php echo colors('p');?> class="occult" ><?php echo wp_trim_words(insert_br($gallery[$i]['desc']),10,'...'); ?></p>
									    </div>
								    </div>
							    </div>
					    <?php
                                    if(($i+1)%4 == 0){echo '</div>';}
                                }          
                        ?>			
					    </div>
					</div>

					</div>

				</div>
			</div>
		</article>
		<!-- end new classes --> 
     <?php }?>
     <?php
         if(of_get_option('home_z2_features')){
     ?>
		<!-- begin Featured Classes -->
		<article class="article-container">
			<div class="container" >
				<div class="row" >
					<div class="col-xs-12">
					<h2 <?php echo colors('h2');?> class="headers"><?php echo of_get_option('features_class_title');?></h2>
					<span <?php echo colors('h1s');?> class="line" >
						<span <?php echo colors('h1s');?> class="sub-line" ></span>
					</span>
					<a <?php echo colors('a');?> class="button-gym normal-button view-all pull-right no-margin" href="<?php echo of_get_option('features_class_btn_url');?>" ><?php echo of_get_option('features_class_btn_link');?></a>
					</div>
				</div>
				<?php
                     for($i=1; $i<=of_get_option('features_class_num'); $i++){
                         if($i==1 || $i%4 == 0){echo '<div class="row" >';}
                ?>
					<div class="col-xs-3 col-sm-2 col-md-1" >
						<img src="<?php echo of_get_option('features_img'.$i);?>" alt="//" />
					</div>
					<div class="col-xs-9 col-sm-10 col-md-3 fc-container" >
						 <h3 <?php echo colors('h3');?>><?php echo of_get_option('features_title'.$i);?></h3>
                        <p <?php echo colors('p');?>><?php echo insert_br(of_get_option('features_desc'.$i));?></p>
                        <a <?php echo colors('a');?> href="<?php echo of_get_option('features_btn_link'.$i);?>"><?php echo of_get_option('features_btn'.$i);?>...</a>
					</div>
				<?php
                        if($i==of_get_option('features_class_num') || $i%3 == 0){echo '</div>';}
                     }
                ?>	
			  </div>
		</article>  
		<!-- end Featured Classes -->
     <?php }?>
	 
		<!-- begin No pressure, no diamonds  / The Club Featured -->
        <article class="article-container">
            <div class="container" >
                <div class="row">
                <?php if(of_get_option('show_home_z3')){ ?>
                    <div class="col-md-7">
                        <h2 <?php echo colors('h2');?> class="headers"><?php echo of_get_option('home_z3_title');?></h2>
                        <span <?php echo colors('h1s');?> class="line" >
                            <span <?php echo colors('h1s');?> class="sub-line" ></span>
                        </span>
                        <div class="tabbable tabs-left">
                            <ul class="nav nav-tabs col-xs-4"><!--  nav-tabs-sec" id="myTab"  nav-tabs-sec -->
                            <?php
                                 for($i=1; $i<=of_get_option('home_z3_num'); $i++){
                             ?>
                                <li class="<?php if($i==1){echo 'active';}?>">
                                <a <?php echo colors('a');?> href="#<?php echo 'sec'.$i;?>" data-toggle="tab">
                                
                                <div class="number<?php echo $i;?>">
                                    <i <?php echo font_awesome_icon_style('home_z3_ico'.$i);?>></i>
                                    <!--<span class="gym-icon icon" style="background: url('<?php //echo of_get_option('home_z3_img'.$i);?>');"></span>-->
                                </div>
                                        <?php echo of_get_option('home_z3_title'.$i);?>
                                    
                                </a>
                                </li>
                             <?php }?>
                            </ul>
                                 
                        <div class="tab-content col-xs-8">
                        <?php
                            for($i=1; $i<=of_get_option('home_z3_num'); $i++){ 
                         ?>
                            <div class="tab-pane in <?php if($i==1){echo 'active';}?>" id="<?php echo 'sec'.$i;?>">
                                <p <?php echo colors('p');?>><?php echo insert_br(of_get_option('home_z3_desc'.$i));?></p>
                            </div>
                         <?php }?>
                            
                        </div> 

                        </div>
                    </div>
                 <?php }?>
                <?php if(of_get_option('show_home_z4')){ ?>
                    <div class="col-md-5">
                        <h2 <?php echo colors('h2');?> class="headers"><?php echo of_get_option('home_z4_title');?></h2>
                        <span <?php echo colors('h1s');?> class="line" >
                            <span <?php echo colors('h1s');?> class="sub-line" ></span>
                        </span>
                        
                        <!-- begin Accordion -->
                        <div class="accordion" id="accordion2">
                        <?php         
                            $c = 0;
                            $numbers = array(1=>'One','Two','Three','Four','Five','Six','Seven','Height','Nine','Ten','Eleven','Twelve','Thirteen','Twenty','Thirty','Fourty');
                             for($i=1; $i<=of_get_option('home_z4_num'); $i++){
                                 $c++;
                        ?>
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <h5 <?php echo colors('h5');?>><?php echo $c.'. '.of_get_option('home_z4_title'.$i);?>?</h5>
                                    <a <?php echo colors('a');?> class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse<?php echo $numbers[$c];?>">+</a>
                                </div>
                                <div id="collapse<?php echo $numbers[$c];?>" class="accordion-body collapse <?php if($c==1){echo 'in';}?>">
                                    <div class="accordion-inner">
                                        <a <?php echo colors('a');?> class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse<?php echo $numbers[$c];?>">-</a>
                                        <h5 <?php echo colors('h5');?>><?php echo of_get_option('home_z4_subtitle'.$i);?></h5>
                                        <p <?php echo colors('p');?>><?php echo insert_br(of_get_option('home_z4_desc'.$i));?></p>
                                    </div>
                                </div>
                            </div>
                        <?php }?>
                        </div>
                    </div>
                  <?php }?>
                </div>
            </div>
        </article>
		<!-- end No pressure, no diamonds  / The Club Featured -->
     
     <?php
         if(of_get_option('show_home_z2')){
     ?>
        <!-- Table -->
        <article class="article-container">      
            <div class="container" >
            
                <div class="row" >
                  <div class="col-xs-12">
                  <h2 <?php echo colors('h2');?> class="headers"><?php echo of_get_option('home_z5_title');?></h2>
                  <span <?php echo colors('h1s');?> class="line" >
                      <span <?php echo colors('h1s');?> class="sub-line" ></span>
                  </span>
                  </div>
                </div>
                            
                <div class="row" >
                	<div class="col-xs-12">
                		<div class="table-responsive">
                        <table class="table table-hover table-bordered">
                          <thead>
                            <tr>
                            <?php for($i=1; $i<=of_get_option('home_z5_numc'); $i++){?>
                              <th width="<?php echo of_get_option('home_z5_cwidth'.$i).'px';?>"><?php echo of_get_option('home_z5_ctitle'.$i);?></th>
                            <?php }?>
                            </tr>
                          </thead>
                          <tbody>
                            <?php for($i=1; $i<=of_get_option('home_z5_numc'); $i++){?>
                            <tr>
                                <?php for($j=1; $j<=of_get_option('home_z5_numr'); $j++){?>
                              <td  style="background-color: <?php echo of_get_option('home_z5_field_color'.$i.$j);?>;" width="<?php echo of_get_option('home_z5_cwidth'.$i).'px';?>">
                                <?php echo '<a '.colors('a').' href="'.of_get_option('home_z5_field_link'.$i.$j).'">';?>
                                <?php echo of_get_option('home_z5_field'.$i.$j);?>
                                <?php echo '</a>';?>
                              </td>
                                <?php }?>
                            </tr>
                            <?php }?>
                          </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>            
        </article>  
        <!-- end Table -->
     <?php }?>
    <?php if(of_get_option('show_home_sponsor')) { ?>
        <article class="article-container">
            <div class="container" >

                <!-- arrows -->
                <div class="row" >
                    <div class="col-md-12">
                        <h2 <?php echo colors('h2');?> class="headers"><?php echo of_get_option('sponsor_title'); ?> </h2>
                        <span <?php echo colors('h1s');?> class="line" >
                            <span <?php echo colors('h1s');?> class="sub-line" ></span>
                        </span>
                        <?php if(of_get_option('sponsor_cant')>5) { ?>
                        <a <?php echo colors('a');?> class="slider-control pull-right next" href="#bodybuilding" data-slide="next"></a>
                        <a <?php echo colors('a');?> class="slider-control pull-right prev" href="#bodybuilding" data-slide="prev"></a>
                        <?php } ?>
                    </div>
                </div>
                <!-- end arrows -->

                <div class="row" >
                    <div id="bodybuilding" class="carousel slide">
                    <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <?php
                                $begin1='<div class="item active"><ul class="logos" >';
                                $last1 = '</ul></div>';
                                $begin2='<div class="item"><ul class="logos" >';
                                for ($log=1; $log <= of_get_option('sponsor_cant') ; $log++) {

                                    if($log!=1){
                                        $begin1 = '';
                                    }
                                    echo $begin1;
                            ?>
                            <li>
                                <a <?php echo colors('a');?> href="<?php echo of_get_option('sponsor_link'.$log);  ?>">
                                    <img src="<?php echo of_get_option('sponsor_text'.$log); ?>" alt="<?php echo $log;?>" />
                                </a>
                            </li>
                            <?php
                                    $c=$log%5;
                                    if($c==0){
                                        echo $last1;
                                        echo $begin2;
                                    }
                                } 
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </article>
        <?php } ?>
	</section>

	<!-- end content -->
<?php 
    }else{
        echo get_the_content();
    }
    get_footer();
?>