<?php get_header() ?>

<!-- begin Content -->
	
<section id="single-news" >
	
	<div id="primary" class="content-area">
	
        <!-- begin Sub Header -->
		
       <?php if(of_get_option('show_banner_post')){?>
	   
        <div class="sub-header" style="<?php echo 'background:url('.of_get_option('post_banner_image').') no-repeat;'?>">
		
            <div class="container">
			
                <div class="row" >
				
                    <ul class="sub-header-container" >
					
                        <li>
						
                            <h3 <?php echo colors('h3');?> class="title"><?php echo of_get_option('post_banner_text') ?></h3>
							
                        </li>
						
                        <li>
						
                            <ul class="custom-breadcrumb" >
							
                                <li><h6 <?php echo colors('h6');?>><a <?php echo colors('a');?> href="<?php echo home_url();?>">Home</a></h6></li>
								 
                                <li><i class="separator entypo-play" ></i></li>
								
                                <li><h6 <?php echo colors('h6');?> <?php echo colors('h6');?>>News</h6></li>

                                <li><i class="separator entypo-play" ></i></li>

                                <li><h6 <?php echo colors('h6');?>>Single News</h6></li>
								
                            </ul>
							
                        </li>
						
                    </ul>
					
                </div>
				
            </div>
			
        </div>
		
        <?php } ?> <!-- begin Sub Header -->

        <!-- begin News -->
		
        <article class="article-container">
		
            <div class="container" >
			
                <div class="row" >
				
                    <?php if(of_get_option('layer_img_single') == 'left'){?>
				
                    <aside class="col-sm-3 sidebar">
					
                        <?php get_sidebar() ?>
						
                    </aside>
					
                    <?php }?>
					
                    <div class="<?php if(of_get_option('layer_img_single') == 'none'){echo 'col-sm-12';}else{echo 'col-sm-9';}?>">
					
                        <h2 <?php echo colors('h2');?> class="article-title" ><?php echo of_get_option('blog_title')?></h2>
						
                        <span <?php echo colors('h1s');?> class="line" >
						
                            <span <?php echo colors('h1s');?> class="sub-line" ></span>
							
                        </span>

                        <!-- begin Post -->
						
                        <section class="row">
						
                            <?php 
							    $paginar=1;
                                the_post();
                                $day = get_the_time('F j, Y');
                            ?>

                            <article class="post col-sm-12">
							
                                <?php the_post_thumbnail();?>
								
                                <header>

                                    <h3 <?php echo colors('h3');?>><?php echo the_title();?></h3>

                                </header>

                                <div class="single_page">
									<div class="content">
	                                    <p <?php echo colors('p');?>><?php the_content();?></p>
							   		</div>
							        <?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'gym' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
							    
								</div>
								
							</article>
								
						</section>

                        <!-- end Post -->

                        <!-- begin Comments -->
						
                        <?php comments_template(); ?><!-- end Comments -->
						
                    </div>

                    <!-- begin Sidebar -->
					
                    <?php if(of_get_option('layer_img_single') == 'right'){?>
					
                    <div class="col-sm-3 sidebar">
					
                        <?php get_sidebar() ?>
						
                    </div>
					
                    <?php }?>
					
                </div>
				  
            </div>
			
        </article>
		
	</div>
		
</section>
	
<!-- end Content -->

<?php get_footer() ?>