<!DOCTYPE html>
<html>
<head>
	<!-- Define Charset -->
	<meta charset="utf-8">
	<!-- Page Title -->
	<title><?php   global $page, $paged;

        wp_title( '|', true, 'right' );


        bloginfo( 'name' );


        $site_description = get_bloginfo( 'description', 'display' );
        if ( $site_description && ( is_home() || is_front_page() ) )
        echo " | $site_description";
    ?>
    </title>

	<!-- Responsive Metatag -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

	<!-- CSS -->

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Media queries -->
	<!--[if lt IE 9]>
	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->
	<?php wp_head(); ?>

</head>
<body <?php body_class();?> >
	<!-- Header -->
	<header>
		<!-- Top Bar -->
		<div class="top-bar" >
			<div class="container" >
				<div class="row">
					<ul class="misc" >
							<li class="dropdown">
								<a href="/getting-started/"><strong>New Members</strong></a>
							</li>
							<li class="free-class">
								<a href="/contact/"><strong>Free Class</strong></a>
							</li>
							<li>
								<a href="/blog/"><strong>Current Athletes:</strong></a>
							</li>
					</ul>
					<ul class="social-header">
						<?php
						for($i=1; $i<=of_get_option('header_sl_num'); $i++) :
							if(of_get_option('show_header'.$i)) :

									echo '<li><a '.colors('a').' href="'.of_get_option('header_social_link'.$i).'"><i '.font_awesome_icon_style('header_social'.$i).'></i></a></li>';
							endif;
						endfor; 
						?>
					</ul>
				</div>
			</div>
		</div>
		<!-- End Top Bar -->

		<!-- Main Menu -->
		<div class="main-menu" >
			<div class="main-menu-line" >
				<div class="container">
					<div class="row">
						<nav class="navbar" role="navigation">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
									<span <?php echo colors('h1s');?> class="sr-only">Toggle navigation</span>
									<span <?php echo colors('h1s');?> class="icon-bar"></span>
									<span <?php echo colors('h1s');?> class="icon-bar"></span>
									<span <?php echo colors('h1s');?> class="icon-bar"></span>
								</button>
								<h1 <?php echo colors('h1');?> class="logo">
									<a <?php $styles = of_get_option('typo_logo'); echo 'style="color: '.$styles['color'].'; font-style: '.$styles['style'].'; font-family: '.$styles['face'].'; font-size: '.$styles['size'].'px;"' ?> 
class="navbar-brand" href="<?php echo get_site_url(); ?>">
									
                                        <?php 
                                            if(of_get_option('style_logo') == '1'){
                                                echo of_get_option('text_logo');
                                            }else{
                                        ?>
                                         <img src="<?php echo of_get_option('img_logo');?>" alt="Logo" />
                                        <?php }?>
                                    </a>
								</h1>
							</div>

							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse navbar-ex1-collapse">
								
		                <?php wp_nav_menu(array(
                                  'theme_location' => 'Main',
                                  'container' => 'false',
                                  'fallback_cb' => 'false',
                                  'menu_class'      => 'nav navbar-nav navbar-right',
                                  'menu_id'         => 'main'
                              ));
                        ?>
                        
							</div><!-- /.navbar-collapse -->
						</nav>
					</div>
				</div>
			</div>
		</div>
		<!-- End Main Menu -->
	</header>