<?php

add_action('wp_print_styles', 'modig_replace_styles', 10);
if( !function_exists( 'modig_replace_styles' ) ) {
	function modig_replace_styles(){
		// Fix the terribly enqueued styles from the parent theme
		
		wp_dequeue_style('gym_style');
		wp_dequeue_style('bootstrap');
		wp_dequeue_style('principal');
		wp_dequeue_style('green');
		wp_dequeue_style('media');
		wp_dequeue_style('pay-chart');
		wp_dequeue_style('mytheme-opensans');

		$reqs = array();
		if (!is_admin()) {
			wp_enqueue_style('gym_style', get_template_directory_uri() . '/style.css');
			$reqs[] = 'gym_style';
		}
		wp_enqueue_style('bootstrap', false, $reqs);
		$reqs[] = 'bootstrap';
		wp_enqueue_style('principal', false, $reqs);
		$reqs[] = 'principal';
		wp_enqueue_style('green', false, $reqs);
		$reqs[] = 'green';
		wp_enqueue_style('media', false, $reqs);
		$reqs[] = 'media';
		wp_enqueue_style('pay-chart', false, $reqs);
		$reqs[] = 'pay-chart';
		wp_enqueue_style('mytheme-opensans', "//fonts.googleapis.com/css?family=Open+Sans", $reqs );
		$reqs[] = 'mytheme-opensans';
		wp_enqueue_style('gym_child', get_stylesheet_uri(), $reqs);
	}
}


wp_dequeue_script('retina');
wp_enqueue_script('retina', get_stylesheet_directory_uri().'/js/retina.js',array('jquery'), false, true);
wp_enqueue_script('watermark', get_stylesheet_directory_uri().'/js/lib/jquery.watermark.min.js',array('jquery'), false, true);
wp_enqueue_script('gym_child', get_stylesheet_directory_uri().'/js/script.js',array('jquery'), false, true);



if(function_exists("register_field_group"))
{
	$slider = new RevSlider();
  $arrSliders = $slider->getArrSlidersShort();
  $arrSliders[0] = 'None';

	register_field_group(array (
		'id' => 'acf_tg-slider',
		'title' => 'TG Slider',
		'fields' => array (
			array (
				'key' => 'field_53ea9ba51a6ee',
				'label' => 'Revolution Slider',
				'name' => 'revolution_slider',
				'type' => 'select',
				'choices' => $arrSliders,
				'default_value' => 0,
				'allow_null' => 0,
				'multiple' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'page',
					'order_no' => 0,
					'group_no' => 0,
				),
				array (
					'param' => 'page_type',
					'operator' => '!=',
					'value' => 'front_page',
					'order_no' => 1,
					'group_no' => 0,
				),
				array (
					'param' => 'page_type',
					'operator' => '!=',
					'value' => 'posts_page',
					'order_no' => 2,
					'group_no' => 0,
				),
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'default',
					'order_no' => 3,
					'group_no' => 0,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'classes',
					'order_no' => 0,
					'group_no' => 1,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}
