<?php get_header(); ?>
	<section id="classes" >
		<?php 
		if(of_get_option('show_banner_class')) : 
			?>
			<div class="sub-header" style="<?php echo 'background:url('.of_get_option('class_banner_image').') no-repeat;'?>">
				<div class="container">
					<div class="row" >
						<ul class="sub-header-container" >
							<li>
								<h3 class="title"><?php echo of_get_option('class_banner_text') ?></h3>
							</li>
							<li>
							   <?php if (of_get_option('show_breadcumbs_class')) { ?>
								<ul class="custom-breadcrumb" >
									 <li><h6><a href="<?php echo home_url();?>">Home</a></h6></li>
									<li><i class="separator entypo-play" ></i></li>
									<li><h6>Classes</h6></li>
								</ul>
								<?php  } ?>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<?php 
		endif; 
		?>

		<!-- begin Classes -->
		<article class="article-container">
			<div class="container grid" >
				<div class="row" >
					<div class="col-md-12">
					<h2 <?php echo colors('h2');?> class="article-title" ><?php echo of_get_option('class_title')?></h2>
					<span <?php echo colors('h1s');?> class="line" >
						<span <?php echo colors('h1s');?> class="sub-line" ></span>
					</span>
					</div>
				</div>
			  <?php
				$args=array(
					'post_type' => 'classes',
					'post_status' => 'publish',
					'posts_per_page' => -1,
				);
				$my_query = null;
				$my_query = new WP_Query($args);
				$classes = array();
				
				while ($my_query->have_posts()) :
					$my_query->the_post();
					$classes[] = array(
						'previus_image' => get_field('previus_image'),
						'title' => get_the_title(),
						'teacher' => get_field('teacher'),
						'category' => get_field('category'),
						'link' => get_permalink()
					);
				endwhile;
			  ?>
				<div class="row content">
				
			<?php
				for($i=0; $i<count($classes); $i++) :
					if($classes[$i]['category'] != 0) :
						?>
						<div class="element <?php echo $classes[$i]['category'][0]->slug;?>" >
							<div class="link" >                                                              
								<a <?php echo colors('a');?> href="<?php echo $classes[$i]['link'] ?>"></a>
								<img src="<?php echo $classes[$i]['previus_image']; ?>" />
							</div>
							<div class="title">
								<h4 <?php echo colors('h4');?>><?php echo $classes[$i]['title']; ?></h4>                       
							</div>
						</div>
			  			<?php 
					endif;
				endfor;
				?>
				</div>
			</div>
		</article> 
		<!-- end Classes --> 

	<!-- begin Bodybuilding Supplements --> 
	<?php 
	if(of_get_option('show_sponsor_class')) :
		?>
		<article class="article-container">
			<div class="container" >
				<!-- arrows -->
				<div class="row" >
					<div class="col-md-12">
						<h2 <?php echo colors('h2');?> class="headers"><?php echo of_get_option('sponsor_title'); ?> </h2>
						<span <?php echo colors('h1s');?> class="line" >
							<span <?php echo colors('h1s');?> class="sub-line" ></span>
						</span>
						<?php if(of_get_option('sponsor_cant')>5) { ?>
						<a <?php echo colors('a');?> class="slider-control pull-right next" href="#bodybuilding" data-slide="next"></a>
						<a <?php echo colors('a');?> class="slider-control pull-right prev" href="#bodybuilding" data-slide="prev"></a>
						<?php } ?>
					</div>
				</div>
				<!-- end arrows -->

				<div class="row" >
					<div id="bodybuilding" class="carousel slide">
						<!-- Wrapper for slides -->
						<div class="carousel-inner">
							<?php
							$begin1='<div class="item active"><ul class="logos" >';
							$last1 = '</ul></div>';
							$begin2='<div class="item"><ul class="logos" >';
							for ($log=1; $log <= of_get_option('sponsor_cant') ; $log++) :

								if($log!=1){ $begin1 = ''; }
								echo $begin1;
								?>
								<li><a <?php echo colors('a');?> href="<?php echo of_get_option('sponsor_link'.$log);  ?>"><img src="<?php echo of_get_option('sponsor_text'.$log); ?>" alt="<?php echo $log;?>" /></a></li>
								<?php
								$c=$log%5;
								if($c==0){ echo $last1; echo $begin2; }
							endfor; 
							?>
						</div>
					</div>
				</div>
			</div>
		</article>
		<!-- end Bodybuilding Supplements -->
		<?php 
	endif; 
	?>
	</section>
 <?php get_footer(); ?>