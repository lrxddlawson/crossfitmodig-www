    <?php
        $print = 1;

        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; 
        $args = array(
            'category__not_in' => array(21),
            'paged' => $paged,
        );

        $query = new WP_Query( $args );
        
        if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();            $format = get_post_format($post->ID);
            if( false === $format ) { $format = 'standard'; }
                $day = get_the_time('j');
                $month = get_the_time('M');
                $year = get_the_time('Y'); 
    ?>
    
     <article class="post">
     
            <?php
                                        
            if( has_post_thumbnail($post->ID) ) { the_post_thumbnail('full', array('alt'=>$post->post_title)); }
                                
            ?>
            
            <header>
            
                <h3 <?php echo colors('h3');?>><?php echo the_title();?></h3>
                
            </header>
            
            <p <?php echo colors('p');?>><?php echo wp_trim_words(get_the_content(),40,'...');?></p>
            
            <a <?php echo colors('a');?> href="<?php the_permalink();?>" class="readmore">Read more <i class="entypo-right-open" ></i></a>
            
            <footer>
            
                <span <?php echo colors('h1s');?>><i class="entypo-pencil" ></i><?php the_author();?></span>
                
                <span <?php echo colors('h1s');?>><i class="entypo-calendar" ></i><?php echo $day;?></span>
                
                <span <?php echo colors('h1s');?>><i class="entypo-comment" ></i><?php comments_number();?></span>
                
            </footer>
            
        </article>
          
    <?php endwhile; ?>

    <?php else : ?>
    
    <p <?php echo colors('p');?> ><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'THEME_FRONT_SITE' ); ?></p>
    
    <?php get_search_form(); ?>
    
    <?php endif;?>