<?php get_header() ?>
    <!-- end Header -->

    <!-- begin Content -->
    <section id="news" >
        <!-- begin Sub Header -->
        <div class="sub-header" style="background: url('http://crossfitmodig.stage.lrxd.com/wp-content/uploads/2014/07/header-image_06_contact1.jpg');">
        
            <div class="container">
            
                <div class="row" >
                
                    <ul class="sub-header-container" >
                    
                        <li>
                        
                            <h3 <?php echo colors('h3');?> class="title">Modig Happenings</h3>
                            
                        </li>
                        
                    </ul>
                    
                </div>
                
            </div>
            
        </div>

    <!-- begin Sub Header -->
	<article class="article-container">
    
        <div class="container" >
        
            <div class="row" >
                <!-- begin Main Colum -->
                    
                <div class="col-sm-9">
                
                    
                    
                    <span <?php echo colors('h1s');?> class="line" >
                    
                        <span <?php echo colors('h1s');?> class="sub-line" ></span>
                        
                    </span>
                    
                    <section class="posts">
                    
                    <?php get_template_part( 'loop' ); ?>
                    
                        <aside>
                        
                            <ul class="pagination">
                            
                            <?php gym_pagination();?>
                            
                            </ul>
                            
                        </aside>
                        
                    </section>
                    
                </div>
            <!-- end Main Colum -->

            <!-- begin Sidebar -->
            <aside class="col-sm-3 sidebar">
            
                <?php get_sidebar(); ?>
                
            </aside>
            <!-- end Sidebar -->
            
            </div>
            
        </div>
        
    </article>
        <!-- end News -->

    </section>
    <!-- end Content -->

<?php get_footer(); ?>