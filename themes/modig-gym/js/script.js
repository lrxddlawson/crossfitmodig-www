jQuery(document).ready(function ($) {
	
/*****************************  Carousel  *********************************/
	// Overriding default Carousel functionality by quickly changing the carousel class and initiating
	// the carousel with a longer interval
	$('.carousel').removeClass('carousel').addClass('tg-carousel');
	$('.tg-carousel').carousel({
	  interval: 6000
	})



	$(window).resize(function() {
		matchHeight( $('#slider-trainers .element .title') );
		matchHeight( $('#classes .grid .content .element .title') );
	})
	.trigger('resize');

	function matchHeight($matchGroup) {
		var tallestTrainerTitle = 0;
		$matchGroup.each(function(idx, val) {
			
			var $title = $(this),
				$titleChildren = $title.children(),
				thisHeight = 0;
			 
			$titleChildren.each(function(){
				var $thisChild = $(this);

				thisHeight = thisHeight + $thisChild.outerHeight();
			});
			if(thisHeight > tallestTrainerTitle) {
				tallestTrainerTitle = thisHeight;
			}
		});
		$matchGroup.height(tallestTrainerTitle);
	}
	

	$('.banner').removeClass('banner');

	$('.watermarked').find('input[type="text"], input[type="email"], input[type="checkbox"], textarea, select').each(function() {
		var $input = $(this),
			$row = $input.closest('.input-row'),
			$label = $row.find('label');

		
		if ($input.is('select')) {
			// Swap Label in for "Null Value" option
			var $option = $input.find('option[value=""]');
			$option.html($label.text());

			// Add in utility classes.  
			$row.addClass('unselected dropdown');
			
			// Add change functionality that detects whether the dropdown is selected or not.
			$input.on('change', function(){
				if($input.val()=='') {
					$row.addClass('unselected');
				}
				else {
					$row.removeClass('unselected');
				}
			});

			// Hide label
			$label.hide();
		}
		else if ($input.is('input[type="checkbox"]')) {
			// Add utility class
			$row.addClass('checkbox');
		}
		else {
			// Use label as input watermark
			$input.watermark($label.text());
			
			// Hide label
			$label.hide();
		}

	});

});