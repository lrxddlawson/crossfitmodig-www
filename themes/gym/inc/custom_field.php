<?php
/**
 *  Install Add-ons
 *  
 *  The following code will include all 4 premium Add-Ons in your theme.
 *  Please do not attempt to include a file which does not exist. This will produce an error.
 *  
 *  All fields must be included during the 'acf/register_fields' action.
 *  Other types of Add-ons (like the options page) can be included outside of this action.
 *  
 *  The following code assumes you have a folder 'add-ons' inside your theme.
 *
 *  IMPORTANT
 *  Add-ons may be included in a premium theme as outlined in the terms and conditions.
 *  However, they are NOT to be included in a premium / free plugin.
 *  For more information, please read http://www.advancedcustomfields.com/terms-conditions/
 */ 

// Fields 
add_action('acf/register_fields', 'my_register_fields');

function my_register_fields()
{
	//include_once('add-ons/acf-repeater/repeater.php');
	//include_once('add-ons/acf-gallery/gallery.php');
	//include_once('add-ons/acf-flexible-content/flexible-content.php');

    register_field_group(array (
        'id' => 'acf_class-detail',
        'title' => 'Class Detail',
        'fields' => array (
            array (
                'key' => 'field_52a4d8016386c',
                'label' => '',
                'name' => 'show_class_detail',
                'type' => 'checkbox',
                'multiple' => 0,
                'allow_null' => 0,
                'choices' => array (
                    1 => 'Show Style Configuration',
                ),
                'default_value' => 1,
            ),
            array (
                'key' => 'field_52a38f2776c2b',
                'label' => 'Class Detail Title',
                'name' => 'class_detail_title',
                'type' => 'text',
                'default_value' => '',
                'formatting' => 'html',
            ),
            array (
                'key' => 'field_52a38f4a76c2c',
                'label' => 'Detail Features',
                'name' => 'detail_features',
                'type' => 'repeater',
                'sub_fields' => array (
                    array (
                        'key' => 'field_52a38f8276c2e',
                        'label' => 'detail_Icon',
                        'name' => 'detail_icon',
                        'type' => 'wysiwyg',
                        'column_width' => '',
                        'default_value' => '',
                        'toolbar' => 'full',
                        'media_upload' => 'yes',
                    ),
                    array (
                        'key' => 'field_52a38fd476c2f',
                        'label' => 'detail_text',
                        'name' => 'detail_text',
                        'type' => 'text',
                        'column_width' => 75,
                        'default_value' => '',
                        'formatting' => 'html',
                    ),
                ),
                'row_min' => 0,
                'row_limit' => '',
                'layout' => 'table',
                'button_label' => 'Add Row',
            ),
            array (
                'key' => 'field_52a390df048be',
                'label' => 'Button Title',
                'name' => 'button_title',
                'type' => 'text',
                'default_value' => '',
                'formatting' => 'html',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'classes',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
                0 => 'the_content',
                1 => 'custom_fields',
            ),
        ),
        'menu_order' => 0,
    ));
}

// Options Page 
//include_once( 'add-ons/acf-options-page/acf-options-page.php' );


/**
 *  Register Field Groups
 *
 *  The register_field_group function accepts 1 array which holds the relevant data to register a field group
 *  You may edit the array as you see fit. However, this may result in errors if the array is not compatible with ACF
 */

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_class-detail',
		'title' => 'Class Detail',
		'fields' => array (
			array (
				'key' => 'field_52a4d8016386c',
				'label' => '',
				'name' => 'show_class_detail',
				'type' => 'checkbox',
				'multiple' => 0,
				'allow_null' => 0,
				'choices' => array (
					1 => 'Show Style Configuration',
				),
				'default_value' => 1,
			),
			array (
				'key' => 'field_52a38f2776c2b',
				'label' => 'Class Detail Title',
				'name' => 'class_detail_title',
				'type' => 'text',
				'default_value' => '',
				'formatting' => 'html',
			),
			array (
				'key' => 'field_52a38f4a76c2c',
				'label' => 'Detail Features',
				'name' => 'detail_features',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_52a38f8276c2e',
						'label' => 'detail_Icon',
						'name' => 'detail_icon',
						'type' => 'wysiwyg',
						'column_width' => '',
						'default_value' => '',
						'toolbar' => 'full',
						'media_upload' => 'yes',
					),
					array (
						'key' => 'field_52a38fd476c2f',
						'label' => 'detail_text',
						'name' => 'detail_text',
						'type' => 'text',
						'column_width' => 75,
						'default_value' => '',
						'formatting' => 'html',
					),
				),
				'row_min' => 0,
				'row_limit' => '',
				'layout' => 'table',
				'button_label' => 'Add Row',
			),
			array (
				'key' => 'field_52a390df048be',
				'label' => 'Button Title',
				'name' => 'button_title',
				'type' => 'text',
				'default_value' => '',
				'formatting' => 'html',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'classes',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'the_content',
				1 => 'custom_fields',
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_class-figure',
		'title' => 'Class Figure',
		'fields' => array (
			array (
				'key' => 'field_52a4d7eb7d8e3',
				'label' => '',
				'name' => 'show_class_figure',
				'type' => 'checkbox',
				'multiple' => 0,
				'allow_null' => 0,
				'choices' => array (
					1 => 'Show Style Configuration',
				),
				'default_value' => 1,
			),
			array (
				'key' => 'field_52a38ab5a143d',
				'label' => 'Subtitle',
				'name' => 'subtitle',
				'type' => 'text',
				'default_value' => '',
				'formatting' => 'html',
			),
			array (
				'key' => 'field_52a38ae6a143e',
				'label' => 'Content',
				'name' => 'content',
				'type' => 'textarea',
				'default_value' => '',
				'formatting' => 'br',
			),
			array (
				'key' => 'field_52a391b13af67',
				'label' => 'Category',
				'name' => 'category',
				'type' => 'taxonomy',
				'taxonomy' => 'category',
				'field_type' => 'select',
				'allow_null' => 0,
				'load_save_terms' => 1,
				'return_format' => 'object',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'classes',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'the_content',
				1 => 'custom_fields',
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_class-imagen-gallery',
		'title' => 'Class Imagen Gallery',
		'fields' => array (
			array (
				'key' => 'field_52a4d7cc40194',
				'label' => '',
				'name' => 'show_gallery',
				'type' => 'checkbox',
				'multiple' => 0,
				'allow_null' => 0,
				'choices' => array (
					1 => 'Show Style Configuration',
				),
				'default_value' => 1,
			),
			array (
				'key' => 'field_52a39488f3645',
				'label' => 'Previus Image',
				'name' => 'previus_image',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'full',
			),
			array (
				'key' => 'field_52a38a4d9387d',
				'label' => 'Imagen Gallery',
				'name' => 'img_gallery',
				'type' => 'gallery',
				'preview_size' => 'full',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'classes',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'the_content',
				1 => 'custom_fields',
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_class-teacher',
		'title' => 'Class Teacher',
		'fields' => array (
			array (
				'key' => 'field_52a4d7b8335b9',
				'label' => '',
				'name' => 'show_teacher',
				'type' => 'checkbox',
				'multiple' => 0,
				'allow_null' => 0,
				'choices' => array (
					1 => 'Show Style Configuration',
				),
				'default_value' => 1,
			),
			array (
				'key' => 'field_52a3910ad6730',
				'label' => 'Teacher Icon',
				'name' => 'teacher_icon',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'post_type' => array (
					0 => 'trainer',
				),
				'taxonomy' => array (
					0 => 'all',
				),
				'multiple' => 0,
				'allow_null' => 0,
				'key' => 'field_52a3912acbd6a',
				'label' => 'Teacher',
				'name' => 'teacher',
				'type' => 'post_object',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'classes',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'the_content',
				1 => 'custom_fields',
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_trainer-education',
		'title' => 'Trainer Education',
		'fields' => array (
			array (
				'key' => 'field_52a4d795ef3b4',
				'label' => '',
				'name' => 'show_trainer_education',
				'type' => 'checkbox',
				'multiple' => 0,
				'allow_null' => 0,
				'choices' => array (
					1 => 'Show Style Configuration',
				),
				'default_value' => 1,
			),
			array (
				'key' => 'field_52a20fe98fc01',
				'label' => 'Title Education',
				'name' => 'title_education',
				'type' => 'text',
				'default_value' => '',
				'formatting' => 'html',
			),
			array (
				'key' => 'field_52a210378fc02',
				'label' => 'Detail Education',
				'name' => 'detail_education',
				'type' => 'text',
				'default_value' => '',
				'formatting' => 'html',
			),
			array (
				'key' => 'field_52a2104a8fc03',
				'label' => 'Description Education',
				'name' => 'description_education',
				'type' => 'textarea',
				'default_value' => '',
				'formatting' => 'br',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'trainer',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'the_content',
				1 => 'custom_fields',
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_trainer-experiences',
		'title' => 'Trainer Experiences',
		'fields' => array (
			array (
				'key' => 'field_52a4d772a7f59',
				'label' => '',
				'name' => 'show_trainer_experience',
				'type' => 'checkbox',
				'multiple' => 0,
				'allow_null' => 0,
				'choices' => array (
					1 => 'Show Style Configuration',
				),
				'default_value' => 1,
			),
			array (
				'key' => 'field_52a22d636137d',
				'label' => 'Class Experience Title',
				'name' => 'class_experience_title',
				'type' => 'text',
				'default_value' => '',
				'formatting' => 'html',
			),
			array (
				'key' => 'field_52a20f42c6c19',
				'label' => 'Class Experience',
				'name' => 'class_experience',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_52a22acd79ad8',
						'label' => 'Percent',
						'name' => 'percent',
						'type' => 'number',
						'column_width' => '',
						'default_value' => 0,
					),
					array (
						'key' => 'field_52a22ae679ad9',
						'label' => 'Class Title',
						'name' => 'class_title',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'formatting' => 'html',
					),
					array (
						'key' => 'field_52a22aff79ada',
						'label' => 'Class Description',
						'name' => 'class_description',
						'type' => 'textarea',
						'column_width' => '',
						'default_value' => '',
						'formatting' => 'br',
					),
				),
				'row_min' => 0,
				'row_limit' => '',
				'layout' => 'row',
				'button_label' => 'Add Row',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'trainer',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'the_content',
				1 => 'custom_fields',
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_trainer-faq',
		'title' => 'Trainer FAQ',
		'fields' => array (
			array (
				'key' => 'field_52a4d75268288',
				'label' => '',
				'name' => 'show_faq',
				'type' => 'checkbox',
				'multiple' => 0,
				'allow_null' => 0,
				'choices' => array (
					1 => 'Show Style Configuration',
				),
				'default_value' => 1,
			),
			array (
				'key' => 'field_52a230b0ca889',
				'label' => 'FAQ Title',
				'name' => 'faq_title',
				'type' => 'text',
				'default_value' => '',
				'formatting' => 'html',
			),
			array (
				'key' => 'field_52a230c1ca88a',
				'label' => 'FAQ',
				'name' => 'faq',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_52a230cfca88b',
						'label' => 'Question',
						'name' => 'question',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'formatting' => 'html',
					),
					array (
						'key' => 'field_52a23149ca88c',
						'label' => 'Title Response',
						'name' => 'title_response',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'formatting' => 'html',
					),
					array (
						'key' => 'field_52a23156ca88d',
						'label' => 'Response',
						'name' => 'response',
						'type' => 'textarea',
						'column_width' => '',
						'default_value' => '',
						'formatting' => 'br',
					),
				),
				'row_min' => 0,
				'row_limit' => '',
				'layout' => 'row',
				'button_label' => 'Add Row',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'trainer',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'the_content',
				1 => 'custom_fields',
				2 => 'categories',
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_trainer-figure',
		'title' => 'Trainer Figure',
		'fields' => array (
			array (
				'key' => 'field_52a4d71a42bb1',
				'label' => '',
				'name' => 'show_figure',
				'type' => 'checkbox',
				'multiple' => 0,
				'allow_null' => 0,
				'choices' => array (
					1 => 'Show Style Configuration',
				),
				'default_value' => 1,
			),
			array (
				'key' => 'field_52a0f3d3bedd2',
				'label' => 'Name',
				'name' => 'name',
				'type' => 'text',
				'default_value' => '',
				'formatting' => 'html',
			),
			array (
				'key' => 'field_52a0f3e1bedd3',
				'label' => 'Photo',
				'name' => 'photo',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'full',
			),
			array (
				'key' => 'field_52a14ea7b1150',
				'label' => 'Specialist',
				'name' => 'specialist',
				'type' => 'text',
				'default_value' => '',
				'formatting' => 'html',
			),
			array (
				'key' => 'field_52a12e8b23a95',
				'label' => 'Fast Description',
				'name' => 'fast_description',
				'type' => 'textarea',
				'default_value' => '',
				'formatting' => 'br',
			),
			array (
				'key' => 'field_52a12f51fd097',
				'label' => 'Social Net',
				'name' => 'social_net',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_52a4da68200e0',
						'label' => 'Social icon',
						'name' => 'social_icon',
						'type' => 'wysiwyg',
						'column_width' => 10,
						'default_value' => '',
						'toolbar' => 'basic',
						'media_upload' => 'no',
					),
				),
				'row_min' => 0,
				'row_limit' => '',
				'layout' => 'row',
				'button_label' => 'Add Row',
			),
			array (
				'key' => 'field_52a0f400bedd4',
				'label' => 'Biografy',
				'name' => 'biografy',
				'type' => 'textarea',
				'default_value' => '',
				'formatting' => 'br',
			),
			array (
				'key' => 'field_52a0f414bedd5',
				'label' => 'Opinion',
				'name' => 'opinion',
				'type' => 'textarea',
				'default_value' => '',
				'formatting' => 'br',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'trainer',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'the_content',
				1 => 'excerpt',
				2 => 'custom_fields',
				3 => 'discussion',
				4 => 'comments',
				5 => 'revisions',
				6 => 'slug',
				7 => 'author',
				8 => 'format',
				9 => 'featured_image',
				10 => 'categories',
				11 => 'tags',
				12 => 'send-trackbacks',
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_trainer-grafic',
		'title' => 'Trainer Grafic',
		'fields' => array (
			array (
				'key' => 'field_52a4d60862f9c',
				'label' => '',
				'name' => 'show_grafic',
				'type' => 'checkbox',
				'multiple' => 0,
				'allow_null' => 0,
				'choices' => array (
					1 => 'Show Style Configuration',
				),
				'default_value' => 1,
			),
			array (
				'key' => 'field_52a4ae8e57de8',
				'label' => 'Line',
				'name' => '',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_52a4aeeb57de9',
						'label' => 'Points',
						'name' => '',
						'type' => 'repeater',
						'column_width' => '',
						'sub_fields' => array (
							array (
								'key' => 'field_52a4aefc57dea',
								'label' => 'text',
								'name' => 'text',
								'type' => 'text',
								'column_width' => '',
								'default_value' => '',
								'formatting' => 'html',
							),
						),
						'row_min' => 0,
						'row_limit' => '',
						'layout' => 'table',
						'button_label' => 'Add Row',
					),
				),
				'row_min' => 0,
				'row_limit' => '',
				'layout' => 'table',
				'button_label' => 'Add Row',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'trainer',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'the_content',
				1 => 'custom_fields',
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_trainer-grafic-pay',
		'title' => 'Trainer Grafic Pay',
		'fields' => array (
			array (
				'key' => 'field_52a4dd7c91195',
				'label' => '',
				'name' => 'show_pay',
				'type' => 'checkbox',
				'multiple' => 0,
				'allow_null' => 0,
				'choices' => array (
					1 => 'Show Style Configuration',
				),
				'default_value' => 1,
			),
			array (
				'key' => 'field_52a4d8aa7175d',
				'label' => 'Pay',
				'name' => 'pay',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_52a4d8cf7175e',
						'label' => 'Pay Percent',
						'name' => 'pay_percent',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'formatting' => 'html',
					),
					array (
						'key' => 'field_52a4d8e17175f',
						'label' => 'Pay Description',
						'name' => 'pay_description',
						'type' => 'textarea',
						'column_width' => '',
						'default_value' => '',
						'formatting' => 'br',
					),
				),
				'row_min' => 0,
				'row_limit' => '',
				'layout' => 'row',
				'button_label' => 'Add Row',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'trainer',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'the_content',
				1 => 'custom_fields',
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_trainer-other-experiences',
		'title' => 'Trainer Other Experiences',
		'fields' => array (
			array (
				'key' => 'field_52a4d6c80fe07',
				'label' => '',
				'name' => 'show_other_experiences',
				'type' => 'checkbox',
				'multiple' => 0,
				'allow_null' => 0,
				'choices' => array (
					1 => 'Show Style Configuration',
				),
				'default_value' => '',
			),
			array (
				'key' => 'field_52a4ac9e51a5c',
				'label' => 'Title Zone',
				'name' => 'title_zone',
				'type' => 'text',
				'default_value' => '',
				'formatting' => 'html',
			),
			array (
				'key' => 'field_52a4ad8651a5d',
				'label' => 'Progres Bar',
				'name' => 'progres_bar',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_52a4adbd51a5e',
						'label' => 'Percent',
						'name' => 'percent',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'formatting' => 'html',
					),
					array (
						'key' => 'field_52a4adde51a5f',
						'label' => 'Title',
						'name' => 'title',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'formatting' => 'html',
					),
				),
				'row_min' => 0,
				'row_limit' => '',
				'layout' => 'row',
				'button_label' => 'Add Row',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'trainer',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'the_content',
				1 => 'custom_fields',
			),
		),
		'menu_order' => 0,
	));
}

?>
