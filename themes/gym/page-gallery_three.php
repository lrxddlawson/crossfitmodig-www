<?php get_header(); ?>
    <!-- end Header -->


    <!-- begin Content -->
    <section class="gallery" id="gal3" >
<?php if(of_get_option('show_banner_three')){?>
        <div class="sub-header" style="<?php echo 'background:url('.of_get_option('three_banner_image').') no-repeat;'?>">
            <div class="container">
                <div class="row" >
                    <ul class="sub-header-container" >
                        <li>
                            <h3 <?php echo colors('h3');?> class="title"><?php echo of_get_option('three_banner_text') ?></h3>
                        </li>
                        <li>
                            <ul class="custom-breadcrumb" >
                                 <li><h6 <?php echo colors('h6');?>><a <?php echo colors('a');?> <?php echo colors('a');?> href="<?php echo home_url();?>">Home</a></h6></li>
                                <li><i class="separator entypo-play" ></i></li>
                                <li><h6 <?php echo colors('h1s');?>>Three</h6></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
<?php } ?>
        <!-- end Sub Header -->


        <!-- begin 3 columns gallery -->
        <article class="article-container">
            <div class="container" >

                <div class="row" >
                    <div class="col-md-12">
                    <h2 <?php echo colors('h2');?> class="article-title"><?php echo of_get_option('three_title'); ?></h2>
                    <span <?php echo colors('h1s');?> class="line" >
                        <span <?php echo colors('h1s');?> class="sub-line" ></span>
                    </span>
                    </div>
                </div>

                 <?php
                 for ($three=1; $three <= of_get_option('three_num_cant') ; $three++) {
                 $temp = $three+2;
                 $num  =  $temp%3;
                 $num2 =  $three%3;
                   ?>
                <?php if($num==0){?><div class="row"><?php }?>
                    <div class="col-sm-4">
                        <a <?php echo colors('a');?> class="example-image-link" href="<?php echo of_get_option('three_image'.$three)?>" data-lightbox="example-1">
                            <img src="<?php echo of_get_option('three_image'.$three)?>" alt="img" />
                        </a>
                    </div>
                    <?php if($num2==0){?></div><?php } ?>
                    <?php }?>
            </div>
        <article>
        <!-- end 3 columns gallery -->


        <!-- begin Bodybuilding Supplements -->
<?php if(of_get_option('show_sponsor_three')) {?>
        <article class="article-container">
            <div class="container" >

                <!-- arrows -->
                <div class="row" >
                    <div class="col-md-12">
                        <h2 <?php echo colors('h2');?> class="headers"><?php echo of_get_option('sponsor_title'); ?> </h2>
                        <span <?php echo colors('h1s');?> class="line" >
                            <span <?php echo colors('h1s');?> class="sub-line" ></span>
                        </span>
                        <?php if(of_get_option('sponsor_cant')>5) { ?>
                        <a <?php echo colors('a');?> class="slider-control pull-right next" href="#bodybuilding" data-slide="next"></a>
                        <a <?php echo colors('a');?> class="slider-control pull-right prev" href="#bodybuilding" data-slide="prev"></a>
                        <?php } ?>
                    </div>
                </div>
                <!-- end arrows -->
                 <div class="row" >
                    <div id="bodybuilding" class="carousel slide">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <?php
                            $begin1='<div class="item active"><ul class="logos" >';
                            $last1 = '</ul></div>';
                            $begin2='<div class="item"><ul class="logos" >';
                        for ($log=1; $log <= of_get_option('sponsor_cant') ; $log++) {

                            if($log!=1)
                               {
                                $begin1 = '';
                               }
                            echo $begin1;
                             ?>
                                <li><a <?php echo colors('a');?> <?php echo colors('a');?> href="<?php echo of_get_option('sponsor_link'.$log);  ?>"><img src="<?php echo of_get_option('sponsor_text'.$log) ?>" alt="<?php echo $log;?>" /></a></li>
                            <?php
                            $c=$log%5;
                            if($c==0)
                            {
                            echo $last1;
                            echo $begin2;

                            }

                            } ?>
                    </div>
                    </div>
                </div>
            </div>
        </article>
        <?php  } ?>

<?php get_footer(); ?>