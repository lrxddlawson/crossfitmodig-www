<footer>

		<div class="container" >
        
			<div class="row misc">
		
        		<div class="col-md-3" >
		
        			<?php if(of_get_option('show_footer_zone_1')) {?>
		
        			<h3 <?php echo colors('h3');?>><?php echo of_get_option('footer_zone_1_title')?></h3>
		
        		    <p <?php echo colors('p');?>><?php echo insert_br(of_get_option('zone_1_desc'));?></p>
		
        			<?php 
                        if(of_get_option('show_footer_adress')) { 
                    ?>
		
        			<ul class="about" >
        
                         <?php for ($t=1; $t <=of_get_option('cant_adress') ; $t++) { ?>
		
        				<li><i <?php echo font_awesome_icon_style('adress'.$t);?>></i><?php echo of_get_option('adress'.$t) ?></li>
		
        			<?php } ?>
		
        			</ul>
		
        			<?php } } ?>
		
        		</div>
		
        		<div class="col-md-3">
		
        			<?php if(of_get_option('show_footer_zone_2')) { ?>
		
        			<h3 <?php echo colors('h3');?>><?php echo of_get_option('footer_zone_2_title') ?></h3>
		
        			<ul class="links" >
        
                       <?php for ($b=1; $b <= of_get_option('cant_zona_2_links') ; $b++) { ?>
		
        				<li><a <?php echo colors('a');?> href="<?php echo of_get_option('zona_2_link'.$b) ?>"><i <i <?php echo font_awesome_icon_style('zona_2_vgn');?>></i><?php echo of_get_option('zona_2_text'.$b)?></a></li>
		
        			<?php } ?>
		
        			</ul>
		
        			<?php } ?>
		
        		</div>
		
        		<div class="col-md-3">
					<?php if(of_get_option('show_footer_zone_3')) { ?>
					<h3 <?php echo colors('h3');?>><?php echo of_get_option('footer_zone_3_title')?></h3>
					<p <?php echo colors('p');?>><?php echo of_get_option('zona_3_text')?></p>
                    

					<div class="newsletter" id="mc_embed_signup">
					
					<form method="post" id="mc_form" class="validate" novalidate>
					
                        <input type="hidden" id="mc_submit_hidden" name="mc_submit_hidden" value="html" />
						
                        <input type="hidden" name="mc_action" id="mc_action" value="<?php echo get_template_directory_uri();?>" />
						
						<div class="form-group">
						
							<label for="name" class="sr-only"><?php echo of_get_option('footer_zone_3_text') ?></label>
                            
							<input type="text" required="" value="" name="NOM" class="mc_input form-control" id="NOM" placeholder="<?php echo of_get_option('footer_zone_3_text');?>">
						
						</div>
						
						<div class="form-group">
							
						   <label for="email" class="sr-only"><?php echo of_get_option('footer_zone_3_mail') ?></label>
                           
						   <input type="email" required="" value="" name="EMAIL" class="mc_input form-control"  id="EMAIL" placeholder="<?php echo of_get_option('footer_zone_3_mail');?>">
						   						
						</div>
                        
						<input class="botton button-gym" <?php //echo colors('btn');?> type="submit" value="<?php echo of_get_option('footer_zone_3_buttom');?>" name="mc_submit" id="mc_submit">
                        
						<div class="send-mc" style="display: none;">Congratulations your request has been sent successfully!!!</div>

					</form>
					
					</div>


					<?php } ?>
				</div>
				<div class="col-md-3">
					<?php if(of_get_option('show_footer_zone_4')){?>
					<h3 <?php echo colors('h3');?>><?php echo of_get_option('footer_zone_4_title')?></h3>
					<p <?php echo colors('p');?>><?php echo of_get_option('footer_zone_4_text')?></p>
					<ul class="social" >
	<?php
          for($i=1; $i<=of_get_option('footer_zone_4_num'); $i++){    
           echo '<li><a '.colors('a').' href="http://'.of_get_option('footer_zone_4_social'.$i).'"><i '.font_awesome_icon_style('social_link'.$i).'></i></a></li>';
        } ?>
</ul>
<?php }?>
				</div>
			</div>
		</div>
		<div class="copyright" >
			<div class="container">
				<?php if(of_get_option('show_footer_rights')) {?>
				<p <?php echo colors('p');?> class="pull-left" >&copy; <?php echo of_get_option('rigths')  ?><a <?php echo colors('a');?> href="http://<?php echo of_get_option('link');  ?> ">  <?php echo of_get_option('company')  ?></a></p>
				<?php } ?>
				<?php 
                    wp_nav_menu(
                        array(
                            'theme_location' => 'Footer',
                            'container' => 'false',
                            'fallback_cb' => 'false',
                            'menu_class' => 'main-links pull-right',
                            'menu_id'=>'menu_footer'
                        ));
                ?>
               
			</div>
		</div>
	</footer>
	<!-- End footer -->


	<a <?php echo colors('a');?> href="#" class="scrollup"><i class="entypo-up-open"></i></a>
<?php wp_footer(); ?>
</body>
</html>