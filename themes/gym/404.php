<?php get_header(); ?>

    <!-- begin Content -->
	
    <section id="errorpage" >
	
        <?php if(of_get_option('show_banner_404')){?>
		
        <div class="sub-header" style="<?php echo 'background:url('.of_get_option('404_banner_image').') no-repeat;'?>">
		
            <div class="container">
			
                <div class="row" >
				
                    <ul class="sub-header-container" >
					
                        <li>
						
                            <h3 <?php echo colors('h3');?> class="title"><?php echo of_get_option('404_banner_text') ?></h3>
							
                        </li>
						
                        <li>
						
                            <?php if (of_get_option('show_breadcumbs_404')) { ?>
                            
                            <ul class="custom-breadcrumb" >
                            
                                <li><h6 <?php echo colors('h6');?>><a <?php echo colors('a');?> href="index.html">Home</a></h6></li>
                                 
                                <li><i class="separator entypo-play" ></i></li>
                                
                                <li><h6 <?php echo colors('h6');?>>PAGE - 404</h6></li>
                               
                               <?php }?> 
                                
                            </ul>
							
                        </li>
						
                    </ul>
					
                </div>
				
            </div>
			
        </div>
		
        <?php } ?><!-- end Sub Header -->

        <!-- begin error page -->
		
        <article class="article-container">
		
            <div class="container" >
			
                <div class="row errorpage">
				
                    <div class="col-md-12">
					
                        <strong><?php echo of_get_option('404_title')?></strong>
						
                        <br />
						
                        <b><?php echo of_get_option('404_text')?></b>

                        <i><?php echo of_get_option('404_subtitle')?></i>

                        <p <?php echo colors('p');?>><?php echo of_get_option('404_explain')?></p>
						
                        <a <?php echo colors('a');?> href="<?php echo get_site_url(); ?>" class="button-gym big-button"><?php echo of_get_option('404_botton')?></a>
						
                    </div>
					
                </div>
				
            </div>
			
        </article>
		
        <!-- end error page -->

    </section>
	 
    <!-- end Content -->

    <!-- end content -->
	
<?php get_footer(); ?>