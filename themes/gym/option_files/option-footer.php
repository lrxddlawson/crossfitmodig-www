<?php
	$options[] = array(
            'name' =>('Footer Section'),
            'type' => 'heading',
            'std' => 'arrow-down'
    );

   //FOOTER ZONES
        $options[] = array(
            'name' => 'Zone 1',
            'type' => 'toggle'
        );
            $options[] = array(
                'name' =>'Zone 1',
                'id' => 'show_footer_zone_1',
                'desc' => 'Show Zone 1',
                'std' => 1,
                'type' => 'checkbox'
            );
            $options[] = array(
                'name' => 'Intro',
                'type' => 'toggle'
                    );

                $options[] = array(
                    'id' => 'footer_zone_1_title',
                    'desc' => 'Title',
					'std' => 'About the Club',
                    'type' => 'text'
                );
                $options[] = array(
                    'id' => 'zone_1_desc',
                    'desc' => 'Description',
                    'std' => 'We can condimentum est lacus ut dolor. Sed facilisis ante felis, vitae mattis massa luctus sit amet. Vestibulum eu blandit ipsum. In ornare enim nunc.',
                    'type' => 'textarea'
                );
            $options[] = array(
                'type' => 'toggle-close'
            );
                  $options[] = array(
                    'name' => 'Adress Group',
                    'type' => 'toggle'
                  );
                $options[] = array(
                'name' =>'Adress Group',
                'id' => 'show_footer_adress',
                'desc' => 'Show Adress Group ',
                'std' => 1,
                'type' => 'checkbox'
            );

            $options[] = array(
                'id' => 'cant_adress',
                'desc' => 'Number of Paragraph',
                'std' => 2,
                'class' => 'mini',
                'type' => 'text'
            );
            if(of_get_option('cant_adress')?$link=of_get_option('cant_adress'):$link=2);
                for($y=1; $y<=$link; $y++){
                    $options[] = array(
                        'name' => 'Adress '.$y,
                        'type' => 'toggle'
                    );

                        $options[] = array(
                            'id' => 'adress'.$y,
                            'desc' => 'Text',
                            'class' => 'medium',
                            'type' => 'text'
                        );
                        foreach(font_awesome_icon('adress'.$y) as $val){
                            $options[] = $val;
                        }

                    $options[] = array(
                        'type' => 'toggle-close'
                    );
                }


                  $options[] = array(
                  'type' => 'toggle-close'
                 );


        $options[] = array(
            'type' => 'toggle-close'
        );

  //Zone 2
  $options[] = array(
    'name' => 'Zone 2',
    'type' => 'toggle'
  );
            $options[] = array(
                'name' =>'Zone 2',
                'id' => 'show_footer_zone_2',
                'desc' => 'Show Zone 2',
                'std' => 1,
                'type' => 'checkbox'
            );

            $options[] = array(
                'id' => 'footer_zone_2_title',
                'desc' => 'Title',
				'std' => 'Useful Links',
                'type' => 'text'
            );
              $options[] = array(
                'id' => 'cant_zona_2_links',
                'desc' => 'Number of Links',
                'std' => 4,
                'class' => 'mini',
                'type' => 'text'
            );
            foreach(font_awesome_icon('zona_2_vgn') as $val){
                $options[] = $val;
            }
            if(of_get_option('cant_zona_2_links')?$link=of_get_option('cant_zona_2_links'):$link=4);
                for($l=1; $l<=$link; $l++){
                    $options[] = array(
                        'name' => 'Links '.$l,
                        'type' => 'toggle'
                    );
                    
                    $options[] = array(
                            'id' => 'zona_2_text'.$l,
                            'desc' => 'Text',
							'std' => 'Meet the Coaches of the Club',
                            'class' => 'medium',
                            'type' => 'text'
                        );

                    $options[] = array(
                            'id' => 'zona_2_link'.$l,
                            'desc' => 'Url',
                            'class' => 'medium',
                            'type' => 'text'
                        );


                    $options[] = array(
                        'type' => 'toggle-close'
                    );
                }


  $options[] = array(
            'type' => 'toggle-close'
        );
  $options[] = array(
    'name' => 'Zone 3',
    'type' => 'toggle'
  );
            $options[] = array(
                'name' =>'Zone 3',
                'id' => 'show_footer_zone_3',
                'desc' => 'Show Zone 3',
                'std' => 1,
                'type' => 'checkbox'
            );

            $options[] = array(
                'id' => 'footer_zone_3_title',
                'desc' => 'Title',
				'std' => 'Newsletter',
                'type' => 'text'
            );
              $options[] = array(
                            'id' => 'zona_3_text',
                            'desc' => 'Text',
							'std' => 'Suscribe to our ahasellus sit amet justo sapien and raesent bibendum you will get nim non fringilla vestibulum.',
                            'class' => 'medium',
                            'type' => 'textarea'
                      );
             $options[] = array(
                'id' => 'footer_zone_3_text',
                'desc' => 'Text Name',
                'std' =>'Your Name',
                'type' => 'text'
            );
             $options[] = array(
                'id' => 'footer_zone_3_mail',
                'desc' => 'Text Mail',
                'std' =>'Your Mail',
                'type' => 'text'
            );
             $options[] = array(
                'id' => 'footer_zone_3_buttom',
                'desc' => 'Text Button',
                'std' =>'Send Mail',
                'type' => 'text'
            );
  $options[] = array(
            'type' => 'toggle-close'
        );

   $options[] = array(
    'name' => 'Zone 4',
    'type' => 'toggle'
  );
   $options[] = array(
                'name' =>'Zone 4',
                'id' => 'show_footer_zone_4',
                'desc' => 'Show Zone 4',
                'std' => 1,
                'type' => 'checkbox'
            );
   $options[] = array(
            'id' => 'footer_zone_4_title',
            'desc' => 'Title',
            'std' => 'Get Social',
            'type' => 'text'
  );
  $options[] = array(
            'id' => 'footer_zone_4_text',
            'desc' => 'Text',
            'std' => 'Follow us on the Social Networks to let all the news and win disccounts!',
            'type' => 'textarea'
  );
  $options[] = array(
            'id' => 'footer_zone_4_num',
            'desc' => 'Number of Social Link',
            'std' => '',
            'type' => 'text',
            'class' => 'mini'
  );
  if(of_get_option('footer_zone_4_num')?$cant=of_get_option('footer_zone_4_num'):$cant=6);
  for($i=1; $i<=$cant; $i++){
      $options[] = array(
            'name' => 'Social Link '.$i,
            'type' => 'toggle'            
      );
        foreach(font_awesome_icon('social_link'.$i) as $val){
            $options[] = $val;
        }
        $options[] = array(
                'id' =>  'footer_zone_4_social'.$i,
                'desc' => 'Social link',
                'type' => 'text'
        );
      $options[] = array(
            'name' => 'Social Link '.$i,
            'type' => 'toggle-close' 
      );
  }
/*  $link_img = get_stylesheet_directory_uri().'/img/social/';
          $social_n = array(
            '1' => array('Facebook','www.facebook.com',array(
                    'img_1' => $link_img.'facebook.png',
                    'img_2' => $link_img.'facebook_1.png',
                    'img_3' => $link_img.'facebook_2.png',
                    'img_4' => $link_img.'facebook_3.png')),
            '2' => array('Dribble','www.dribble.com',array(
                    'img_1' => $link_img.'dribble.png',
                    'img_2' => $link_img.'dribble_1.png',
                    'img_3' => $link_img.'dribble_2.png',
                    'img_4' => $link_img.'dribble_3.png')),
            '3' => array('Twitter','www.twitter.com',array(
                    'img_1' => $link_img.'twitter.png',
                    'img_2' => $link_img.'twitter_1.png',
                    'img_3' => $link_img.'twitter_2.png',
                    'img_4' => $link_img.'twitter_3.png')),
            '4' => array('Linkedin','www.linkedin.com',array(
                    'img_1' => $link_img.'linkedin.png',
                    'img_2' => $link_img.'linkedin_1.png',
                    'img_3' => $link_img.'linkedin_2.png',
                    'img_4' => $link_img.'linkedin_3.png')),
            '5' => array('Google','www.google.com',array(
                    'img_1' => $link_img.'google.png',
                    'img_2' => $link_img.'google_1.png',
                    'img_3' => $link_img.'google_2.png',
                    'img_4' => $link_img.'google_3.png')),
            '6' => array('Flickr','www.Flickr.com',array(
                    'img_1' => $link_img.'flickr.png',
                    'img_2' => $link_img.'flickr_1.png',
                    'img_3' => $link_img.'flickr_2.png',
                    'img_4' => $link_img.'flickr_3.png')),
            '7' => array('You-Tube','www.youtube.com',array(
                    'img_1' => $link_img.'you-tube.png',
                    'img_2' => $link_img.'you-tube_1.png',
                    'img_3' => $link_img.'you-tube_2.png',
                    'img_4' => $link_img.'you-tube_3.png')),
             '8' => array('Rss','www.rss.com',array(
                    'img_1' => $link_img.'rss.png',
                    'img_2' => $link_img.'rss_1.png',
                    'img_3' => $link_img.'rss_2.png',
                    'img_4' => $link_img.'rss_3.png'))
             );

  for($q=1; $q<=8; $q++){

    $options[] = array(
    'name' => 'Social Icons '.$q,
    'type' => 'toggle'
  );
    $options[] = array(
        'desc' =>'Display on Footer, default false',
        'id' => 'show_footer_4'.$q,
        'std' => '0',
        'type' => 'checkbox'
          );
    $options[] = array(
            'id' => 'footer_social_link_4'.$q,
            'desc' => 'Link',
            'type' => 'text'
      );

    $options[] = array(
                'id' => 'footer_social_img_4'.$q,
                'desc' => $social_n[$q][0].' image style',
                'std' => 'img_1',
                'options' => $social_n[$q][2],
                'type' => 'images'
         );
      $options[] = array(
        'type' => 'toggle-close'
      );


  }
*/
    
  $options[] = array(
    'name' => 'Zone 4',
    'type' => 'toggle-close'
  );
  $options[] = array(
    'name' => 'Credits',
    'type' => 'toggle'
    );
  $options[] = array(
        'desc' =>'Show Rights, default true',
        'id' => 'show_footer_rights',
        'std' => '1',
        'type' => 'checkbox'
          );

  $options[] = array(
            'id' => 'rigths',
            'desc' => 'Rights',
            'std' => 'All rights reserved. Gym Website by',
            'type' => 'text'

  );

  $options[] = array(
            'id' => 'company',
            'desc' => 'Company',
            'std' => 'Company',
            'type' => 'text'

  );

  $options[] = array(
            'id' => 'link',
            'desc' => 'Url',
            'std' => 'themeforest.com',
            'type' => 'text'

  );
  $options[] = array(
    'type' => 'toggle-close'
  );

?>
