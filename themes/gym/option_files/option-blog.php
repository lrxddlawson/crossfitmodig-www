<?php

    $options[] = array(
        'name' => ('Blog and Post Options'),
        'type' => 'heading',
        'std' => 'comments'
    );

    $options[] = array(
                'name' => 'Blog Zone',
                'type' => 'toggle');
				
			$options[] = array(
                'name' =>'Show Banner',
                'id' => 'show_banner_blog',
                'desc' => 'Show Banner',
                'std' => 1,
                'type' => 'checkbox'
            );

     $options[] = array(
                'id' => 'blog_banner_text',
                'desc' => 'Banner Text',
                'std' => 'NO PRESSURE, NO DIAMONDS',
                'type' => 'text'
     );
      $options[] = array(
                    'id' => 'blog_banner_image',
                    'desc' => 'Load Imagen',
                    'type' => 'upload'
         );

        $options[] = array(
                'name' =>'Show Breadcumbs',
                'id' => 'show_breadcumbs_blog',
                'desc' => 'Show Breadcumbs',
                'std' => 1,
                'type' => 'checkbox'
            );

            $options[] = array(
                'id' => 'blog_title',
                'desc' => 'Blog title',
                'type' => 'text',
                'std' => 'Blog',
                'class' => 'text'
          );
          $options[] = array(
                'id' => 'layer_img',
                'std' => 'right',
                'options' => array(
                    'left' => get_template_directory_uri().'/img/sidebar-left.png',
                    'right' => get_template_directory_uri().'/img/sidebar-right.png',
                    'none' => get_template_directory_uri().'/img/full-width.png'
                ),
                'type' => 'images'
            );
       $options[] = array(  
            'type' => 'toggle-close');
            
       $options[] = array(
                'name' => 'Single Post Zone',
                'type' => 'toggle'
       );
	   
	   $options[] = array(
                'name' =>'Show Banner',
                'id' => 'show_banner_post',
                'desc' => 'Show Banner',
                'std' => 1,
                'type' => 'checkbox'
            );

     $options[] = array(
                'id' => 'post_banner_text',
                'desc' => 'Banner Text',
                'std' => 'NO PRESSURE, NO DIAMONDS',
                'type' => 'text'
     );
      $options[] = array(
                    'id' => 'post_banner_image',
                    'desc' => 'Load Imagen',
                    'type' => 'upload'
         );

        $options[] = array(
                'name' =>'Show Breadcumbs',
                'id' => 'show_breadcumbs_post',
                'desc' => 'Show Breadcumbs',
                'std' => 1,
                'type' => 'checkbox'
            );
	        $options[] = array(
                'id' => 'single_title',
                'desc' => 'Single title',
                'type' => 'text',
                'std' => 'Post',
                'class' => 'text'
            );
            $options[] = array(
                'id' => 'layer_img_single',
                'std' => 'right',
                'options' => array(
                    'left' => get_template_directory_uri().'/img/sidebar-left.png',
                    'right' => get_template_directory_uri().'/img/sidebar-right.png',
                    'none' => get_template_directory_uri().'/img/full-width.png'
                ),
                'type' => 'images'
            );         
       $options[] = array(
                'name' => 'Single Post Zone',
                'type' => 'toggle-close');
?>