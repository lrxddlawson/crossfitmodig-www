<?php

    $options[] = array(
        'name' => ('Single Class'),
        'type' => 'heading');


     $options[] = array(
                'name' => 'Banner',
                'type' => 'toggle'
     );
       $options[] = array(
                'name' =>'Show Banner',
                'id' => 'show_banner_single',
                'desc' => 'Show Banner',
                'std' => 1,
                'type' => 'checkbox'
            );

     $options[] = array(
                'id' => 'single_banner_text',
                'desc' => 'Banner Text',
                'std' => 'NO PRESSURE, NO DIAMONDS',
                'type' => 'text'
     );
      $options[] = array(
                    'id' => 'single_banner_image',
                    'desc' => 'Load Imagen',
                    'type' => 'upload'
         );

        $options[] = array(
                'name' =>'Show Breadcumbs',
                'id' => 'show_breadcumbs_single',
                'desc' => 'Show Breadcumbs',
                'std' => 1,
                'type' => 'checkbox'
            );


 $options[] = array(

   'type' => 'toggle-close');

 $options[] = array(

            'name' => 'Header Zone',
            'type' => 'toggle');

$options[] = array(
                'id' => 'single_title',
                'desc' => 'single title',
                'type' => 'text',
                'std' => 'News',
                'class' => 'text'
          );
 $options[] = array(

            'name' => 'Image',
            'type' => 'toggle');

$options[] = array(
            'id' => 'num_img',
            'type' => 'text',
            'desc' => 'Number of Image',
            'std' => '2',
      );


      if(of_get_option('num_img') > 0? $cant_tag = of_get_option('num_img'):$cant_tag=2);
      for($i=1; $i<=$cant_tag; $i++){

          $options[] = array(

            'name' => 'Image '.$i,
            'type' => 'toggle');

          $options[] = array(
                'id' => 'single_ti_'.$i,
                'desc' => 'Image '.$i,
                'type' => 'upload',
                'class' => 'text'
          );

          $options[] = array(

   'type' => 'toggle-close');
      }

       $options[] = array(

   'type' => 'toggle-close');

        $options[] = array(

            'name' => 'Paragraph',
            'type' => 'toggle');

    $options[] = array(
                'id' => 'single_title_2',
                'desc' => 'Header Second Title',
                'type' => 'text',
                'std' => 'single',
                'class' => 'text'
          );

    $options[] = array(
            'id' => 'single_num_pa',
            'type' => 'text',
            'desc' => 'Number of Paragraph',
            'std' => '2',
      );


      if(of_get_option('single_num_pa') > 0? $cant_tag = of_get_option('single_num_pa'):$cant_tag=2);
      for($i=1; $i<=$cant_tag; $i++){

          $options[] = array(

            'name' => 'Paragraph '.$i,
            'type' => 'toggle');

            $options[] = array(
                'id' => 'single_pa_'.$i,
                'desc' => 'Paragraph '.$i,
                'type' => 'textarea',
                'class' => 'textarea'
          );

          $options[] = array(

   'type' => 'toggle-close');
      }

 $options[] = array(

   'type' => 'toggle-close');

       $options[] = array(

            'name' => 'Details',
            'type' => 'toggle');

    $options[] = array(
                'id' => 'single_title_3',
                'desc' => 'Detail Principal Title',
                'type' => 'text',
                'std' => 'single',
                'class' => 'text'
          );

    $options[] = array(
                'id' => 'single_title_4',
                'desc' => 'Detail Second Title',
                'type' => 'text',
                'std' => 'single',
                'class' => 'text'
          );

    $options[] = array(
            'id' => 'single_num_det',
            'type' => 'text',
            'desc' => 'Number of Details',
            'std' => '2',
      );


      if(of_get_option('single_num_det') > 0? $cant_tag = of_get_option('single_num_det'):$cant_tag=2);
      for($i=1; $i<=$cant_tag; $i++){

          $options[] = array(

            'name' => 'Detail '.$i,
            'type' => 'toggle');

            $options[] = array(
                'id' => 'single_pa_'.$i,
                'desc' => 'Detail '.$i,
                'type' => 'text',
                'class' => 'textarea'
          );

          $options[] = array(

   'type' => 'toggle-close');
      }

 $options[] = array(

   'type' => 'toggle-close');

$options[] = array(

            'name' => 'Clases',
            'type' => 'toggle');


          $options[] = array(

   'type' => 'toggle-close');
          $options[] = array(

            'name' => 'Accordion',
            'type' => 'toggle');


          $options[] = array(

   'type' => 'toggle-close');
          $options[] = array(

            'name' => 'Testimonial',
            'type' => 'toggle');


          $options[] = array(

   'type' => 'toggle-close');
       $options[] = array(

   'type' => 'toggle-close');
?>