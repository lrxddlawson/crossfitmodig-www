<?php
    $options[] = array(
        'name' => 'Price Options',
        'type' => 'heading',
        'std' => 'dollar'
    );
    
    $options[] = array(
            'id' => 'show_price',
            'desc' => 'Show Style Configuration',
            'std' => 1,
            'type' => 'checkbox'
    );
    
    $options[] = array(
                'name' => 'General Options',
                'type' => 'toggle'
     );
        $options[] = array(
                'id' => 'show_banner_price',
                'desc' => 'Show Banner',
                'std' => 1,
                'type' => 'checkbox'
        );
        $options[] = array(
                'id' => 'price_banner_text',
                'desc' => 'Banner Text',
                'std' => 'NO PRESSURE, NO DIAMONDS',
                'type' => 'text'
        );
        $options[] = array(
                'id' => 'price_banner_image',
                'desc' => 'Load Imagen',
                'type' => 'upload'
         );
        $options[] = array(
                'id' => 'show_breadcumbs_price',
                'desc' => 'Show Breadcumbs',
                'std' => 1,
                'type' => 'checkbox'
        );
        
     $options[] = array(
            'name' => 'General Options',
            'type' => 'toggle-close'
     );
     
     $options[] = array(
        'name' => 'First Zone',
        'type' => 'toggle'
    );
        $options[] = array(
            'id' => 'show_price_first',
            'desc' => 'Show Style Configuration',
            'std' => 1,
            'type' => 'checkbox'
        );
        $options[] = array(
                'id' => 'price_first_title',
                'desc' => 'Subscribe title',
                'type' => 'text',
                'std' => 'Title Text',
                'class' => 'text'
        );
        $options[] = array(
            'id' => 'price_first_style',
            'desc' => 'Style of Plan',
            'std' => '1',
            'options' => array(
                '1' => 'Standar',
                '2' => 'Special'
            ),
            'type' => 'radio',
            'class' => 'side'
        );
        
        $args=array(
            'post_type' => 'plan',
            'post_status' => 'publish',
            'posts_per_page' => -1,
        );
        $my_query = null;
        $my_query = new WP_Query($args);
        $plan = array();
        
        while ($my_query->have_posts()) {
            $my_query->the_post();
            $plan[] = array(
                'title' => get_the_title(),
                'price' => get_field('price'),
                'freq' => get_field('frequency'),
                'data' => get_field('other_data'),
                'btn' => get_field('button')
            );
        }
        $c = 0;
        foreach($plan as $key){
            $c++;
            $options[] = array(
                'id' => 'price_first_plan'.$c,
                'desc' => 'Plan: '.$key['title'],
                'std' => 0,
                'type' => 'checkbox'
            );
        }
    $options[] = array(
        'name' => 'First Zone',
        'type' => 'toggle-close'
    );
    
    $options[] = array(
        'name' => 'Second Zone',
        'type' => 'toggle'
    );
        $options[] = array(
            'id' => 'show_price_second',
            'desc' => 'Show Style Configuration',
            'std' => 1,
            'type' => 'checkbox'
        );
        $options[] = array(
            'id' => 'price_second_title',
            'desc' => 'Title',
            'std' => 'Title Text',
            'type' => 'text'
        );
        $options[] = array(
            'id' => 'price_second_desc',
            'desc' => 'Description',
            'std' => 'Description Text',
            'type' => 'text'
        );
        $options[] = array(
            'id' => 'price_second_btn',
            'desc' => 'Button Text',
            'std' => 'Button Text',
            'type' => 'text'
        );
        $options[] = array(
            'id' => 'price_second_btn_link',
            'desc' => 'Button Link',
            'std' => 'Button Link',
            'type' => 'text'
        );
    $options[] = array(
        'name' => 'Second Zone',
        'type' => 'toggle-close'
    );
    
    $options[] = array(
        'name' => 'Third Zone',
        'type' => 'toggle'
    );
        $options[] = array(
            'id' => 'show_price_third',
            'desc' => 'Show Style Configuration',
            'std' => 1,
            'type' => 'checkbox'
        );
        $options[] = array(
                'id' => 'price_third_title',
                'desc' => 'Subscribe title',
                'type' => 'text',
                'std' => 'Title Text',
                'class' => 'text'
        );
        $options[] = array(
            'id' => 'price_third_style',
            'desc' => 'Style of Plan',
            'std' => '1',
            'options' => array(
                '1' => 'Standar',
                '2' => 'Special'
            ),
            'type' => 'radio',
            'class' => 'side'
        );
        
        $c = 0;
        foreach($plan as $key){
            $c++;
            $options[] = array(
                'id' => 'price_third_plan'.$c,
                'desc' => 'Plan: '.$key['title'],
                'std' => 0,
                'type' => 'checkbox'
            );
        }
        
    $options[] = array(
        'name' => 'Third Zone',
        'type' => 'toggle-close'
    );
?>