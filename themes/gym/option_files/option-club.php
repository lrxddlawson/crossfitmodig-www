<?php

    $options[] = array(
        'name' => ('Club Options'),
        'type' => 'heading',
        'std' => 'trophy'
    );

     $options[] = array(
                'name' => 'Banner',
                'type' => 'toggle'
     );
       $options[] = array(
                'name' =>'Show Banner',
                'id' => 'show_banner_club',
                'desc' => 'Show Banner',
                'std' => 1,
                'type' => 'checkbox'
            );

     $options[] = array(
                'id' => 'club_banner_text',
                'desc' => 'Banner Text',
                'std' => 'NO PRESSURE, NO DIAMONDS',
                'type' => 'text'
     );
      $options[] = array(
                    'id' => 'club_banner_image',
                    'desc' => 'Load Imagen',
                    'type' => 'upload'
         );

        $options[] = array(
                'name' =>'Show Breadcumbs',
                'id' => 'show_breadcumbs_club',
                'desc' => 'Show Breadcumbs',
                'std' => 1,
                'type' => 'checkbox'
            );


 $options[] = array(

   'type' => 'toggle-close');

 $options[] = array(

            'name' => 'Club Zone',
            'type' => 'toggle');

$options[] = array(
                'id' => 'club_title',
                'desc' => 'Club title',
                'type' => 'text',
                'std' => 'Club',
                'class' => 'text'
          );
 $options[] = array(

            'name' => 'Zone 1',
            'type' => 'toggle');

$options[] = array(
            'id' => 'club_num_pa',
            'type' => 'text',
            'desc' => 'Number of Paragraph',
            'std' => '2',
      );

$options[] = array(
                'id' => 'club_title_1',
                'desc' => 'Club Second Title',
                'type' => 'text',
                'std' => 'Club',
                'class' => 'text'
          );
          $options[] = array(
                    'id' => 'club_zone_image',
                    'desc' => 'Load Imagen',
                    'type' => 'upload'
         );
         $options[] = array(
                    'id' => 'club_zone_img_pos',
                    'desc' => 'Image Position',
                    'options' => array(
                        '1' => 'left',
                        '2' => 'right'
                    ),
                    'class' => 'side',
                    'type' => 'radio'
                );
      if(of_get_option('club_num_pa') > 0? $cant_tag = of_get_option('club_num_pa'):$cant_tag=2);
      for($i=1; $i<=$cant_tag; $i++){

          $options[] = array(
            'name' => 'Paragraph '.$i,
            'type' => 'toggle');

          $options[] = array(
                'id' => 'club_pa_'.$i,
                'desc' => 'Paragraph '.$i,
                'type' => 'textarea',
                'std' => 'Paragraph '.$i,
                'class' => 'text'
          );

      $options[] = array( 
            'name' => 'Paragraph '.$i,      
            'type' => 'toggle-close');
      }

       $options[] = array(
        'name' => 'Zone 1',
   'type' => 'toggle-close');

        $options[] = array(

            'name' => 'Zone 3',
            'type' => 'toggle');

      $options[] = array(
            'id' => 'club_num_tabs',
            'type' => 'text',
            'desc' => 'Number of Tabs',
            'std' => '2',
      );
      $skins = array(
            '1' => 'Green',
            '2' => 'White'
            );

      $options[] = array(
                'name' => 'Tabs Fill color',
                'type' => 'toggle'
     );

         $options[] = array(
                'id' => 'style',
                'options' => $skins,
                'type' => 'select',
                'class' => 'medium'

         );
        $options[] = array(
                'type' => 'toggle-close'
     );


      if(of_get_option('club_num_tabs') > 0? $cant_tabs = of_get_option('club_num_tabs'):$cant_tabs=2);
      for($i=1; $i<=$cant_tabs; $i++){

          $options[] = array(

            'name' => 'Tabs '.$i,
            'type' => 'toggle');

            $options[] = array(
                'id' => 'club_tab_'.$i,
                'desc' => 'Title '.$i,
                'type' => 'text',
                'std' => 'Title '.$i,
                'class' => 'text'
          );


            $options[] = array(
                'id' => 'club_ta_'.$i,
                'desc' => 'Text '.$i,
                'type' => 'textarea',
                'std' => 'Text ',
                'class' => 'text'
          );

          $options[] = array(

   'type' => 'toggle-close');
      }


       $options[] = array(

   'type' => 'toggle-close');


 $options[] = array(

            'name' => 'Zone 4',
            'type' => 'toggle');

$options[] = array(
            'id' => 'club_num_pa2',
            'type' => 'text',
            'desc' => 'Number of Paragraph',
            'std' => '2',
      );

$options[] = array(
                'id' => 'club_title_3',
                'desc' => 'Club Second Title',
                'type' => 'text',
                'std' => 'Club',
                'class' => 'text'
          );

      if(of_get_option('club_num_pa2') > 0? $cant_tag = of_get_option('club_num_pa2'):$cant_tag=2);
      for($i=1; $i<=$cant_tag; $i++){

          $options[] = array(

            'name' => 'Paragraph '.$i,
            'type' => 'toggle');

          $options[] = array(
                'id' => 'club_pa2_'.$i,
                'desc' => 'Paragraph '.$i,
                'type' => 'textarea',
                'std' => 'Paragraph '.$i,
                'class' => 'text'
          );

          $options[] = array(

   'type' => 'toggle-close');
      }

       $options[] = array(

   'type' => 'toggle-close');

       $options[] = array(

   'type' => 'toggle-close');

          $options[] = array(

            'name' => 'Sponsor',
            'type' => 'toggle');

           $options[] = array(
                'name' =>'Show Sponsor',
                'id' => 'show_sponsor_club',
                'desc' => 'Show Sponsor',
                'std' => 1,
                'type' => 'checkbox'
            );



       $options[] = array(

   'type' => 'toggle-close');
?>