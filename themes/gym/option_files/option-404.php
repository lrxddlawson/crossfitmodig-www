<?php
  $options[] = array(
        'name' => '404 Options',
        'type' => 'heading',
        'std' => 'remove'
  );

  $options[] = array(
                'name' => 'Banner',
                'type' => 'toggle'
     );
       $options[] = array(
                'name' =>'Show Banner',
                'id' => 'show_banner_404',
                'desc' => 'Show Banner',
                'std' => 1,
                'type' => 'checkbox'
            );

     $options[] = array(
                'id' => '404_banner_text',
                'desc' => 'Banner Text',
                'std' => 'NO PRESSURE, NO DIAMONDS',
                'type' => 'text'
     );
      $options[] = array(
                    'id' => '404_banner_image',
                    'desc' => 'Load Imagen',
                    'type' => 'upload'
         );

        $options[] = array(
                'name' =>'Show Breadcumbs',
                'id' => 'show_breadcumbs_404',
                'desc' => 'Show Breadcumbs',
                'std' => 1,
                'type' => 'checkbox'
            );


 $options[] = array(

   'type' => 'toggle-close');

  $options[] = array(
                'name' => 'Content',
                'type' => 'toggle'
     );

  /*****************/
    $options[] = array(
        'id' => '404_title',
        'desc' => '404 Title',
        'std' => '',
        'type' => 'text'
  );
  $options[] = array(
        'id' => '404_text',
        'desc'=> '404 Text',
        'std' => '',
        'type' => 'text'
  );

  $options[] = array(
        'id' => '404_subtitle',
         'desc'=> '404 Secundary Title',
        'std' => '',
        'type' => 'text'
  );
  $options[] = array(
        'id' => '404_explain',
         'desc'=> '404 Secundary Text',
        'std' => '',
        'type' => 'text'
  );
  $options[] = array(
        'id' => '404_botton',
        'desc'=> '404 Text Botton',
        'std' => '',
        'type' => 'text'
  );


 $options[] = array(

   'type' => 'toggle-close');

?>
