<?php

    $options[] = array(
        'name' => ('Tabs Options'),
        'type' => 'heading',
        'std' => 'wrench'
    );


     $options[] = array(
                'name' => 'Tabs Options',
                'type' => 'toggle'
     );
        $options[] = array(
                'name' =>'Show Banner',
                'id' => 'show_banner_tab',
                'desc' => 'Show Banner',
                'std' => 1,
                'type' => 'checkbox'
        );
        $options[] = array(
                'id' => 'tab_banner_text',
                'desc' => 'Banner Text',
                'std' => 'NO PRESSURE, NO DIAMONDS',
                'type' => 'text'
        );
        $options[] = array(
                'id' => 'tab_banner_image',
                'desc' => 'Load Imagen',
                'type' => 'upload'
         );
        $options[] = array(
                'name' =>'Show Breadcumbs',
                'id' => 'show_breadcumbs_tab',
                'desc' => 'Show Breadcumbs',
                'std' => 1,
                'type' => 'checkbox'
        );
               
          $options[] = array(
                'name' => 'Sponsor Zone',
                'type' => 'info'
          );
              $options[] = array(
                    'name' =>'Show Sponsor',
                    'id' => 'show_sponsor_tab',
                    'desc' => 'Show Sponsor',
                    'std' => 1,
                    'type' => 'checkbox'
              );
     $options[] = array(
            'name' => 'Tabs Options',
            'type' => 'toggle-close'
     );

     $options[] = array(
            'name' => 'Tabs',
            'type' => 'toggle'
     );
        $options[] = array(
                'id' => 'show_tab',
                'desc' => 'Show Tabs',
                'std' => 1,
                'type' => 'checkbox'
        );
        $options[] = array(
            'id' => 'tab_title',
            'desc' => 'Title Text',
            'std' => 'Title Text',
            'type' => 'text'
        );
        $options[] = array(
            'id' => 'tab_num',
            'desc' => 'Number of Tabs Zones',
            'std' => '2',
            'type' => 'text',
            'class' => 'mini'
        );
        if(of_get_option('tab_num')?$cant=of_get_option('tab_num'):$cant=2);
        for($i=1; $i<=$cant; $i++){
            $options[] = array(
                'name' => 'Tab Zone '.$i,
                'type' => 'toggle'
            );
                $options[] = array(
                    'id' => 'tab_back_tab'.$i,
                    'desc' => 'Background Enable',
                    'std' => 1,
                    'type' => 'checkbox'
                );
                $options[] = array(
                    'id' => 'tab_num_tab'.$i,
                    'desc' => 'Number of Tabs',
                    'std' => '4',
                    'type' => 'text'
                );
                if(of_get_option('tab_num_tab'.$i)?$cant1=of_get_option('tab_num_tab'.$i):$cant1=4);
                for($j=1; $j<=$cant1; $j++){
                    $options[] = array(
                        'name' => 'Tab '.$j,
                        'type' => 'info'
                    );
                    $options[] = array(
                        'id' => 'tab_tab_title'.$i.$j,
                        'desc' => 'Title',
                        'std' => 'Title Text',
                        'type' => 'text'
                    );
                    $options[] = array(
                        'id' => 'tab_tab_desc'.$i.$j,
                        'desc' => 'Decription',
                        'std' => 'Description Text',
                        'type' => 'textarea'
                    );
                }
            $options[] = array(
                'name' => 'Tab Zone '.$i,
                'type' => 'toggle-close'
            );
        }
     $options[] = array(
            'name' => 'Tabs',
            'type' => 'toggle-close'
     );
     
?>