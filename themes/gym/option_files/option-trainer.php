<?php

    $options[] = array(
        'name' => ('Trainers Option'),
        'type' => 'heading',
        'std' => 'bike-sign'
    );
     $options[] = array(
                'name' => 'All Trainer Options',
                'type' => 'toggle'
     );
        $options[] = array(
                'name' =>'Show Banner',
                'id' => 'show_banner_trainer',
                'desc' => 'Show Banner',
                'std' => 1,
                'type' => 'checkbox'
        );
        $options[] = array(
                'id' => 'trainer_banner_text',
                'desc' => 'Banner Text',
                'std' => 'NO PRESSURE, NO DIAMONDS',
                'type' => 'text'
        );
        $options[] = array(
                'id' => 'trainer_banner_image',
                'desc' => 'Load Imagen',
                'type' => 'upload'
         );
        $options[] = array(
                'name' =>'Show Breadcumbs',
                'id' => 'show_breadcumbs_trainer',
                'desc' => 'Show Breadcumbs',
                'std' => 1,
                'type' => 'checkbox'
        );
        $options[] = array(
                'id' => 'trainer_all_title',
                'desc' => 'Subscribe title',
                'type' => 'text',
                'std' => 'All our Trainers',
                'class' => 'text'
        );
        $options[] = array(
                'name' => 'Midle Zone',
                'type' => 'info'
        );
            $options[] = array(
                    'name' =>'Show Subscribe',
                    'id' => 'show_subscribe_trainer',
                    'desc' => 'Show Subscribe Zone',
                    'std' => 1,
                    'type' => 'checkbox'
                );
               $options[] = array(
                    'id' => 'trainer_subscribe_title',
                    'desc' => 'Subscribe title',
                    'type' => 'text',
                    'std' => 'Classes at the Club',
                    'class' => 'text'
              );
              $options[] = array(
                    'id' => 'trainer_subscribe_text',
                    'desc' => 'Subscribe title',
                    'type' => 'textarea',
                    'std' => 'Ut a molestie urna. Vestibulum malesuada, diam ut mollis varius, lorem urna euismod purus, ut varius est erat a orci. Sed ultricies elit eget venenatis tristique. Sed tempus egestas ante, vel ullamcorper nulla ultricies at.',
                    'class' => 'text'
              );
             $options[] = array(
                    'id' => 'trainer_subscribe_signal',
                    'desc' => 'Subscribe title',
                    'type' => 'text',
                    'std' => 'suscribe right now',
                    'class' => 'text'
              );
			  $options[] = array(
                'id' =>'trainer_subscribe_link',
                'desc' => 'Subscribe Link',
                'std' => 'URL',
                'type' => 'text'
        );
          $options[] = array(
                'name' => 'Class Gallery Zone',
                'type' => 'info'
          );
              $options[] = array(
                    'id' => 'trainer_class_title',
                    'desc' => 'Title',
                    'type' => 'text',
                    'std' => 'Title Text'
              );
          $options[] = array(
                'name' => 'Sponsor Zone',
                'type' => 'info'
          );
              $options[] = array(
                    'id' => 'show_sponsor_trainer1',
                    'desc' => 'Show Sponsor',
                    'std' => 1,
                    'type' => 'checkbox'
              );
     $options[] = array(
            'name' => 'Trainer Gallery Otions',
            'type' => 'toggle-close'
     );

     $options[] = array(
            'name' => 'Single Trainer Options',
            'type' => 'toggle'
     );
        $options[] = array(
                'name' =>'Show Banner',
                'id' => 'show_banner_single_trainer',
                'desc' => 'Show Banner',
                'std' => 1,
                'type' => 'checkbox'
        );
        $options[] = array(
                'id' => 'trainer_banner_single_text',
                'desc' => 'Banner Text',
                'std' => 'NO PRESSURE, NO DIAMONDS',
                'type' => 'text'
        );
        $options[] = array(
                'id' => 'trainer_banner_single_image',
                'desc' => 'Load Imagen',
                'type' => 'upload'
         );
        $options[] = array(
                'name' =>'Show Breadcumbs',
                'id' => 'show_breadcumbs_single_trainer',
                'desc' => 'Show Breadcumbs',
                'std' => 1,
                'type' => 'checkbox'
        );
        $options[] = array(
                'id' => 'trainer_all_single_title',
                'desc' => 'Subscribe title',
                'type' => 'text',
                'std' => 'All our Trainers',
                'class' => 'text'
        );
        $options[] = array(
                'name' => 'Sponsor Zone',
                'type' => 'info'
          );
              $options[] = array(
                    'id' => 'show_sponsor_single_trainer1',
                    'desc' => 'Show Sponsor',
                    'std' => 1,
                    'type' => 'checkbox'
              );
     $options[] = array(
            'name' => 'Single Trainer Options',
            'type' => 'toggle-close'
     );
  
?>
