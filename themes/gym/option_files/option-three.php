<?php

    $options[] = array(
        'name' => ('Three Gallery Options'),
        'type' => 'heading',
        'std' => 'th'
    );

     $options[] = array(
                'name' => 'Banner',
                'type' => 'toggle'
     );
       $options[] = array(
                'name' =>'Show Banner',
                'id' => 'show_banner_three',
                'desc' => 'Show Banner',
                'std' => 1,
                'type' => 'checkbox'
            );

     $options[] = array(
                'id' => 'three_banner_text',
                'desc' => 'Banner Text',
                'std' => 'NO PRESSURE, NO DIAMONDS',
                'type' => 'text'
     );
      $options[] = array(
                    'id' => 'three_banner_image',
                    'desc' => 'Load Imagen',
                    'type' => 'upload'
         );

        $options[] = array(
                'name' =>'Show Breadcumbs',
                'id' => 'show_breadcumbs_three',
                'desc' => 'Show Breadcumbs',
                'std' => 1,
                'type' => 'checkbox'
            );


 $options[] = array(

   'type' => 'toggle-close');

 $options[] = array(

            'name' => 'Gallery Zone',
            'type' => 'toggle');

$options[] = array(
                'id' => 'three_title',
                'desc' => 'Gallery title',
                'type' => 'text',
                'std' => 'Imagen',
                'class' => 'text'
          );
$options[] = array(
            'id' => 'three_num_cant',
            'type' => 'text',
            'desc' => 'Number of Image',
            'std' => '2',
      );


      if(of_get_option('three_num_cant') > 0? $cant_memb = of_get_option('three_num_cant'):$cant_memb=2);
      for($i=1; $i<=$cant_memb; $i++){

          $options[] = array(

            'name' => 'Image '.$i,
            'type' => 'toggle');

         $options[] = array(
                    'id' => 'three_image'.$i,
                    'desc' => 'Load Imagen',
                    'type' => 'upload'
       );


          $options[] = array(

   'type' => 'toggle-close');
      }


       $options[] = array(

   'type' => 'toggle-close');

          $options[] = array(

            'name' => 'Sponsor',
            'type' => 'toggle');


           $options[] = array(
                'name' =>'Show Sponsor',
                'id' => 'show_sponsor_three',
                'desc' => 'Show Sponsor',
                'std' => 1,
                'type' => 'checkbox'
            );



       $options[] = array(

   'type' => 'toggle-close');
?>