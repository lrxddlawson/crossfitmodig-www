<?php
    $options[] = array(
        'name' => 'Home Options',
        'type' => 'heading',
        'std' => 'home'
    );
    
    $options[] = array(
            'id' => 'show_home',
            'desc' => 'Show Style Configuration',
            'std' => 1,
            'type' => 'checkbox'
    );
    $options[] = array(
            'name' =>'Zone 1 Link',
            'type' => 'toggle'
    );
        $options[] = array(
                'id' => 'show_home_z1',
                'desc' => 'Show Style Configuration',
                'std' => 1,
                'type' => 'checkbox'
        );
        $options[] = array(
                'id' =>'home_z1_title',
                'desc' => 'Title Phrase',
                'std' => 'Title Text',
                'type' => 'textarea'
        );
        $options[] = array(
                'id' =>'home_z1_desc',
                'desc' => 'Description',
                'std' => 'Description Text',
                'type' => 'textarea'
        );
        $options[] = array(
                'id' =>'home_z1_btn',
                'desc' => 'Button Text',
                'std' => 'Button Text',
                'type' => 'text'
        );
        $options[] = array(
                'id' =>'home_z1_btn_link',
                'desc' => 'Button Link',
                'std' => 'URL',
                'type' => 'text'
        );
    $options[] = array(
            'name' =>'Zone 1 Link',
            'type' => 'toggle-close'
    );
    
    $options[] = array(
            'name' =>'Zone 2 Gallery',
            'type' => 'toggle'
    );
        $options[] = array(
                'id' => 'show_home_z2',
                'desc' => 'Show Style Configuration',
                'std' => 1,
                'type' => 'checkbox'
        );
        $options[] = array(
                'id' => 'home_z2_galleries',
                'desc' => 'Gralleries to show',
                'std' => 'classes',
                'options' => array(
                    'classes' => 'Classes List',
                    'trainer' => 'Trainers'
                ),
                'type' => 'radio',
                'class' => 'side'
        );
        $options[] = array(
                'id' => 'home_z2_btn',
                'desc' => 'Button Text',
                'std' => 'VIEW ALL',
                'type' => 'text'
        );
        $options[] = array(
                'id' => 'home_z2_btn_link',
                'desc' => 'Button Text',
                'std' => 'URL',
                'type' => 'text'
        );
        //FEATURES
        $options[] = array(
                'name' => 'Features Classes',
                'type' => 'info'
        );
        $options[] = array(
                'id' => 'home_z2_features',
                'desc' => 'Show Features Classes',
                'std' => 1,
                'type' => 'checkbox'
        ); 
    $options[] = array(
            'name' =>'Zone Gallery',
            'type' => 'toggle-close'
    );
    $options[] = array(
            'name' =>'Announcements',
            'type' => 'toggle'
    );
        $options[] = array(
                'id' => 'show_home_z3',
                'desc' => 'Show Style Configuration',
                'std' => 1,
                'type' => 'checkbox'
        );
        $options[] = array(
                'id' => 'home_z3_title',
                'desc' => 'Title',
                'std' => 'Title Text',
                'type' => 'text'
        );
        $options[] = array(
                'id' => 'home_z3_num',
                'desc' => 'Number of Announcements',
                'std' => '5',
                'type' => 'text',
                'class' => 'mini'
        );
        if(of_get_option('home_z3_num')?$cant=of_get_option('home_z3_num'):$cant=5);
        for($i=1; $i<=$cant; $i++){
            $options[] = array(
                    'name' => 'Announcements '.$i,
                    'type' => 'toggle'
            );
                $options[] = array(
                    'id' => 'home_z3_title'.$i,
                    'desc' => 'Title',
                    'std' => 'Title Text',
                    'type' => 'text'
                );
                $options[] = array(
                    'id' => 'home_z3_desc'.$i,
                    'desc' => 'Description',
                    'std' => 'Description Text',
                    'type' => 'textarea'
                );
                foreach(font_awesome_icon('home_z3_ico'.$i) as $val){
                    $options[] = $val;
                }
            $options[] = array(
                    'name' => 'Announcements '.$i,
                    'type' => 'toggle-close'
            );
        }
    $options[] = array(
            'name' =>'Announcements',
            'type' => 'toggle-close'
    );
    
    $options[] = array(
            'name' =>'Accordion',
            'type' => 'toggle'
    );
        $options[] = array(
                'id' => 'show_home_z4',
                'desc' => 'Show Style Configuration',
                'std' => 1,
                'type' => 'checkbox'
        );
        $options[] = array(
                'id' => 'home_z4_title',
                'desc' => 'Title',
                'std' => 'Title Text',
                'type' => 'text'
        );
        $options[] = array(
                'id' => 'home_z4_num',
                'desc' => 'Number of Accordion',
                'std' => '3',
                'type' => 'text',
                'class' => 'mini'
        );
        if(of_get_option('home_z4_num')?$cant=of_get_option('home_z4_num'):$cant=3);
        for($i=1; $i<=$cant; $i++){
             $options[] = array(
                    'name' => 'Accordion '.$i,
                    'type' => 'toggle'
            );
                $options[] = array(
                    'id' => 'home_z4_title'.$i,
                    'desc' => 'Title',
                    'std' => 'Title Text',
                    'type' => 'text'
                ); 
                $options[] = array(
                    'id' => 'home_z4_subtitle'.$i,
                    'desc' => 'Subtitle',
                    'std' => 'Subtitle Text',
                    'type' => 'text'
                );
                $options[] = array(
                    'id' => 'home_z4_desc'.$i,
                    'desc' => 'Description',
                    'std' => 'Description Text',
                    'type' => 'textarea'
                );
            $options[] = array(
                    'name' => 'Accordion '.$i,
                    'type' => 'toggle-close'
            );
        }
    $options[] = array(
            'name' =>'Accordion',
            'type' => 'toggle-close'
    );
        
    $options[] = array(
            'name' =>'Chronogram Table',
            'type' => 'toggle'
    );
        $options[] = array(
                'id' => 'show_home_z5',
                'desc' => 'Show Style Configuration',
                'std' => 1,
                'type' => 'checkbox'
        );
        $options[] = array(
                'id' => 'home_z5_title',
                'desc' => 'Title',
                'std' => 'Title Text',
                'type' => 'text'
        );
        $options[] = array(
                'id' => 'home_z5_numc',
                'desc' => 'Number of Columns',
                'std' => '8',
                'type' => 'text',
                'class' => 'mini'
        );
        $options[] = array(
                'id' => 'home_z5_numr',
                'desc' => 'Number of Rows',
                'std' => '1',
                'type' => 'text',
                'class' => 'mini'
        );
        $options[] = array(
            'name' => 'Column Title',
            'type' => 'toggle'
       );
        if(of_get_option('home_z5_numc')?$cant=of_get_option('home_z5_numc'):$cant=8);
        for($i=1; $i<=$cant; $i++){
            
                $options[] = array(
                    'id' => 'home_z5_ctitle'.$i,
                    'desc' => 'Column Title',
                    'std' => 'Column'.$i,
                    'type' => 'text'
                );
                $options[] = array(
                    'name' => ' ',
                    'type' => 'info'
                );
        }
        $options[] = array(
            'name' => 'Column Title',
            'type' => 'toggle-close'
       );
        if(of_get_option('home_z5_numc')?$cant=of_get_option('home_z5_numc'):$cant=8);
        for($i=1; $i<=$cant; $i++){
           $options[] = array(
                'name' => 'Column '.$i,
                'type' => 'toggle'
           );
               if(of_get_option('home_z5_numr')?$cantj=of_get_option('home_z5_numr'):$cantj=1);
               for($j=1; $j<=$cantj; $j++){
                   $options[] = array(
                        'id' => 'home_z5_field'.$i.$j,
                        'desc' => 'Column '.$i.' Row -'.$j.'-',
                        'std' => 'Null',
                        'type' => 'text'
                    );
                    $options[] = array(
                        'id' => 'home_z5_field_link'.$i.$j,
                        'desc' => 'Column '.$i.' Row -'.$j.'-',
                        'type' => 'text'
                    );
                    $options[] = array(
                        'id' => 'home_z5_field_color'.$i.$j,
                        'desc' => 'Color to Mark Field',
                        'type' => 'color'
                    );   
               }
           $options[] = array(
                'name' => 'Column '.$i,
                'type' => 'toggle-close'
           ); 
        }
    $options[] = array(
            'name' =>'Chronogram Table',
            'type' => 'toggle-close'
    );
    
    $options[] = array(
        'name' => 'Sponsor',
        'type' => 'toggle'
    );
       $options[] = array(
            'name' =>'Show Sponsor',
            'id' => 'show_home_sponsor',
            'desc' => 'Show Sponsor',
            'std' => 1,
            'type' => 'checkbox'
        );
    $options[] = array(
        'name' => 'Sponsor',
        'type' => 'toggle-close'
    );
?>