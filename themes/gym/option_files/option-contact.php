<?php
    $options[] = array(
        'name' => 'Contact Options',
        'type' => 'heading',
        'std' => 'envelope'
    );
    
    $options[] = array(
            'id' => 'show_contact1',
            'desc' => 'Show Style Configuration',
            'std' => 1,
            'type' => 'checkbox'
    );
    
    $options[] = array(
                'name' => 'General Options',
                'type' => 'toggle'
     );
        $options[] = array(
                'id' => 'show_banner_contact',
                'desc' => 'Show Banner',
                'std' => 1,
                'type' => 'checkbox'
        );
        $options[] = array(
                'id' => 'contact_banner_text',
                'desc' => 'Banner Text',
                'std' => 'NO PRESSURE, NO DIAMONDS',
                'type' => 'text'
        );
        $options[] = array(
                'id' => 'contact_banner_image',
                'desc' => 'Load Imagen',
                'type' => 'upload'
         );
        $options[] = array(
                'id' => 'show_breadcumbs_contact',
                'desc' => 'Show Breadcumbs',
                'std' => 1,
                'type' => 'checkbox'
        );
        $options[] = array(
            'name' => 'Sponsor Zone',
            'type' => 'info'
        );
            $options[] = array(
                'id' => 'show_sponsor_contact',
                'desc' => 'Show Sponsor',
                'std' => 1,
                'type' => 'checkbox'
            );
     $options[] = array(
            'name' => 'General Options',
            'type' => 'toggle-close'
     );
    
    $options[] = array(
        'name' => 'First Zone',
        'type' => 'toggle'
    );
        $options[] = array(
                'id' => 'show_contact_first',
                'desc' => 'Show Style Configuration',
                'std' => 1,
                'type' => 'checkbox'
        );
        $options[] = array(
                'id' => 'contact_title',
                'desc' => 'Subscribe title',
                'type' => 'text',
                'std' => 'Title Text',
                'class' => 'text'
        );
         $options[] = array(
            'name' => 'Map',
            'type' => 'toggle'
        );
            $options[] = array(
                'id' => 'show_contact_map',
                'desc' => 'Show Style Configuration',
                'std' => 1,
                'type' => 'checkbox'
            );
            $options[] = array(
                'id' => 'contact_map',
                'desc' => 'Location',
                'std' => 'http://',
                'type' => 'text'
            );
        $options[] = array(
            'name' => 'Map',
            'type' => 'toggle-close'
        );
        $options[] = array(
            'name' => 'Formulaire',
            'type' => 'toggle'
        );
            $options[] = array(
                'id' => 'show_contact_form',
                'desc' => 'Show Style Configuration',
                'std' => 1,
                'type' => 'checkbox'
            );
            $options[] = array(
                'id' => 'contact_form_name',
                'desc' => 'Name',
                'std' => 'Name Text',
                'type' => 'text'
            );
            foreach(font_awesome_icon('name_icon') as $val){
                $options[] = $val;
            }
            $options[] = array(
                'id' => 'contact_form_mail',
                'desc' => 'E-Mail',
                'std' => 'E-Mail Text',
                'type' => 'text'
            );
            foreach(font_awesome_icon('mail_icon') as $val){
                $options[] = $val;
            }
            $options[] = array(
                'id' => 'contact_form_subject',
                'desc' => 'Subject',
                'std' => 'Subject Text',
                'type' => 'text'
            );
            foreach(font_awesome_icon('subject_icon') as $val){
                $options[] = $val;
            }
            $options[] = array(
                'id' => 'contact_form_msg',
                'desc' => 'Message',
                'std' => 'Message Text',
                'type' => 'text'
            );
            foreach(font_awesome_icon('msg_icon') as $val){
                $options[] = $val;
            }
            $options[] = array(
                'id' => 'contact_form_btn',
                'desc' => 'Button',
                'std' => 'Button Text',
                'type' => 'text'
            );
            
        $options[] = array(
            'name' => 'Formulaire',
            'type' => 'toggle-close'
        );
    $options[] = array(
        'name' => 'First Zone',
        'type' => 'toggle-close'
    );
    $options[] = array(
        'name' => 'Second Zone',
        'type' => 'toggle'
    );
        $options[] = array(
            'id' => 'show_contact_second',
            'desc' => 'Show Style Configuration',
            'std' => 1,
            'type' => 'checkbox'
        );
        $options[] = array(
            'name' => 'Left Zone',
            'type' => 'info'
        );
            $options[] = array(
                'id' => 'show_contact_second_l',
                'desc' => 'Show Style Configuration',
                'std' => 1,
                'type' => 'checkbox'
            );
            $options[] = array(
                'id' => 'contact_second_l_title',
                'desc' => 'Title',
                'std' => 'Title Text',
                'type' => 'text'
            ); 
            $options[] = array(
                'id' => 'contact_second_l_desc',
                'desc' => 'Description',
                'std' => 'Description Text',
                'type' => 'textarea'
            );   
        $options[] = array(
            'name' => 'Right Zone',
            'type' => 'info'
        );
            $options[] = array(
                'id' => 'show_contact_second_r',
                'desc' => 'Show Style Configuration',
                'std' => 1,
                'type' => 'checkbox'
            ); 
            $options[] = array(
                'id' => 'contact_second_r_title',
                'desc' => 'Title',
                'std' => 'Title Text',
                'type' => 'text'
            ); 
            $options[] = array(
                'id' => 'contact_second_r_num',
                'desc' => 'Number of Contact Info',
                'std' => '5',
                'type' => 'text',
                'class' => 'mini'
            );
            $options[] = array(
                'name' => 'Contact Info',
                'type' => 'info'
            );
                if(of_get_option('contact_second_r_num')?$cant=of_get_option('contact_second_r_num'):$cant=5);
                for($i=1; $i<=$cant; $i++){
                    $options[] = array(
                        'id' => 'contact_second_r_info'.$i,
                        'desc' => 'Information',
                        'std' => 'Info Text',
                        'type' => 'text'
                    );    
                }
           
    $options[] = array(
        'name' => 'Second Zone',
        'type' => 'toggle-close'
    );
    
    
    
?>