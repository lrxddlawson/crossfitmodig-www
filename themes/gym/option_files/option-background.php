<?php
  $options[] = array(
        'name' => 'Backgrounds Options',
        'type' => 'heading',
        'std' => 'eye-open'
  );  
    $options[] = array(
        'name' => 'General Style Color',
        'type' => 'info'
    );
        $options[] = array(
            'id' => 'general_config',
            'desc' => 'General Color Selector',
            'std' => 'green',
            'options' => array(
                'green' => get_template_directory_uri().'/img/green.jpg',
                'red' => get_template_directory_uri().'/img/red.jpg',
                'cyan' => get_template_directory_uri().'/img/cyan.jpg',
                'yellow' => get_template_directory_uri().'/img/yellow.jpg'
            ),
            'type' => 'images',
        );
    $options[] = array(
        'name' => 'Optional Color Configuration',
        'type' => 'info'
    );
    //SKIN COLOR STYLE
    $options[] = array(
        'name' => 'Skin Color Style',
        'type' => 'toggle'
    );   
        $options[] = array(
            'id' => 'general_skin_color_ch',
            'desc' => 'Enable style color configuration',
            'type' => 'checkbox'
        );
        for($i=1; $i<=6; $i++){
            $options[] = array(
                'name' => 'H'.$i.' Color Style',
                'type' => 'toggle'
            );
            $options[] = array(
                'id' => 'general_color_h'.$i.'_ch',
                'desc' => 'Enable style color configuration',
                'type' => 'checkbox'
            );
            $options[] = array(
                'id' => 'general_color_h'.$i,
                'desc' => 'Define h'.$i.' Color',
                'std' => '1',
                'type' => 'color'
            );
            $options[] = array(
                'name' => 'H'.$i.' Color Style',
                'type' => 'toggle-close'
            );
        }
        //H1 SMALL
        $options[] = array(
            'name' => 'Small Color Style',
            'type' => 'toggle'
        );
            $options[] = array(
                'id' => 'general_color_h1s_ch',
                'desc' => 'Enable style color configuration',
                'type' => 'checkbox'
            );
            $options[] = array(
                'id' => 'general_color_h1s',
                'desc' => 'Define small Color',
                'std' => '1',
                'type' => 'color'
            );
        $options[] = array(
            'name' => 'Small Color Style',
            'type' => 'toggle-close'
        );
        //<a>
        $options[] = array(
            'name' => 'A Color Style',
            'type' => 'toggle'
        );
            $options[] = array(
                'id' => 'general_color_a_ch',
                'desc' => 'Enable style color configuration',
                'type' => 'checkbox'
            );
            $options[] = array(
                'id' => 'general_color_a',
                'desc' => 'Define a Color',
                'std' => '1',
                'type' => 'color'
            );
        $options[] = array(
            'name' => 'A Color Style',
            'type' => 'toggle-close'
        );
        //<p>
        $options[] = array(
            'name' => 'P Color Style',
            'type' => 'toggle'
        );
            $options[] = array(
                'id' => 'general_color_p_ch',
                'desc' => 'Enable style color configuration',
                'type' => 'checkbox'
            );
            $options[] = array(
                'id' => 'general_color_p',
                'desc' => 'Define p Color',
                'std' => '1',
                'type' => 'color'
            );
        $options[] = array(
            'name' => 'P Color Style',
            'type' => 'toggle-close'
        );        
        //BUTTON
        $options[] = array(
            'name' => 'Button Background Color Style',
            'type' => 'toggle'
        );
            $options[] = array(
                'id' => 'general_color_btn_ch',
                'desc' => 'Enable style color configuration',
                'type' => 'checkbox'
            );
            $options[] = array(
            'id' => 'general_color_btn',
            'desc' => 'Define Button Background Color',
            'std' => '1',
            'type' => 'color'
        );
        $options[] = array(
            'name' => 'Button Background Color Style',
            'type' => 'toggle-close'
        );
    
    $options[] = array(
        'name' => 'Skin Color Style',
        'type' => 'toggle-close'
    );
  /*  //FOOTER ZONE
    $options[] = array(
        'name' => 'Footer Background Style',
        'type' => 'toggle'
    );
        $options[] = array(
            'id' => 'generalf_back_check',
            'desc' => 'Background Color',
            'std' => '1',
            'options' => array(
                '1' => 'Background Color',
                '2' => 'Background Image',
            ),
            'type' => 'select'
        );
        $hide_skin = '';
        $hide_back = 'hide';
        if(of_get_option('generalf_back_check')){                    
            if(of_get_option('generalf_back_check')==2){
                $hide_skin = 'hide';
                $hide_back = '';
            }
        }               
        $options[] = array(
            'id' => 'generalf_color_back', 
            'std' => '1',
            'type' => 'color',
            'class' => $hide_skin
        );
        $img_back = array();
        for($i=1; $i<=12; $i++){
            $value = get_template_directory_uri().'/img/bg-theme/'.$i.'.png';
            $img_back[$value] = $value;
        }
        $options[] = array(
            'name' => 'Add Background Images',
            'type' => 'toggle',
            'class' => 'toggle_esp '.$hide_back
        );
            $options[] = array(
                'id' => 'generalf_back_num',
                'desc' => 'Number of Backgrounds',
                'std' => '1',
                'type' => 'text',
                'class' => 'mini'
            );
            if(of_get_option('generalf_back_num')?$cant_=of_get_option('generalf_back_num'):$cant_=1);
            for($i=1; $i<=$cant_; $i++){
                $options[] = array(
                    'name' => 'Image Style',
                    'type' => 'toggle'
                );  
                    if(of_get_option('generalf_back_img'.$i)?$image=of_get_option('generalf_back_img'.$i):$image ='');
                    $options[] = array(
                        'id' => 'generalf_back_img'.$i,
                        'desc' => 'New Image '.$i,
                        'std' => $image,
                        'type' => 'upload'
                    );
                $options[] = array(
                    'name' => 'Image Style',
                    'type' => 'toggle-close'
                ); 
                if(!of_get_option('generalf_back_img'.$i)?$img_back[$image]=$image:$img_back[$image]=$image);
            }
        $options[] = array(
            'name' => 'Add Background Images',
            'type' => 'toggle-close'
        );
        $options[] = array(
            'id' => 'generalf_back_img',
            'desc' => 'Choose the background image',
            'std' => 'img-12',
            'options' => $img_back,
            'type' => 'images',
            'class' => 'mini_img '.$hide_back
        );
    $options[] = array(
        'name' => 'Footer Background Style',
        'type' => 'toggle-close'
    );   */
    
    
?>