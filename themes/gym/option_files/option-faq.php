<?php

    $options[] = array(
        'name' => ('Faq Options'),
        'type' => 'heading',
        'std' => 'star'
    );

     $options[] = array(
                'name' => 'Banner',
                'type' => 'toggle'
     );
         $options[] = array(
                    'name' =>'Show Banner',
                    'id' => 'show_banner_faq',
                    'desc' => 'Show Banner',
                    'std' => 1,
                    'type' => 'checkbox'
                );
         $options[] = array(
                    'id' => 'faq_banner_text',
                    'desc' => 'Banner Text',
                    'std' => 'NO PRESSURE, NO DIAMONDS',
                    'type' => 'text'
         );
         $options[] = array(
                        'id' => 'faq_banner_image',
                        'desc' => 'Load Imagen',
                        'type' => 'upload'
             );
         $options[] = array(
                    'name' =>'Show Breadcumbs',
                    'id' => 'show_breadcumbs_faq',
                    'desc' => 'Show Breadcumbs',
                    'std' => 1,
                    'type' => 'checkbox'
                );
     $options[] = array(
        'name' => 'Banner',
        'type' => 'toggle-close'
     );

     $options[] = array(
            'name' => 'Accordions',
            'type' => 'toggle'
     );
          $options[] = array(
                'id' => 'show_faq_acc',
                'desc' => 'Show Style Configuration',
                'std' => 1,
                'type' => 'checkbox'
          );
          $options[] = array(
                'id' => 'faq_acc_title',
                'desc' => 'Accordion Zone Title',
                'std' => 'Title Text',
                'type' => 'text'
          );
          $options[] = array(
                'id' => 'faq_acc_num',
                'desc' => 'Number of Accordions',
                'std' => '4',
                'type' => 'text',
                'class' => 'mini'
          );
          if(of_get_option('faq_acc_num')?$cant=of_get_option('faq_acc_num'):$cant=4);
          for($i=1; $i<=$cant; $i++){
            $options[] = array(
                'name' => 'Accordion '.$i,
                'type' => 'toggle'
            );
                $options[] = array(
                    'id' => 'faq_acc_o_title'.$i,
                    'desc' => 'Accordion Title',
                    'std' => 'Title Text',
                    'type' => 'text'
                );
                $options[] = array(
                    'id' => 'faq_acc_o_subtitle'.$i,
                    'desc' => 'Accordion Subtitle',
                    'std' => 'Subtitle Text',
                    'type' => 'text'
                );
                $options[] = array(
                    'id' => 'faq_acc_o_desc'.$i,
                    'desc' => 'Accordion Description',
                    'std' => 'Description Text',
                    'type' => 'textarea'
                );
            $options[] = array(
                'name' => 'Accordion '.$i,
                'type' => 'toggle-close'
            );
          }
     $options[] = array(
        'name' => 'Accordions',
        'type' => 'toggle-close'
     );              
    $options[] = array(
        'name' => 'Sponsor',
        'type' => 'toggle'
    );
       $options[] = array(
            'name' =>'Show Sponsor',
            'id' => 'show_sponsor_faq',
            'desc' => 'Show Sponsor',
            'std' => 1,
            'type' => 'checkbox'
        );
    $options[] = array(
        'name' => 'Sponsor',
        'type' => 'toggle-close'
    );
    $options[] = array(
        'name' => 'Right Zone',
        'type' => 'info'
    );
  
    $options[] = array(
        'name' => 'Zone 1',
        'type' => 'toggle'
    );
        $options[] = array(
                'id' => 'show_zone1_r',
                'desc' => 'Show Style Configuration',
                'std' => 1,
                'type' => 'checkbox'
        );
        $options[] = array(
                'id' => 'zone1_r_title',
                'desc' => 'Title',
                'std' => 'Title Text',
                'type' => 'text'
        );
        foreach(font_awesome_icon('vgn_zone1') as $val){
            $options[] = $val;
        }
        $options[] = array(
                'id' => 'zone1_r_num',
                'desc' => 'Number of Memberships',
                'std' => '4',
                'type' => 'text',
                'class' => 'mini'
          );
            
          if(of_get_option('zone1_r_num')?$cant=of_get_option('zone1_r_num'):$cant=4);
          for($i=1; $i<=$cant; $i++){
            $options[] = array(
                'name' => 'Option '.$i,
                'type' => 'toggle'
            );
               $options[] = array(
                     'id' => 'zone1_r_text'.$i,
                     'desc' => 'Text',
                     'std' => 'Text',
                     'type' => 'text'
               );
               $options[] = array(
                     'id' => 'zone1_r_link'.$i,
                     'desc' => 'Link',
                     'std' => 'Link Text',
                     'type' => 'text'
               );
               $options[] = array(
                     'id' => 'zone1_r_price'.$i,
                     'desc' => 'Price',
                     'std' => '0.00',
                     'type' => 'text'
               );  
            $options[] = array(
                'name' => 'Option '.$i,
                'type' => 'toggle-close'
            );
          }
    $options[] = array(
        'name' => 'Zone 1',
        'type' => 'toggle-close'
    );
    $options[] = array(
        'name' => 'Zone 2',
        'type' => 'toggle'
    );
        $options[] = array(
                'id' => 'show_zone2_r',
                'desc' => 'Show Style Configuration',
                'std' => 1,
                'type' => 'checkbox'
        );
        $options[] = array(
                'id' => 'zone2_r_title',
                'desc' => 'Title',
                'std' => 'Title Text',
                'type' => 'text'
        );
        $options[] = array(
            'name' => 'Choose the category icon to show',
            'type' => 'info-exp',
            'class' => 'important-info'
        );
        
        foreach(get_categories() as $cats){
            $options[] = array(
                'id' => 'zone2_r_cat'.$cats->slug,
                'desc' => $cats->cat_name,
                'type' => 'checkbox'
            );
        }
        
        foreach(font_awesome_icon('vgn_zone2') as $val){
            $options[] = $val;
        }
        
    $options[] = array(
        'name' => 'Zone 2',
        'type' => 'toggle-close'
    );
  
    $options[] = array(
        'name' => 'Zone 3',
        'type' => 'toggle'
    );
        $options[] = array(
                'id' => 'show_zone3_r',
                'desc' => 'Show Style Configuration',
                'std' => 1,
                'type' => 'checkbox'
        );
        $options[] = array(
                'id' => 'zone3_r_title',
                'desc' => 'Title',
                'std' => 'Title Text',
                'type' => 'text'
        );
        $options[] = array(
            'id' => 'zone3_r_num',
            'desc' => 'Number of Social Link',
            'std' => '7',
            'type' => 'text',
            'class' => 'mini'
        );
        if(of_get_option('zone3_r_num')?$cant=of_get_option('zone3_r_num'):$cant=7);
        for($i=1; $i<=$cant; $i++){
          $options[] = array(
                'name' => 'Social Link '.$i,
                'type' => 'toggle'            
          );
            foreach(font_awesome_icon('sl_zone3_r'.$i) as $val){
                $options[] = $val;
            }
            $options[] = array(
                    'id' =>  'sl_zone3_r_url'.$i,
                    'desc' => 'Social link',
                    'type' => 'text'
            );
          $options[] = array(
                'name' => 'Social Link '.$i,
                'type' => 'toggle-close' 
          );
        }
    $options[] = array(
        'name' => 'Zone 3',
        'type' => 'toggle-close'
    );
?>