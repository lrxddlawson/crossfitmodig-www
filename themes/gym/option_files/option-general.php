<?php

    $options[] = array(
        'name' => ('General'),
        'type' => 'heading',
        'std' => 'gears'
    );

        $options[] = array(
            'id' => 'main_mail',
            'desc' => 'Principal Mail',
            'std' => 'URL mail',
            'type' => 'text'
        );

     $options[] = array(
                'name' => 'Logo Zone',
                'type' => 'toggle'
     );

         $options[] = array(
            'id' => 'style_logo',
            'std' => '1',
            'options' => array(
                '1' => 'Text Logo',
                '2' => 'Image Logo'                     
            ),
            'type' => 'radio',
            'class' => 'side'
        );
        $options[] = array(
            'id' => 'text_logo',
            'desc' => 'Logo Text',
            'std' => 'GYM - Sport',
            'type' => 'text'
        );
        $options[] = array(
            'id' => 'img_logo',
            'desc' => 'Logo Image',
            'type' => 'upload'
        );
        $options[] = array(
            'id' => 'typo_logo',
            'desc' => 'Logo Typography',
            'std' => array( 'size' => '36', 'face' => 'Rokkitt','style'=>'normal', 'color'=> '#00bc96'),
            'type' => 'typography'
        );


     $options[] = array(
                'type' => 'toggle-close'
     );


     $options[] = array(
                'name' => 'Customization',
                'type' => 'toggle'
     );

            $options[] = array(
            'id' => 'favicon',
            'desc'=>'Favicon',
            'type' => 'upload');

            $options[] = array(
            'id' => 'login',
            'desc'=>'Logo Login',
            'type' => 'upload');
                
     $options[] = array(
                'type' => 'toggle-close'
     );

    //STYLES PAGES CONFIGURATION
     $options[] = array(
                'name' => 'Styles pages configuration zone',
                'type' => 'toggle'
     );

     $options[] = array(
                'desc' => 'WARNING: Two pages never be the same style.',
                'type' => 'info'
     );
  //PAGE STYLE
  $styles_pages_sele = array(
            '1' => 'No-style',
            '2' => 'Home',
            '3' => 'Class',
            '4' => 'Trainers',
            '5' => 'Club',
            '6' => 'Faq',
            '7' => 'Gallery_Two',
            '8' => 'Gallery_Three',
            '9' => 'Gallery_Four',
            '10' => 'Blog',
            '11' => 'Price',
            '12' => 'Contact',
            '13' => 'Tabs'
     );

     $paginas = array();
     $pages_obj = get_pages();
     foreach ($pages_obj as $page) {
         $paginas[$page->ID] = array($page->post_title,$page->post_name);
            $pag[] = $page->post_title;
            $foot[] = $page->post_id;
     }
     ksort($paginas);
      $styles_pages = array_keys($paginas);

       for($i=0; $i<count($paginas); $i++){
         if($paginas[$styles_pages[$i]][1] == 'whmcs-bridge'){

          wp_update_post(array('ID' => $styles_pages[$i], 'post_name' => 'customer'));

         }
         if($paginas[$styles_pages[$i]][1] != 'customer'){
             $selec_pos = array_search(ucfirst($paginas[$styles_pages[$i]][1]),$styles_pages_sele);
             $options[] = array(
                    'id' => 'page_style_'.$styles_pages[$i],
                    'desc' => 'Page style for: '.$paginas[$styles_pages[$i]][0],
                    'std' => $selec_pos,
                    'options' => $styles_pages_sele,
                    'type' => 'select'
             );
             $pos = of_get_option('page_style_'.$styles_pages[$i]);
                if($pos)
            {
                 if($pos != '1'){
                    wp_update_post(array('ID' => $styles_pages[$i], 'post_name' => $styles_pages_sele[$pos]));
                 }
            }

         }
     }
     $options[] = array(
                'type' => 'toggle-close'
     );
     //FIRST ZONE

     $options[] = array(
                'name' => 'Head Zone',
                'type' => 'toggle'
     );
     $options[] = array(
                'name' => 'First Field',
                'type' => 'toggle'
     );
     $options[] = array(
                'id' => 'header_text',
                'desc' => 'Header Text',
                'std' => 'Opening Hours',
                'type' => 'text'
     );

     foreach(font_awesome_icon('header_first') as $val){
            $options[] = $val;
     }

     $options[] = array(
                'name' => 'Show Header',
                'id' => 'show_header',
                'desc' => 'Show Header',
                'std' => 1,
                'type' => 'checkbox'
     );

     $options[] = array(
                'name' => 'Sub Field',
                'type' => 'toggle'
     );
     $options[] = array(
        'id' => 'sub_field_num',
        'desc' => 'Number of Sub Field',
        'std' => 3,
        'class' => 'mini',
        'type' => 'text'
  );
  if(of_get_option('sub_field_num')?$cant=of_get_option('sub_field_num'):$cant=3);
    for($i=1; $i<=$cant; $i++)
    {

       $options[] = array(
                'name' => 'Sub Field '.$i,
                'type' => 'toggle'
        );
        $options[] = array(
                    'id' => 'sub_text'.$i,
                    'desc' => 'Sub Field Text',
                    'std' => 'Opening Hours',
                    'type' => 'text'
         );

         foreach(font_awesome_icon('header_first'.$i) as $val){
                $options[] = $val;
         }

         $options[] = array(
                    'type' => 'toggle-close'
         );

     }
    $options[] = array(
                'name' => 'Last Link',
                'type' => 'toggle'
        );

    $options[] = array(
                'id' => 'last_link_text',
                'desc' => 'Last Text',
                'std' => '',
                'type' => 'text'
     );
     $options[] = array(
                'id' => 'last_link_url',
                'desc' => 'Last Text Url',
                'std' => '',
                'type' => 'text'
     );
       $options[] = array(
                'name' => 'Show Last Link',
                'id' => 'show_last',
                'desc' => 'Show Last Link',
                'std' => 1,
                'type' => 'checkbox'
     );

       $options[] = array(
                    'type' => 'toggle-close'
         );


     $options[] = array(
                'type' => 'toggle-close'
     );


     $options[] = array(
                'type' => 'toggle-close'
     );
     $options[] = array(
                'name' => 'Second Field',
                'type' => 'toggle'
     );

     $options[] = array(
                'id' => 'number_phone',
                'desc' => 'Phone number',
                'std' => '2-985-505-6574',
                'type' => 'text'
     );

     foreach(font_awesome_icon('header_second') as $val){
            $options[] = $val;
     }

     $options[] = array(
                'name' => 'Show Number',
                'id' => 'show_phone',
                'desc' => 'Show Phone',
                'std' => 1,
                'type' => 'checkbox'
     );

     $options[] = array(
                'type' => 'toggle-close'
     );
     $options[] = array(
                'name' => 'Third Zone',
                'type' => 'toggle'
     );
          $options[] = array(
                'id' => 'contac_text',
                'desc' => 'Text',
                'std' => 'Contact',
                'type' => 'text'
     );
               $options[] = array(
                'id' => 'contac_link',
                'desc' => 'Url',
                'std' => '',
                'type' => 'text'
     );
         foreach(font_awesome_icon('header_third') as $val){
            $options[] = $val;
         }

     $options[] = array(
                'name' => 'Show Contact',
                'id' => 'show_contact',
                'desc' => 'Show Contact',
                'std' => 1,
                'type' => 'checkbox'
     );

     $options[] = array(
                'type' => 'toggle-close'
     );
     $options[] = array(
                'name' => 'Icon Zone',
                'type' => 'toggle'
     );
     $options[] = array(
        'id' => 'header_sl_num',
        'desc' => 'Number of Social Links',
        'std' => '8',
        'type' => 'text',
        'class' => 'mini'
    );
    if(of_get_option('header_sl_num')?$cant=of_get_option('header_sl_num'):$cant=8);
    for($i=1; $i<=$cant; $i++){
            $options[] = array(
                        'name' => 'Social Icon '.$i,
                        'type' => 'toggle'
            );
            $options[] = array(
                'desc' =>'Display on Header, default false',
                'id' => 'show_header'.$i,
                'std' => '0',
                'type' => 'checkbox'
            );
            $options[] = array(
                    'id' => 'header_social_link'.$i,
                    'desc' => 'Link',
                    'type' => 'text'
            );
            foreach(font_awesome_icon('header_social'.$i) as $val){
                $options[] = $val;
            }
            $options[] = array(
                'name' => 'Social Icon '.$i,
                'type' => 'toggle-close'
            );
        }

     $options[] = array(
                'type' => 'toggle-close'
     );
     $options[] = array(
                'type' => 'toggle-close'
     );
          $options[] = array(
                'name' => 'Slider',
                'type' => 'toggle'
     );
        $options[] = array(
            'id' => 'show_slider_opt',
            'desc' => 'Show Slider',
            'std' => 0,
            'type' => 'checkbox'
        );
        $options[] = array(
                    'id' => 'slider_opt',
                    'desc' => 'Put the Alias of Slider',
                    'std' => '',
                    'type' => 'text'
         );

     $options[] = array(
                'type' => 'toggle-close'
     );
          $options[] = array(
                'name' => 'Sponsor',
                'type' => 'toggle'
     );

        $options[] = array(
                    'id' => 'sponsor_title',
                    'desc' => 'Title',
                    'std' => 'Title',
                    'type' => 'text'
         );

      $options[] = array(
        'id' => 'sponsor_cant',
        'desc' => 'Number of Sponsor',
        'std' => 3,
        'class' => 'mini',
        'type' => 'text'
  );
  if(of_get_option('sponsor_cant')?$cant=of_get_option('sponsor_cant'):$cant=3);
    for($u=1; $u<=$cant; $u++)
    {

       $options[] = array(
                'name' => 'Sponsor '.$u,
                'type' => 'toggle'
        );
        $options[] = array(
                    'id' => 'sponsor_text'.$u,
                    'desc' => 'Load Imagen',
                    'type' => 'upload'
         );

        $options[] = array(
                    'id' => 'sponsor_link'.$u,
                    'desc' => 'Url',
                    'type' => 'text'
         );

         $options[] = array(
                    'type' => 'toggle-close'
         );

     }

     $options[] = array(
                'type' => 'toggle-close'
     );
     
?>