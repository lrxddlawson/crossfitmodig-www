<?php

    $options[] = array(
        'name' => ('Classes Options'),
        'type' => 'heading',
        'std' => 'pencil'
    );

     $options[] = array(
                'name' => 'Classes Gallery Options',
                'type' => 'toggle'
     );
        $options[] = array(
                'name' =>'Show Banner',
                'id' => 'show_banner_class',
                'desc' => 'Show Banner',
                'std' => 1,
                'type' => 'checkbox'
        );
        $options[] = array(
                'id' => 'class_banner_text',
                'desc' => 'Banner Text',
                'std' => 'NO PRESSURE, NO DIAMONDS',
                'type' => 'text'
        );
        $options[] = array(
                'id' => 'class_banner_image',
                'desc' => 'Load Imagen',
                'type' => 'upload'
         );
        $options[] = array(
                'name' =>'Show Breadcumbs',
                'id' => 'show_breadcumbs_class',
                'desc' => 'Show Breadcumbs',
                'std' => 1,
                'type' => 'checkbox'
        );
        $options[] = array(
                'id' => 'class_title',
                'desc' => 'Subscribe title',
                'type' => 'text',
                'std' => 'All our Trainers',
                'class' => 'text'
        );
          $options[] = array(
                'name' => 'Sponsor Zone',
                'type' => 'info'
          );
              $options[] = array(
                    'name' =>'Show Sponsor',
                    'id' => 'show_sponsor_class',
                    'desc' => 'Show Sponsor',
                    'std' => 1,
                    'type' => 'checkbox'
              );
     $options[] = array(
            'name' => 'Classes Gallery Options',
            'type' => 'toggle-close'
     );

     $options[] = array(
            'name' => 'Single-Class Gallery Options',
            'type' => 'toggle'
     );
        $options[] = array(
                'name' =>'Show Banner',
                'id' => 'show_banner_single_class',
                'desc' => 'Show Banner',
                'std' => 1,
                'type' => 'checkbox'
        );
        $options[] = array(
                'id' => 'class_banner_single_text',
                'desc' => 'Banner Text',
                'std' => 'NO PRESSURE, NO DIAMONDS',
                'type' => 'text'
        );
        $options[] = array(
                'id' => 'class_banner_single_image',
                'desc' => 'Load Imagen',
                'type' => 'upload'
         );
        $options[] = array(
                'name' =>'Show Breadcumbs',
                'id' => 'show_breadcumbs_single_class',
                'desc' => 'Show Breadcumbs',
                'std' => 1,
                'type' => 'checkbox'
        );        
        $options[] = array(
                'name' => 'Sponsor Zone',
                'type' => 'info'
        );
              $options[] = array(
                    'name' =>'Show Sponsor',
                    'id' => 'show_sponsor_single_class',
                    'desc' => 'Show Sponsor',
                    'std' => 1,
                    'type' => 'checkbox'
              );
        $options[] = array(
                'name' => 'Features Classes',
                'type' => 'toggle'
        );
            $options[] = array(
                'id' => 'show_features_class',
                'desc' => 'Show Style Configuration',
                'std' => 1,
                'type' => 'checkbox'
            );
            $options[] = array(
                'id' => 'features_class_title',
                'desc' => 'Title',
                'std' => 'Title Text',
                'type' => 'text'
            );
            $options[] = array(
                'id' => 'features_class_btn_link',
                'desc' => 'Button Link',
                'std' => 'Button Link Text',
                'type' => 'text'
            );
            $options[] = array(
                    'id' => 'features_class_btn_url',
                'desc' => 'Button URL',
                'std' => 'Button URL Text',
                'type' => 'text'
            );
            $options[] = array(
                'id' => 'features_class_num',
                'desc' => 'Number of features classes',
                'std' => '3',
                'type' => 'text',
                'class' => 'mini'
            );
            if(of_get_option('features_class_num')?$cant=of_get_option('features_class_num'):$cant=3);
            for($i=1; $i<=$cant; $i++){
                $options[] = array(
                        'name' => 'Feature Class '.$i,
                        'type' => 'toggle'
                );
                    $options[] = array(
                        'id' => 'features_title'.$i,
                        'desc' => 'Title',
                        'std' => 'Title Text',
                        'type' => 'text'
                    );
                    $options[] = array(
                        'id' => 'features_desc'.$i,
                        'desc' => 'Description',
                        'std' => 'Description Text',
                        'type' => 'textarea'
                    );
                    $options[] = array(
                        'id' => 'features_img'.$i,
                        'desc' => 'Image',
                        'type' => 'upload'
                    );
                    $options[] = array(
                        'id' => 'features_btn'.$i,
                        'desc' => 'Button Link',
                        'std' => 'Button Link Text',
                        'type' => 'text'
                    );
                    $options[] = array(
                        'id' => 'features_btn_link'.$i,
                        'desc' => 'Link',
                        'std' => 'Link Text',
                        'type' => 'text'
                    );
                $options[] = array(
                        'name' => 'Feature Class '.$i,
                        'type' => 'toggle-close'
                );
            }
        $options[] = array(
                'name' => 'Features Classes',
                'type' => 'toggle-close'
        );
        $options[] = array(
                'name' => 'Weekend Classes',
                'type' => 'toggle'
        );
            $options[] = array(
                'id' => 'show_week_class',
                'desc' => 'Show Style Configuration',
                'std' => 1,
                'type' => 'checkbox'
            );
            $options[] = array(
                'id' => 'week_class_title',
                'desc' => 'Title',
                'std' => 'Title Text',
                'type' => 'text'
            );
            $options[] = array(
                'id' => 'week_class_num',
                'desc' => 'Number of weekend classes',
                'std' => '3',
                'type' => 'text',
                'class' => 'mini'
            );
            if(of_get_option('week_class_num')?$cant=of_get_option('week_class_num'):$cant=3);
            for($i=1; $i<=$cant; $i++){
                $options[] = array(
                        'name' => 'Weekend Class '.$i,
                        'type' => 'toggle'
                );
                    $options[] = array(
                        'id' => 'week_title'.$i,
                        'desc' => 'Title',
                        'std' => 'Title Text',
                        'type' => 'text'
                    );
                    $options[] = array(
                        'id' => 'week_desc'.$i,
                        'desc' => 'Description',
                        'std' => 'Description Text',
                        'type' => 'textarea'
                    );
                $options[] = array(
                        'name' => 'Weekend Class '.$i,
                        'type' => 'toggle-close'
                );
            }
        $options[] = array(
                'name' => 'Weekend Classes',
                'type' => 'toggle-close'
        );
        $options[] = array(
                'name' => 'Testimonials',
                'type' => 'toggle'
        );
            $options[] = array(
                'id' => 'show_test_class',
                'desc' => 'Show Style Configuration',
                'std' => 1,
                'type' => 'checkbox'
            );
            $options[] = array(
                'id' => 'test_class_title',
                'desc' => 'Title',
                'std' => 'Title Text',
                'type' => 'text'
            );
            $options[] = array(
                'id' => 'test_class_num',
                'desc' => 'Number of testimonials',
                'std' => '3',
                'type' => 'text',
                'class' => 'mini'
            );
            if(of_get_option('test_class_num')?$cant=of_get_option('test_class_num'):$cant=3);
            for($i=1; $i<=$cant; $i++){
                $options[] = array(
                        'name' => 'Testimonial '.$i,
                        'type' => 'toggle'
                );
                    $options[] = array(
                        'id' => 'test_name'.$i,
                        'desc' => 'Title',
                        'std' => 'Title Text',
                        'type' => 'text'
                    );
                    $options[] = array(
                        'id' => 'test_dtl'.$i,
                        'desc' => 'Detail',
                        'std' => 'Detail Text',
                        'type' => 'text'
                    );
                    $options[] = array(
                        'id' => 'test_desc'.$i,
                        'desc' => 'Description',
                        'std' => 'Description Text',
                        'type' => 'textarea'
                    );
                    $options[] = array(
                        'id' => 'test_img'.$i,
                        'desc' => 'Photo',
                        'type' => 'upload'
                    );
                $options[] = array(
                        'name' => 'Testimonial '.$i,
                        'type' => 'toggle-close'
                );
            }
        $options[] = array(
                'name' => 'Testimonials',
                'type' => 'toggle-close'
        );
     $options[] = array(
            'name' => 'Trainer Gallery Otions',
            'type' => 'toggle-close'
     );
?>