<?php get_header(); ?>
    <!-- begin Content -->
    <section id="club" >
        <!-- begin Sub Header -->
     <?php if(of_get_option('show_banner_club')){?>

        <div class="sub-header" style="<?php echo 'background:url('.of_get_option('club_banner_image').') no-repeat;'?>">

            <div class="container">

                <div class="row" >

                    <ul class="sub-header-container" >

                        <li>

                            <h3 <?php echo colors('h3');?> class="title"><?php echo of_get_option('club_banner_text') ?></h3>

                        </li>

                        <li>

                            <ul class="custom-breadcrumb" >

                                 <li><h6 <?php echo colors('h6');?>><a <?php echo colors('a');?> href="index.html">Home</a></h6></li>

                                <li><i class="separator entypo-play" ></i></li>

                                <li><h6 <?php echo colors('h6');?>>Classes</h6></li>

                            </ul>

                        </li>

                    </ul>

                </div>

            </div>

        </div>
<?php } ?>
        <!-- end Sub Header -->

        <!-- begin GYM Club -->
        <article class="article-container">
         
            <div class="container" >

                <div class="row" >
         
                    <div class="col-md-12">
         
                    <h2 <?php echo colors('h2');?> class="article-title" ><?php echo of_get_option('club_title') ?></h2>
         
                    <span <?php echo colors('h1s');?> class="line" >
         
                        <span <?php echo colors('h1s');?> class="sub-line" ></span>
         
                    </span>
         
                    </div>
         
                </div>

                <div class="row">
                    <!-- -->
                    <?php if(of_get_option('club_zone_img_pos') == '1'){?>
         
                    <div class="col-md-6" >
         
                        <img src="<?php echo of_get_option('club_zone_image') ?>" class="gym-club" alt="img" />
         
                    </div>
         
                    <?php }?>
         
                    <div class="col-md-6" >
         
                        <h3 <?php echo colors('h3');?>><?php echo of_get_option('club_title_1') ?></h3>
         
                        <?php for ($p=1; $p <= of_get_option('club_num_pa') ; $p++) {?>

                            <p <?php echo colors('p');?>><?php echo of_get_option('club_pa_'.$p) ?></p>
                            
                            <?php }?>
                    
                    </div>  
                                        
                    <?php if(of_get_option('club_zone_img_pos') == '2'){?>
                    
                    <div class="col-md-6" >
                    
                        <img src="<?php echo of_get_option('club_zone_image') ?>" class="gym-club" alt="img" />
                    
                    </div>
                    
                    <?php }?>
                
                </div>

                <div class="row">
                    <!-- -->
                    <div class="col-md-6" >
                        <!-- TABS -->
                        <?php
                        $color;
                        if(of_get_option('style')==1)
                        {
                            $color = 'nav-tabs-sec';
                        }
                        else
                        {
                            $color = '';
                        }

                        ?>
            
                        <ul class="nav nav-tabs <?php echo $color; ?> ">
            
                            <?php for ($in=1; $in <= of_get_option('club_num_tabs'); $in++) {
                                $active = 'class=active';
                                if($in!=1)
                                {
                                    $active='';
                                }
                                    ?>
            
                                <li <?php echo $active; ?> >
            
                            <a <?php echo colors('a');?> href="#<?php echo of_get_option('club_tab_'.$in); ?>" data-toggle="tab"><?php echo of_get_option('club_tab_'.$in) ?></a>
            
                          </li>
            
                           <?php  } ?>

                          </ul>

                        <div class="tab-content <?php echo $color; ?>">
    
                            <?php for ($dv=1; $dv <= of_get_option('club_num_tabs'); $dv++) {
                                $active2 = 'active';
                                if($dv!=1)
                                {
                                    $active2='';
                                }
                                    ?>

                          <div class="tab-pane <?php echo $active2 ?>" id="<?php echo of_get_option('club_tab_'.$dv); ?>">
    
                            <p <?php echo colors('p');?>>
    
                               <?php echo of_get_option('club_ta_'.$dv) ?>
    
                            </p>
                          </div>
                          <?php } ?>
                          </div>
                    </div>
    
                    <div class="col-md-6" >
    
                        <h3 <?php echo colors('h3');?>><?php echo of_get_option('club_title_3') ?></h3>
    
                        <?php for ($p=1; $p <= of_get_option('club_num_pa2') ; $p++) {?>

                            <p <?php echo colors('p');?>><?php echo of_get_option('club_pa2_'.$p) ?></p>
  
                            <?php }?></div>
  
                </div>
  
            </div>

        </article>
        <!-- end GYM Club -->
        <!-- begin Bodybuilding Supplements -->
<?php if(of_get_option('show_sponsor_club')) {?>
        <article class="article-container">
            <div class="container" >

                <!-- arrows -->
                <div class="row" >
                    <div class="col-md-12">
                        <h2 <?php echo colors('h2');?> class="headers"><?php echo of_get_option('sponsor_title'); ?> </h2>
                        <span <?php echo colors('h1s');?> class="line" >
                            <span <?php echo colors('h1s');?> class="sub-line" ></span>
                        </span>
                        <?php if(of_get_option('sponsor_cant')>5) { ?>
                        <a <?php echo colors('a');?> class="slider-control pull-right next" href="#bodybuilding" data-slide="next"></a>
                        <a <?php echo colors('a');?> class="slider-control pull-right prev" href="#bodybuilding" data-slide="prev"></a>
                        <?php } ?>
                    </div>
                </div>
                <!-- end arrows -->
                 <div class="row" >
                    <div id="bodybuilding" class="carousel slide">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <?php
                            $begin1='<div class="item active"><ul class="logos" >';
                            $last1 = '</ul></div>';
                            $begin2='<div class="item"><ul class="logos" >';
                        for ($log=1; $log <= of_get_option('sponsor_cant') ; $log++) {

                            if($log!=1)
                               {
                                $begin1 = '';
                               }
                            echo $begin1;
                             ?>
                                <li><a <?php echo colors('a');?> href="<?php echo of_get_option('sponsor_link'.$log);  ?>"><img src="<?php echo of_get_option('sponsor_text'.$log) ?>" alt="<?php echo $log;?>" /></a></li>
                            <?php
                            $c=$log%5;
                            if($c==0)
                            {
                            echo $last1;
                            echo $begin2;

                            }

                            } ?>
                    </div>
                    </div>
                </div>
            </div>
        </article>
        <?php  } ?>
        <!-- end Bodybuilding Supplements -->
    </section>

    <!-- end content --><?php get_footer(); ?>
    <!-- Footer -->
