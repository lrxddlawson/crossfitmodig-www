<?php
/**
 * The template for displaying all pages.
 */

get_header(); ?>

<section id="page">
	
	<div class="single_page">

        <article class="article-container">
		
		    <div class="container">
		
	            <div id="primary" class="content-area">
	
		            <div id="content" class="site-content" role="main">

			            <?php while ( have_posts() ) : the_post(); ?>

				        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

						    <?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
						
						    <div class="entry-thumbnail">
							
							    <?php the_post_thumbnail(); ?>
						    
							</div>
							
						    <?php endif; ?>

						    <h1 <?php echo colors('h1');?> class="entry-title"><?php the_title(); ?></h1>

					        <div class="entry-content">
						        
								<?php the_content(); ?>
						        
								<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'gym' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
	
					        </div><!-- .entry-content -->

				        </article><!-- #post -->

			        <?php endwhile; ?>

				</div><!-- #content -->
				
	        </div><!-- #primary -->
			
		    </div>
			
		</article>
		
	</div>
		
</section>

<?php get_footer(); ?>