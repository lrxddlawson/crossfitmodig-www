<?php
    get_header();
    the_post();
    if(of_get_option('show_contact1') == '1'){
?>
    <!-- begin Content -->
    <section id="contact" >
        <!-- begin Sub Header -->
    <?php if(of_get_option('show_banner_contact')){?>
        <div class="sub-header" style="<?php echo 'background:url('.of_get_option('contact_banner_image').') no-repeat;'?>">
            <div class="container">
                <div class="row" >
                    <ul class="sub-header-container" >
                        <li>
                            <h3 <?php echo colors('h3');?> class="title"><?php echo of_get_option('contact_banner_text') ?></h3>
                        </li>
                        <li>
                        <?php if (of_get_option('show_breadcumbs_contact')) { ?>
                            <ul class="custom-breadcrumb" >
                                <li><h6 <?php echo colors('h6');?>><a <?php echo colors('a');?> href="<?php echo home_url();?>">Home</a></h6></li>
                                <li><i class="separator entypo-play" ></i></li>
                                <li><h6 <?php echo colors('h6');?>>Contact</h6></li>
                            </ul>                    
                        <?php }?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    <?php }?>
        <!-- end Sub Header -->

        <!-- begin Contanct -->
        <article class="article-container">
            <div class="container" >
              <?php if(of_get_option('show_contact_first') == '1'){?>               
               
                <div class="row" >
                    <div class="col-md-12">
                    <h2 <?php echo colors('h2');?> class="article-title" ><?php echo of_get_option('contact_title');?></h2>
                    <span <?php echo colors('h1s');?> class="line" >
                        <span <?php echo colors('h1s');?> class="sub-line" ></span>
                    </span>
                    </div>
                </div>

                <div class="row">
                
                    <?php if(of_get_option('show_contact_map')){?>      
                             
                      <div class="col-md-7">
                         
                          <div class="entry-content">
						        
			<?php the_content(); ?>

			<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __(               'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
	
					        </div><!-- .entry-content -->
                        
                    </div>
                    
                    <?php }?>
                    
                    <?php     if(of_get_option('show_contact_form')){?>
                        
                    <?php
                        if (isset($_REQUEST['email'])){
                        $name = $_REQUEST['name'] ;
                        $email = $_REQUEST['email'] ;
                        $phone = $_REQUEST['web'] ;
                        $message = $_REQUEST['message'] ;
                        $sub   =  'Contact Us';
                        $msg   =  'Dear Admin,<br /><br />';  
                        $msg  .=  'Name: '.$name.'<br /><br />';  
                        $msg  .=  'Email: '.$email.'<br /><br />';      
                        $msg  .=  'Web: '.$phone.'<br /><br />';  
                        $msg  .=  'Message: '.$message.'<br /><br />';              
                        $msg  .=  '<br /><br />Best Regards<br />'.$name."&nbsp;";      
                        $subject = $sub;
                        $mailheader = "From: <".$email.">\r\n"; 
                        $mailheader .= "Content-type: text/html; charset=iso-8859-1\r\n"; 
                        $MESSAGE_BODY = $msg; 
                        @mail("test@test.com", $subject, $MESSAGE_BODY, $mailheader);
                        echo '<div class="success">Email has submited.</div>';
                        }
                    ?>

                    
  
                                 
                    <div class="col-md-5 contact" >
                        <form id="form" action="<?php echo get_template_directory_uri();?>/send_mail.php">
                          <div class="form-group">
                            <label for="contact_name"><i <?php echo font_awesome_icon_style('name_icon');?> ></i> <?php echo of_get_option('contact_form_name');?> <em>(required)</em></label>
                            <input type="text" name="name" class="form-control" id="contact_name">
                          </div>
                          <div class="form-group">
                            <label for="contact_e-mail"><i <?php echo font_awesome_icon_style('mail_icon');?> ></i> <?php echo of_get_option('contact_form_mail');?> <em>(required)</em></label>
                            <input type="email" name="email" class="form-control" id="contact_e-mail">
                          </div>
                          <div class="form-group">
                            <label for="contact_subject"><i <?php echo font_awesome_icon_style('subject_icon');?> ></i> <?php echo of_get_option('contact_form_subject');?> <em>(required)</em></label>
                            <input type="text" name="subject" class="form-control" id="contact_subject">
                          </div>
                          <div class="form-group">
                            <label for="contact_message"><i <?php echo font_awesome_icon_style('msg_icon');?> ></i> <?php echo of_get_option('contact_form_msg');?></label>
                            <textarea id="contact_message" name="message" class="form-control" rows="6"></textarea>
                          </div>
                          <div id="result"></div>
                          <input name="from" type="hidden" value="<?php echo of_get_option('main_mail');?>">
                          <button type="submit" name="Submit" class="btn btn-default"><?php echo of_get_option('contact_form_btn');?></button>
                        </form>
                        
                    </div>
                    
                    <?php }?>
                    
                </div>
                
              <?php }?>  
              
              <?php if(of_get_option('show_contact_second')  == '1'){?>               
              
                <div class="row">
                
                    <?php if(of_get_option('show_contact_second_l')){?> 
                
                    <div class="col-md-9">
                    
                        <h4 <?php echo colors('h4');?>><?php echo of_get_option('contact_second_l_title');?></h4>

                        <p <?php echo colors('p');?>><?php echo insert_br(of_get_option('contact_second_l_desc'));?></p>
                        
                    </div>
                    
                    <?php }?>
                    <?php if(of_get_option('show_contact_second_r')){?> 
                    
                    <div class="col-md-3">
                    
                        <h4 <?php echo colors('h4');?>><?php echo of_get_option('contact_second_r_title');?></h4>
                        
                        <p <?php echo colors('p');?>>
                        <?php 
                        for($i=1; $i<=of_get_option('contact_second_r_num'); $i++){
                            echo of_get_option('contact_second_r_info'.$i).'</br>';
                        }
                        ?>
                        </p>
                        
                    </div>
                    
                    <?php }?>
                    
                </div>
              
              <?php }?>
              
            </div>
        </article>
        <!-- end Contact -->


<?php if(of_get_option('show_sponsor_contact')) { ?>
        <article class="article-container">
            <div class="container" >

                <!-- arrows -->
                <div class="row" >
                    <div class="col-md-12">
                        <h2 <?php echo colors('h2');?> class="headers"><?php echo of_get_option('sponsor_title'); ?> </h2>
                        <span <?php echo colors('h1s');?> class="line" >
                            <span <?php echo colors('h1s');?> class="sub-line" ></span>
                        </span>
                        <?php if(of_get_option('sponsor_cant')>5) { ?>
                        <a <?php echo colors('a');?> class="slider-control pull-right next" href="#bodybuilding" data-slide="next"></a>
                        <a <?php echo colors('a');?> class="slider-control pull-right prev" href="#bodybuilding" data-slide="prev"></a>
                        <?php } ?>
                    </div>
                </div>
                <!-- end arrows -->

                <div class="row" >
                    <div id="bodybuilding" class="carousel slide">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <?php
                            $begin1='<div class="item active"><ul class="logos" >';
                            $last1 = '</ul></div>';
                              $begin2='<div class="item"><ul class="logos" >';
                        for ($log=1; $log <= of_get_option('sponsor_cant') ; $log++) {

                              if($log!=1)
                                 {
                                 $begin1 = '';
                                }
                            echo $begin1;
                             ?>
                                 <li><a <?php echo colors('a');?> href="<?php echo of_get_option('sponsor_link'.$log);  ?>"><img src="<?php echo of_get_option('sponsor_text'.$log); ?>" alt="<?php echo $log;?>" /></a></li>
                            <?php
                            $c=$log%5;
                            if($c==0)
                            {
                            echo $last1;
                            echo $begin2;

                            }

                            } ?>
                    </div>
                    </div>
                </div>
            </div>
        </article>
        <!-- end Bodybuilding Supplements -->
<?php } ?>
        
    </section>
    <!-- end Content -->
<?php
    }else{
        echo get_the_content();
    }
    get_footer(); ?>