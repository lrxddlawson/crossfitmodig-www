<?php get_header(); ?>

    <!-- begin Content -->
    <section class="gallery" id="gal2" >

       <?php if(of_get_option('show_banner_two')){?>
        <div class="sub-header" style="<?php echo 'background:url('.of_get_option('two_banner_image').') no-repeat;'?>">
            <div class="container">
                <div class="row" >
                    <ul class="sub-header-container" >
                        <li>
                            <h3 <?php echo colors('h3');?> class="title"><?php echo of_get_option('two_banner_text') ?></h3>
                        </li>
                        <li>
                            <ul class="custom-breadcrumb" >
                                 <li><h6 <?php echo colors('h6');?>><a <?php echo colors('a');?> href="<?php echo home_url();?>">Home</a></h6></li>
                                <li><i class="separator entypo-play" ></i></li>
                                <li><h6 <?php echo colors('h6');?>>Two</h6></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
<?php } ?>

        <!-- begin 2 columns gallery -->
        <article class="article-container">
            <div class="container" >

                <div class="row" >
                    <div class="col-md-12">
                    <h2 <?php echo colors('h2');?> class="article-title"><?php echo of_get_option('two_title'); ?></h2>
                    <span <?php echo colors('h1s');?> class="line" >
                        <span <?php echo colors('h1s');?> class="sub-line" ></span>
                    </span>
                    </div>
                </div>
                 <?php
                 for ($two=1; $two <= of_get_option('two_num_cant') ; $two++) {
                 $temp = $two+1;
                 $num = $temp%2;
                 $num2=$two%2;
                   ?>
                    <?php if($num==0){?><div class="row"><?php }?>
                        <div class="col-xs-6">
                        <a <?php echo colors('a');?> class="example-image-link" href="<?php echo of_get_option('two_image'.$two)?>" data-lightbox="example-1">
                            <img src="<?php echo of_get_option('two_image'.$two)?>" alt="img" />
                        </a>
                    </div>
                    <?php if($num2==0){?></div><?php } ?>
                    <?php

                     }?>


            </div>
        </article>
        <!-- end 2 columns gallery -->
<?php if(of_get_option('show_sponsor_two')) {?>
        <article class="article-container">
            <div class="container" >

                <!-- arrows -->
                <div class="row" >
                    <div class="col-md-12">
                        <h2 <?php echo colors('h2');?> class="headers"><?php echo of_get_option('sponsor_title'); ?> </h2>
                        <span <?php echo colors('h1s');?> class="line" >
                            <span <?php echo colors('h1s');?> class="sub-line" ></span>
                        </span>
                        <?php if(of_get_option('sponsor_cant')>5) { ?>
                        <a <?php echo colors('a');?> class="slider-control pull-right next" href="#bodybuilding" data-slide="next"></a>
                        <a <?php echo colors('a');?> class="slider-control pull-right prev" href="#bodybuilding" data-slide="prev"></a>
                        <?php } ?>
                    </div>
                </div>
                <!-- end arrows -->
                 <div class="row" >
                    <div id="bodybuilding" class="carousel slide">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <?php
                            $begin1='<div class="item active"><ul class="logos" >';
                            $last1 = '</ul></div>';
                            $begin2='<div class="item"><ul class="logos" >';
                        for ($log=1; $log <= of_get_option('sponsor_cant') ; $log++) {

                            if($log!=1)
                               {
                                $begin1 = '';
                               }
                            echo $begin1;
                             ?>
                                <li><a <?php echo colors('a');?> href="<?php echo of_get_option('sponsor_link'.$log);  ?>"><img src="<?php echo of_get_option('sponsor_text'.$log) ?>" alt="<?php echo $log;?>" /></a></li>
                            <?php
                            $c=$log%5;
                            if($c==0)
                            {
                            echo $last1;
                            echo $begin2;

                            }

                            } ?>
                    </div>
                    </div>
                </div>
            </div>
        </article>
        <?php  } ?>

<?php get_footer(); ?>