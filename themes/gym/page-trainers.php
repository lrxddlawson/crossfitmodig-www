<?php
    get_header();
    the_post();
?>
    <!-- Begin Content -->
    <section id="trainers" >
        <?php if(of_get_option('show_banner_trainer')) { ?>
    <div class="sub-header" style="<?php echo 'background:url('.of_get_option('trainer_banner_image').') no-repeat;'?>">
            <div class="container">
                <div class="row" >
                    <ul class="sub-header-container" >
                        <li>
                            <h3 <?php echo colors('h3');?> class="title"><?php echo of_get_option('trainer_banner_text');?></h3>
                        </li>
                        <li>
                            <?php if (of_get_option('show_breadcumbs_trainer')) { ?>
                            <ul class="custom-breadcrumb" >
                                 <li><h6 <?php echo colors('h6');?>><a <?php echo colors('a');?> href="<?php echo home_url();?>">Home</a></h6></li>
                                <li><i class="separator entypo-play" ></i></li>
                                <li><h6 <?php echo colors('h6');?>>Trainers</h6></li>
                            </ul>
                            <?php  } ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
         <?php } ?>
        <!-- end Sub Header -->

        <!--begin All Trainers -->
        <article class="article-container">
            <div class="container grid slider-grid" >
                <div class="row" >
                    <div class="col-md-12">
                        <h2 <?php echo colors('h2');?> class="article-title"><?php echo of_get_option('trainer_all_title'); ?></h2>
                        <span <?php echo colors('h1s');?> class="line" >
                            <span <?php echo colors('h1s');?> class="sub-line" ></span>
                        </span>
                        <a <?php echo colors('a');?> class="slider-control pull-right next" href="#slider-trainers" data-slide="next"></a>
                        <a <?php echo colors('a');?> class="slider-control pull-right prev" href="#slider-trainers" data-slide="prev"></a>
                    </div>
                </div>

                <div class="row content">
                    <div id="slider-trainers" class="carousel slide">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">

            <?php
                $args=array(
                    'post_type' => 'trainer',
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                );
                $my_query = null;
                $my_query = new WP_Query($args);
                $trainers = array();
                
                while ($my_query->have_posts()) {
                    $my_query->the_post();
                    $trainers[] = array(
                        'fast_description' => get_field('fast_description'),
                        'specialist' => get_field('specialist'),
                        'nick_name'=> get_the_title(),
                        'name' => get_field('name'),
                        'photo' => get_field('photo'),
                        'biografy' => get_field('biografy'),
                        'opinion' => get_field('opinion'),
                        'edu_subtitle' => get_field('education_subtitle'),
                        'education' => get_field('education'),
                        'social_link' => get_field('social_net'),
                        'link' => get_permalink(),
                        'cat' => get_field('category')
                    );
                    
                }
                
               for($i=1; $i<=count($trainers); $i++){  
                    if($i == 1){
                       echo '<div class="item active">';
                       
                    }
            ?>      
                    <div class="element <?php echo $i;?>" >
                        <div class="link" >
                            <img src="<?php echo $trainers[$i-1]['photo']?>" alt="//" />
                        </div>
                        <div class="title">
                            <h4 <?php echo colors('h4');?> class="testimonials-name" ><a href="<?php echo $trainers[$i-1]['link'] ?>"> <?php echo $trainers[$i-1]['name']?></a></h4>
                            <h6 <?php echo colors('h6');?> class="testimonials-position" ><a href="#"><?php echo $trainers[$i-1]['specialist']?></a></h6>
                            <p <?php echo colors('p');?>><?php echo $trainers[$i-1]['fast_description']?></p>
                            <ul class="social" >
                            <?php  foreach($trainers[$i-1]['social_link'] as $key){?>
                                <li><a <?php echo colors('a');?> href="<?php echo $key['social_link']?>"><?php echo $key['social_icon']?></a></li>
                            <?php }?>
                            </ul>
                        </div>
                    </div>
            <?php       
                    if($i%4 == 0 ){
                        echo '</div>';
                        if($i+1<=count($trainers)){echo '<div class="item">';}
                    }
                }
            ?>
                    </div>
                    </div>
                </div>

            </div>
        </article>
        <!--end All Trainers -->

        <!-- begin Suscribe -->
        <?php if(of_get_option('show_subscribe_trainer')) {?>
        <article class="article-container" id="suscribe">
            <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2 <?php echo colors('h2');?>><strong><?php echo of_get_option('trainer_subscribe_title') ?></strong></h2>
                    <p <?php echo colors('p');?>><?php echo of_get_option('trainer_subscribe_text') ?></p>
                </div>
                <div class="col-md-6">
                    <a <?php echo colors('a');?> href="<?php echo of_get_option('trainer_subscribe_link');?>">
                        <button class="button-gym big-button"><?php echo of_get_option('trainer_subscribe_signal') ?></button>
                    </a>
                </div>
            </div>
            </div>
        </article>
        <?php } ?>
        <!-- end Suscribe -->

        <!-- begin Trainers Gallery -->
        <article class="article-container">
            <div class="container grid magic-grid" >
                <div class="row" >
                    <div class="col-md-12">
                    <h2 <?php echo colors('h2');?> class="article-title" ><?php echo of_get_option('trainer_class_title');?></h2>
                    <span <?php echo colors('h1s');?> class="line" >
                        <span <?php echo colors('h1s');?> class="sub-line" ></span>
                    </span>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 filters" >
                        <button class="button-gym normal-button" data-filter="*">View All</button>
                    <?php 
                        $trainer_father_ID = get_cat_ID('Classes');
                        $categories = get_categories();
                        foreach($categories as $cat){
                            if($cat->parent == $trainer_father_ID){
                    ?>
                        <button class="button-gym normal-button" data-filter=".<?php echo $cat->slug?>" class=" filter-trainer <?php echo $cat->slug?>"><?php echo $cat->name?></button>
                    <?php
                            }
                        }
                     ?>
                    </div>
                </div>

                <div class="row content">
                
            <?php
                $args=array(
                    'post_type' => 'classes',
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                );
                $my_query = null;
                $my_query = new WP_Query($args);
                $classes = array();
                
                while ($my_query->have_posts()) {
                    $my_query->the_post();
                    $classes[] = array(
                        'previus_image' => get_field('previus_image'),
                        'title' => get_the_title(),
                        'teacher' => get_field('teacher'),
                        'category' => get_field('category'),
                        'link' => get_permalink()
                    );
                    
                }

                for($i=0; $i<count($classes); $i++){
                    if($classes[$i]['category'] != 0){
            ?>  
                    <div class="element <?php echo $classes[$i]['category'][0]->slug;?>" >
                        <div class="link" >                                                              
                            <a <?php echo colors('a');?> href="<?php echo $classes[$i]['link'] ?>"></a>
                            <img src="<?php echo $classes[$i]['previus_image'] ?>" />
                        </div>
                        <div class="title">
                            <h4 <?php echo colors('h4');?>><?php echo $classes[$i]['category'][0]->name; ?></h4>
                            <p <?php echo colors('p');?>><?php 
                                $value = get_post_custom($classes[$i]['teacher']->ID);
                                echo 'Teacher: '.$value['name'][0];
                            ?></p>                          
                        </div>
                    </div>
                <?php
                        }
                    }
                ?>
                   
                </div>

            </div>
        </article>
        <!-- end Trainers Gallery -->


        <!-- begin Bodybuilding Supplements -->
<?php if(of_get_option('show_sponsor_trainer1')) { ?>
        <article class="article-container">
            <div class="container" >

                <!-- arrows -->
                <div class="row" >
                    <div class="col-md-12">
                        <h2 <?php echo colors('h2');?> class="headers"><?php echo of_get_option('sponsor_title'); ?> </h2>
                        <span <?php echo colors('h1s');?> class="line" >
                            <span <?php echo colors('h1s');?> class="sub-line" ></span>
                        </span>
                        <?php if(of_get_option('sponsor_cant')>5) { ?>
                        <a <?php echo colors('a');?> class="slider-control pull-right next" href="#bodybuilding" data-slide="next"></a>
                        <a <?php echo colors('a');?> class="slider-control pull-right prev" href="#bodybuilding" data-slide="prev"></a>
                        <?php } ?>
                    </div>
                </div>
                <!-- end arrows -->

                <div class="row" >
                    <div id="bodybuilding" class="carousel slide">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <?php
                            $begin1='<div class="item active"><ul class="logos" >';
                            $last1 = '</ul></div>';
                            $begin2='<div class="item"><ul class="logos" >';
                            for ($log=1; $log <= of_get_option('sponsor_cant') ; $log++) {

                                if($log!=1){
                                    $begin1 = '';
                                }
                                echo $begin1;
                        ?>
                        <li>
                            <a <?php echo colors('a');?> href="<?php echo of_get_option('sponsor_link'.$log);  ?>">
                                <img src="<?php echo of_get_option('sponsor_text'.$log); ?>" alt="<?php echo $log;?>" />
                            </a>
                        </li>
                        <?php
                                $c=$log%5;
                                if($c==0){
                                    echo $last1;
                                    echo $begin2;
                                }
                            } 
                        ?>
                    </div>
                    </div>
                </div>
            </div>
        </article>
        <?php } ?>
        <!-- end Bodybuilding Supplements --> 

    </section>
    <!-- end content -->

<?php 
    get_footer();
?>