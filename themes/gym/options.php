<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 */

function optionsframework_option_name() {

	// This gets the theme name from the stylesheet
	$themename = get_option( 'stylesheet' );
	$themename = preg_replace("/\W/", "_", strtolower($themename) );

	$optionsframework_settings = get_option( 'optionsframework' );
	$optionsframework_settings['id'] = $themename;
	update_option( 'optionsframework', $optionsframework_settings );
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'tecno'
 * with the actual text domain for your theme.  Read more:
 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
 */

function optionsframework_options() {



	$wp_editor_settings = array(
		'wpautop' => true, // Default
		'textarea_rows' => 5,
		'tinymce' => array( 'plugins' => 'wordpress' )
	);

    //GLOBAL ARRAYS IMG

    $local_ad = get_stylesheet_directory_uri();

    $info_rep = array(
            'icon1' => $local_ad.'/img/info/icon1.png',
            'icon2' => $local_ad.'/img/info/icon2.png',
            'icon3' => $local_ad.'/img/info/icon3.png',
            'icon4' => $local_ad.'/img/info/icon4.png',
            'icon5' => $local_ad.'/img/info/icon5.png',
            'icon6' => $local_ad.'/img/info/icon6.png',
            'icon7' => $local_ad.'/img/info/icon7.png',
            'icon8' => $local_ad.'/img/info/icon8.png'
    );
    $footer_contact = array(
        'footer_mb' => $local_ad.'/img/footer/footer_mb.png',
        'footer_email' => $local_ad.'/img/footer/footer_email.png',
		'footer_mail' => $local_ad.'/img/footer/footer_mail.png',
        'footer_address' => $local_ad.'/img/footer/footer_address.png',
		'footer_locat' => $local_ad.'/img/footer/footer_locat.png',
		'footer_ring' => $local_ad.'/img/footer/footer_ring.png',
		'footer_location' => $local_ad.'/img/footer/footer_location.png',
		'footer_phone' => $local_ad.'/img/footer/footer_phone.png'
    );
     $link_img = get_stylesheet_directory_uri().'/img/social/';
          $social_n = array(
            '1' => array('Facebook','www.facebook.com',array(
                    'img_1' => $link_img.'facebook.png',
                    'img_2' => $link_img.'facebook_1.png',
                    'img_3' => $link_img.'facebook_2.png',
                    'img_4' => $link_img.'facebook_3.png')),
            '2' => array('Dribble','www.dribble.com',array(
                    'img_1' => $link_img.'dribble.png',
                    'img_2' => $link_img.'dribble_1.png',
                    'img_3' => $link_img.'dribble_2.png',
                    'img_4' => $link_img.'dribble_3.png')),
            '3' => array('Twitter','www.twitter.com',array(
                    'img_1' => $link_img.'twitter.png',
                    'img_2' => $link_img.'twitter_1.png',
                    'img_3' => $link_img.'twitter_2.png',
                    'img_4' => $link_img.'twitter_3.png')),
            '4' => array('Linkedin','www.linkedin.com',array(
                    'img_1' => $link_img.'linkedin.png',
                    'img_2' => $link_img.'linkedin_1.png',
                    'img_3' => $link_img.'linkedin_2.png',
                    'img_4' => $link_img.'linkedin_3.png')),
            '5' => array('Google','www.google.com',array(
                    'img_1' => $link_img.'google.png',
                    'img_2' => $link_img.'google_1.png',
                    'img_3' => $link_img.'google_2.png',
                    'img_4' => $link_img.'google_3.png')),
            '6' => array('Flickr','www.Flickr.com',array(
                    'img_1' => $link_img.'flickr.png',
                    'img_2' => $link_img.'flickr_1.png',
                    'img_3' => $link_img.'flickr_2.png',
                    'img_4' => $link_img.'flickr_3.png')),
            '7' => array('You-Tube','www.youtube.com',array(
                    'img_1' => $link_img.'you-tube.png',
                    'img_2' => $link_img.'you-tube_1.png',
                    'img_3' => $link_img.'you-tube_2.png',
                    'img_4' => $link_img.'you-tube_3.png')),
             '8' => array('Rss','www.rss.com',array(
                    'img_1' => $link_img.'rss.png',
                    'img_2' => $link_img.'rss_1.png',
                    'img_3' => $link_img.'rss_2.png',
                    'img_4' => $link_img.'rss_3.png'))
             );

  $social_link = array(
        'img_1' => $local_ad.'/img/social_net/twitter_b.png',
        'img_2' => $local_ad.'/img/social_net/facebook_b.png',
        'img_3' => $local_ad.'/img/social_net/dribble_b.png',
        'img_4' => $local_ad.'/img/social_net/gmail_b.png'
  );

  //PAGE STYLE
  $styles_pages_sele = array(
            '1' => 'No-style',
            '2' => 'Home',
            '3' => 'Class',
            '4' => 'Trainers',
            '5' => 'Club',
            '6' => 'Faq',
            '7' => 'Gallery_Two',
            '8' => 'Gallery_Three',
            '9' => 'Gallery_Four',
            '10' => 'Blog',
            '11' => 'Price',
            '12' => 'Contact',
            '13' => 'Tabs'
     );
  //LOAD PAGES
     $paginas = array();
     $pages_obj = get_pages();
     foreach ($pages_obj as $page) {
         $paginas[$page->ID] = array($page->post_title,$page->post_name);
            $pag[$page->post_name] = $page->post_title;
     }
     ksort($paginas);
     $styles_pages = array_keys($paginas);

   //LOAD CONFIGURATIONS PAGES
  include 'option_files/option-general.php';
  include 'option_files/option-home.php';
  include 'option_files/option-class.php';
  include 'option_files/option-trainer.php';
  include 'option_files/option-club.php';
  include 'option_files/option-faq.php';
  include 'option_files/option-tabs.php';
  include 'option_files/option-two.php';
  include 'option_files/option-three.php';
  include 'option_files/option-four.php';
  include 'option_files/option-blog.php';
  include 'option_files/option-price.php';
  include 'option_files/option-contact.php';
  include 'option_files/option-background.php';
  include 'option_files/option-footer.php';
  include 'option_files/option-404.php';


	return $options;
}

?>
