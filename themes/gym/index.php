<?php get_header() ?>
    <!-- end Header -->

    <!-- begin Content -->
    <section id="news" >
        <!-- begin Sub Header -->
        <div class="sub-header" style="background: url('<?php echo get_template_directory_uri();?>/img/index.jpg');">
        
            <div class="container">
            
                <div class="row" >
                
                    <ul class="sub-header-container" >
                    
                        <li>
                        
                            <h3 <?php echo colors('h3');?> class="title">Welcomes</h3>
                            
                        </li>
                        
                    </ul>
                    
                </div>
                
            </div>
            
        </div>

    <!-- begin Sub Header -->
	<article class="article-container">
    
        <div class="container" >
        
            <div class="row" >
                <!-- begin Main Colum -->
                    
                <div class="col-sm-9">
                
                    <h2 <?php echo colors('h2');?> class="article-title" >Blog and News</h2>
                    
                    <span <?php echo colors('h1s');?> class="line" >
                    
                        <span <?php echo colors('h1s');?> class="sub-line" ></span>
                        
                    </span>
                    
                    <section class="posts">
                    
                    <?php get_template_part( 'loop' ); ?>
                    
                        <aside>
                        
                            <ul class="pagination">
                            
                            <?php gym_pagination();?>
                            
                            </ul>
                            
                        </aside>
                        
                    </section>
                    
                </div>
            <!-- end Main Colum -->

            <!-- begin Sidebar -->
            <aside class="col-sm-3 sidebar">
            
                <?php get_sidebar(); ?>
                
            </aside>
            <!-- end Sidebar -->
            
            </div>
            
        </div>
        
    </article>
        <!-- end News -->

    </section>
    <!-- end Content -->

<?php get_footer(); ?>