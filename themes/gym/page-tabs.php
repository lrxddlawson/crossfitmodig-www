<?php 
    get_header();
    the_post();
?>
    <!-- begin Content -->
    <section id="tabs" >
    <?php if(of_get_option('show_banner_tab')) { ?>
    <div class="sub-header" style="<?php echo 'background:url('.of_get_option('tab_banner_image').') no-repeat;'?>">
            <div class="container">
                <div class="row" >
                    <ul class="sub-header-container" >
                        <li>
                            <h3 <?php echo colors('h3');?> class="title"><?php echo of_get_option('tab_banner_text');?></h3>
                        </li>
                        <li>
                            <?php if (of_get_option('show_breadcumbs_tab')) { ?>
                            <ul class="custom-breadcrumb" >
                                 <li><h6 <?php echo colors('h6');?>><a <?php echo colors('a');?> href="<?php echo home_url();?>">Home</a></h6></li>
                                <li><i class="separator entypo-play" ></i></li>
                                <li><h6 <?php echo colors('h6');?>>Tabs</h6></li>
                            </ul>
                            <?php  } ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
         <?php } ?>
         <?php if(of_get_option('show_tab')) { ?>
        <!-- begin tabs -->
        <article class="article-container">
            <div class="container" >

                <div class="row" >
                    <div class="col-md-12">
                    <h2 <?php echo colors('h2');?> class="article-title" ><?php echo of_get_option('tab_title')?></h2>
                    <span <?php echo colors('h1s');?> class="line" >
                        <span <?php echo colors('h1s');?> class="sub-line" ></span>
                    </span>
                    </div>
                </div>
                           
                <div class="row" >
                        <section class="col-sm-12">
                        <!-- TABS -->
                        <?php
                            for($i=1; $i<=of_get_option('tab_num'); $i++){
                                $sec = '';
                        ?>
                            <ul class="nav nav-tabs <?php if(of_get_option('tab_back_tab'.$i) == 1){echo 'nav-tabs-sec'; $sec='-sec';}?>" id="myTab">
                                <?php for($j=1; $j<=of_get_option('tab_num_tab'.$i); $j++){?>
                                <li class="<?php if($j == '1'){echo 'active';}?>">
                                    <a <?php echo colors('a');?> href="#<?php echo 'tab'.$i.$j.$sec;?>" data-toggle="tab">
                                        <?php echo of_get_option('tab_tab_title'.$i.$j);?>
                                    </a>
                                </li>
                                <?php }?>
                            </ul>

                            <div class="tab-content <?php if(of_get_option('tab_back_tab'.$i) == 1){echo 'nav-tabs-sec'; $sec='-sec';}?>">
                                <?php for($j=1; $j<=of_get_option('tab_num_tab'.$i); $j++){?>
                                <div class="tab-pane <?php if($j == '1'){echo 'active';}?>" id="<?php echo 'tab'.$i.$j.$sec;?>">
                                    <p <?php echo colors('p');?>><?php echo of_get_option('tab_tab_desc'.$i.$j);?></p>
                                </div>
                                <?php }?>
                            </div>
                            
                            <br />
                        <?php
                            }
                        ?>
                        
                        </section>
                    </div>
                </div>
                <!-- end Main Colum -->

                </div>
            </div>
        </article>
        <!-- end tabs -->
 
        <!-- begin Bodybuilding Supplements -->
        <?php if(of_get_option('show_sponsor_tab')) { ?>
        <article class="article-container">
            <div class="container" >

                <!-- arrows -->
                <div class="row" >
                    <div class="col-md-12">
                        <h2 class="headers"><?php echo of_get_option('sponsor_title'); ?> </h2>
                        <span class="line" >
                            <span class="sub-line" ></span>
                        </span>
                        <?php if(of_get_option('sponsor_cant')>5) { ?>
                        <a <?php echo colors('a');?> class="slider-control pull-right next" href="#bodybuilding" data-slide="next"></a>
                        <a <?php echo colors('a');?> class="slider-control pull-right prev" href="#bodybuilding" data-slide="prev"></a>
                        <?php } ?>
                    </div>
                </div>
                <!-- end arrows -->

                <div class="row" >
                    <div id="bodybuilding" class="carousel slide">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <?php
                            $begin1='<div class="item active"><ul class="logos" >';
                            $last1 = '</ul></div>';
                            $begin2='<div class="item"><ul class="logos" >';
                            for ($log=1; $log <= of_get_option('sponsor_cant') ; $log++) {

                                if($log!=1){
                                    $begin1 = '';
                                }
                                echo $begin1;
                        ?>
                        <li>
                            <a <?php echo colors('a');?> href="<?php echo of_get_option('sponsor_link'.$log);  ?>">
                                <img src="<?php echo of_get_option('sponsor_text'.$log); ?>" alt="<?php echo $log;?>" />
                            </a>
                        </li>
                        <?php
                                $c=$log%5;
                                if($c==0){
                                    echo $last1;
                                    echo $begin2;
                                }
                            } 
                        ?>
                    </div>
                    </div>
                </div>
            </div>
        </article>
        <?php } ?>
        <!-- end Bodybuilding Supplements -->
    <?php }?>
    </section>
    <!-- end Content -->
<?php get_footer();?>