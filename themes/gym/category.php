<?php get_header() ?>

    <!-- end Header -->

    <!-- begin Content -->
	
    <section id="news" >
	
        <!-- begin Sub Header -->
		
       <?php if(of_get_option('show_banner_blog')){?>
	   
        <div class="sub-header" style="<?php echo 'background:url('.of_get_option('blog_banner_image').') no-repeat;'?>">
		
            <div class="container">
			
                <div class="row" >
				
                    <ul class="sub-header-container" >
					
                        <li>
						
                            <h3 <?php echo colors('h3');?> class="title"><?php echo of_get_option('blog_banner_text') ?></h3>
							
                        </li>
						
                        <li>
						
                            <ul class="custom-breadcrumb" >
							
                                <li><h6 <?php echo colors('h6');?>><a <?php echo colors('a');?> href="<?php echo home_url();?>">Home</a></h6></li>
								 
                                <li><i class="separator entypo-play" ></i></li>
								
                                <li><h6 <?php echo colors('h6');?>>Classes</h6></li>
								
                            </ul>
							
                        </li>
						
                    </ul>
					
                </div>
				
            </div>
			
        </div>
		
        <?php } ?> <!-- begin Sub Header -->

        <!-- begin News -->
		
        <article class="article-container">
		
            <div class="container" >
			
                <div class="row" >
				
                    <!-- begin Main Colum -->
					
                    <?php if(of_get_option('layer_img') == 'left'){?>
					
                    <aside class="col-sm-3 sidebar">
					
                        <?php get_sidebar() ?>
						
                    </aside>
					
                    <?php }?>
					
                    <div class="<?php if(of_get_option('layer_img') == 'none'){echo 'col-sm-12';}else{echo 'col-sm-9';}?>">
					
                        <h2 <?php echo colors('h2');?> class="article-title" ><?php echo of_get_option('blog_title')?></h2>
						
                        <span <?php echo colors('h1s');?> class="line" >
						
                            <span <?php echo colors('h1s');?> class="sub-line" ></span>
							
                        </span>
						
                        <section class="posts">
						
                        <?php
							$paginar=1;
							$page = isset($_GET['page']) ? $_GET['page'] : 1;
							$catID = get_query_var('cat');
							query_posts('cat='.$catID.'&posts_per_page='.$posts_per_page.'&paged='.$page);
							while (have_posts()) {
                            the_post();
                            $day = get_the_time('F j, Y');
                            $cate = get_the_category(get_the_id());
                            foreach($cate as $k){
                            }
                        ?>

                            <article class="post">
							
                                <?php the_post_thumbnail();?>
								
                                <header>
								
                                    <h3 <?php echo colors('h3');?>><?php echo the_title();?></h3>
									
                                </header>
								
                                <h4 <?php echo colors('h4');?>><?php echo the_title();?></h4>

                                <p <?php echo colors('p');?>><?php echo wp_trim_words(get_the_content(),40,'...');?></p>
								
                                <a href="<?php the_permalink();?>" class="readmore">Read more <i class="entypo-right-open" ></i></a>
								
                                <footer>
								
                                    <span <?php echo colors('h1s');?>><i class="entypo-pencil" ></i><?php the_author();?></span>
									
                                    <span <?php echo colors('h1s');?>><i class="entypo-calendar" ></i><?php echo $day;?></span>
									
                                    <span <?php echo colors('h1s');?>><i class="entypo-comment" ></i><?php comments_number();?></span>
									
                                </footer>
								
                            </article>
							
                            <?php  } ?>
							
                            <aside>
							
                                <ul class="pagination">
								
                                   <?php
										if ($paginar == '1') {
										gym_pagination();
										}
										else
										{
										next_posts_link('&larr; '.'Older posts', 'mythemeshop' );
										previous_posts_link('Newer posts'.' &rarr;', 'mythemeshop' );
										}
									?>

								</ul>
								
                            </aside>
							
                        </section>

                    </div>
					
                    <!-- end Main Colum -->

                    <!-- begin Sidebar -->
					
                    <?php if(of_get_option('layer_img') == 'left'){?>
					
                    <aside class="col-sm-3 sidebar">
					
                        <?php get_sidebar() ?>
						
                    </aside>
					
                    <?php }?>
					
                    <!-- end Sidebar -->
					
                </div>
				
            </div>
			
        </article>
		
        <!-- end News -->

    </section>
	
    <!-- end Content -->

<?php get_footer() ?>