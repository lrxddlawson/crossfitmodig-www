<?php
    get_header();
    the_post();
?>
    <?php if(of_get_option('show_price')){?>
    
    <!-- begin Content -->
    <section id="price" >
    
        <?php if(of_get_option('show_banner_price')){?>
    
        <!-- begin Sub Header -->
        <div class="sub-header" style="<?php echo 'background:url('.of_get_option('price_banner_image').') no-repeat;'?>">
            <div class="container">
                <div class="row" >
                    <ul class="sub-header-container" >
                        <li>
                            <h3 <?php echo colors('h3');?> class="title"><?php echo of_get_option('price_banner_text');?></h3>
                        </li>
                        <li>
                        <?php if(of_get_option('show_breadcumbs_price')){?>
                            <ul class="custom-breadcrumb" >
                                <li><h6 <?php echo colors('h6');?>><a <?php echo colors('a');?> href="index.html">Home</a></h6></li>
                                <li><i class="separator entypo-play" ></i></li>
                                <li><h6 <?php echo colors('h6');?>>Pricing Tables</h6></li>
                            </ul>                    
                        <?php }?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- end Sub Header -->
        
        <?php }?>
        <?php if(of_get_option('show_price_first')){?>
        
        <?php
                    $args=array(
                        'post_type' => 'plan',
                        'post_status' => 'publish',
                        'posts_per_page' => -1,
                    );
                    $my_query = null;
                    $my_query = new WP_Query($args);
                    $plan = array();
                    
                    while ($my_query->have_posts()) {
                        $my_query->the_post();
                        $plan[] = array(
                            'title' => get_the_title(),
                            'price' => get_field('price'),
                            'freq' => get_field('frequency'),
                            'data' => get_field('other_data'),
                            'btn' => get_field('button')
                        );
                    }
                    $c = 0;
                
                     if(of_get_option('price_first_style') == '1'){
                         $plan_style = 'col-md-4';
                         $style = 'std';
                     }else{
                         $plan_style = 'col-sm-6 col-md-3';
                         $style = 'spc';
                     }
        ?>
        <!-- begin Prices -->
        <article class="article-container">
            <?php 
                $member = '';
                if(of_get_option('price_first_style') == '1'){$member = 'membership-price';}
             ?>
            <div class="container <?php echo $member;?>" >
                
                <div class="row" >
                    <div class="col-md-12">
                    <h2 <?php echo colors('h2');?> class="article-title" ><?php echo of_get_option('price_first_title');?></h2>
                    <span <?php echo colors('h1s');?> class="line" >
                        <span <?php echo colors('h1s');?> class="sub-line" ></span>
                    </span>
                    </div>
                </div>

                <div class="row pricing">
                        <!-- Pricing tables -->
                <?php     
                     $c=0;
                     foreach($plan as $key){
                         $c++;
                         if(of_get_option('price_first_plan'.$c) == '1'){
                 ?>
                        <div class="<?php echo $plan_style;?>" >
                            <ul class="pricing-table" >
                                <li class="title" >
                                    <p <?php echo colors('p');?> class="item-title" ><?php echo $key['title'];?></p>
                                    <p <?php echo colors('p');?> class="item-price" >
                                        <?php if($style == 'spc'){?>
                                            <strong><?php echo $key['price'];?></strong> <?php echo $key['freq'];?>
                                        <?php }else{?>
                                            <?php echo substr($key['price'],0,stripos($key['price'],'.'));?>
                                            <?php echo substr($key['price'],stripos($key['price'],'.'));?>
                                        <?php }?>
                                    </p>
                                </li>
                                <?php foreach($key['data'] as $data){?>
                                <li class="features" ><?php echo $data['data'];?></li>
                                <?php }?>
                                <li class="button" >
                                    <a <?php echo colors('a');?> href="<?php echo $key['btn'][0]['btn_link'];?>"><?php echo $key['btn'][0]['btn_text'];?></a>
                                </li>
                            </ul>
                        </div>
                   <?php
                         }
                     }
                   ?>
                   
                </div>
            </div>
        </article>
        <!-- end Prices -->

        <?php }?>
        <?php if(of_get_option('show_price_second')){?>
        
        <!-- begin Membership -->
        <article>
            <div class="container">
                <div class="row membership">
                    <div class="col-md-12">
                        <h2 <?php echo colors('h2');?>><?php echo of_get_option('price_second_title');?></h2>
                        <p <?php echo colors('p');?>><?php echo insert_br(of_get_option('price_second_desc'));?></p>
                        <span <?php echo colors('h1s');?>><a <?php echo colors('a');?> href="<?php echo of_get_option('price_second_btn_link');?>"><?php echo of_get_option('price_second_btn');?></a></span>
                    </div>
                </div>
            </div>
        </article>
        <!-- end Membership -->
        
        <?php }?>
        <?php if(of_get_option('show_price_third')){?>
        
        <!-- begin All Prices -->
        <article class="article-container">
            <?php 
                $member = '0';
                if(of_get_option('price_third_style') == '1'){$member = 'membership-price';}
             ?>
            <div class="container <?php echo $member;?>" >
                
                <div class="row ">
                
                    <div class="col-md-12">
                
                        <h2 <?php echo colors('h2');?> class="article-title" ><?php echo of_get_option('price_third_title');?></h2>
                
                        <span <?php echo colors('h1s');?> class="line" >
                
                            <span <?php echo colors('h1s');?> class="sub-line" ></span>
                
                        </span>
                    
                    </div>
                
                </div>

                <div class="row pricing">
                
                <?php
                    $c = 0;
                
                     if(of_get_option('price_third_style') == '1'){
                         $plan_style = 'col-md-4';
                         $style = 'std';
                     }else{
                         $plan_style = 'col-sm-6 col-md-3';
                         $style = 'spc';
                     }
                     
                     foreach($plan as $key){
                         $c++;
                         if(of_get_option('price_third_plan'.$c) == '1'){
                 ?>
                        <div class="<?php echo $plan_style;?>" >
                            <ul class="pricing-table" >
                                <li class="title" >
                                    <p <?php echo colors('p');?> class="item-title" ><?php echo $key['title'];?></p>
                                    <p <?php echo colors('p');?> class="item-price" >
                                        <?php if($style == 'spc'){?>
                                            <strong><?php echo $key['price'];?></strong> <?php echo $key['freq'];?>
                                        <?php }else{?>
                                            <?php echo substr($key['price'],0,stripos($key['price'],'.'));?>
                                            <?php echo substr($key['price'],stripos($key['price'],'.'));?>
                                        <?php }?>
                                    </p>
                                </li>
                                <?php foreach($key['data'] as $data){?>
                                <li class="features" ><?php echo $data['data'];?></li>
                                <?php }?>
                                <li class="button" >
                                    <a <?php echo colors('a');?> href="<?php echo $key['btn'][0]['btn_link'];?>"><?php echo $key['btn'][0]['btn_text'];?></a>
                                </li>
                            </ul>
                        </div>
                <?php 
                         }
                     }
                ?>
                </div>
            </div>
        </article>
        <!-- end All Prices --> 
        
        <?php }?>
        
    </section>
    <!-- begin Content -->
 <?php 
    }else{
        echo get_the_content();
    }
    get_footer();
 ?>