<?php
    get_header();
?>
    <!-- begin Content -->
    <section id="single-class" >
    <?php if(of_get_option('show_banner_single_class')) { ?>
        <!-- begin Sub Header -->
        <div class="sub-header" style="<?php echo 'background:url('.of_get_option('class_banner_single_image').') no-repeat;'?>">
        
            <div class="container">
            
                <div class="row" >
                
                    <ul class="sub-header-container" >
                    
                        <li>
                        
                            <h3 <?php echo colors('h3');?> class="title"><?php echo of_get_option('class_banner_single_text'); ?></h3>
                            
                        </li>
                        
                        <li>
                            <?php if (of_get_option('show_breadcumbs_single_class')) { ?>
                            <ul class="custom-breadcrumb" >
                            
                                <li><h6 <?php echo colors('h6');?>><a <?php echo colors('a');?> href="<?php echo home_url();?>">Home</a></h6></li>
                                
                                <li><i class="separator entypo-play" ></i></li>
                                
                                <li><h6 <?php echo colors('h6');?>><a <?php echo colors('a');?> href="#">Classes</a></h6></li>
                                
                                <li><i class="separator entypo-play" ></i></li>
                                
                                <li><h6 <?php echo colors('h1s');?>>Single Class</h6></li>
                                
                            </ul> 
                            <?php }?>                   
                        </li>
                        
                    </ul>
                    
                </div>
                
            </div>
            
        </div>
        <!-- end Sub Header -->
     <?php }?>   
        <!-- begin Sub Header -->
        <article class="article-container">    
        
            <div class="container" >
            
                <div class="row" >
                
                    <div class="col-md-12">
                    
                    <h2 <?php echo colors('h2');?> class="article-title" ><?php echo get_the_title();?></h2>
                    
                    <span <?php echo colors('h1s');?> class="line" >
                    
                        <span <?php echo colors('h1s');?> class="sub-line" ></span>
                        
                    </span>
                    
                    </div>
                    
                </div>
               <?php 
                $value = get_field('show_gallery');
                 if($value[0] == '1'){
                    
               ?> 
                <div class="row">
                
                    <div class=" col-xs-12">
                    
                        <!-- Slider -->                             
                        <div class="bannercontainer">    
                           
                        <div class="banner ">
                        
                            <ul>                  
                                <?php                            
                                     if(get_field('img_gallery') != ''){
                                     foreach(get_field('img_gallery') as $key){
                                 ?>
                                  <li data-transition="boxslide" data-slotamount="1">
                                    <img src="<?php echo $key['url'];?>" alt="//" />
                                  </li>
                                    
                                <?php
                                     }
                                     }
                                 ?>
                                    
                                
                                
                            </ul>
                        </div>
                        </div>
                        <!-- End Slider -->
                    </div>
                </div>
               <?php
                 }
               ?>
                <div class="row">
                <?php 
                    if(get_field('show_class_figure')){
                ?>
                    <div class="col-md-8">
                        <h2 <?php echo colors('h2');?>><?php echo get_field('subtitle');?></h2>
                        <p <?php echo colors('p');?>><?php echo get_field('content');?></p>
                    </div>
                <?php
                    }
                 ?>
                 <?php 
                    if(get_field('show_class_detail')){
                ?>
                    <div class="col-md-4">
                        <div class="panel panel-gym">
                            <div class="panel-heading">
                                <h3 <?php echo colors('h3');?> class="panel-title"><?php echo get_field('class_detail_title');?></h3>
                            </div>
                            <div class="panel-body">
                                <ul class="custom-icon-list" >
                                    <?php
                                       foreach(get_field('detail_features') as $key){  
                                    ?>
                                    <li><?php echo $key['detail_icon'].'<p>'.$key['detail_text'].'</p>';?></li>
                                    <?php
                                       }     
                                    ?>                                    
                                </ul>
                            </div>
                            <div class="panel-footer"><a href="<?php echo get_field('button_link');?>"><?php echo get_field('button_title');?></a></div>
                        </div>
                    </div>
                 <?php
                    }
                 ?>   
                </div>
            </div>
        </article>
        <?php if(of_get_option('show_features_class')){?>
        <!-- begin Related Classes -->
        <article class="article-container">
            <div class="container" >
                <div class="row" >
                    <div class="col-xs-12">
                    <h2 <?php echo colors('h2');?> class="headers"><?php echo of_get_option('features_class_title');?></h2>
                    <span <?php echo colors('h1s');?> class="line" >
                        <span <?php echo colors('h1s');?> class="sub-line" ></span>
                    </span>
                    <a <?php echo colors('a');?> class="button-gym normal-button view-all pull-right no-margin" href="<?php echo of_get_option('features_class_btn_url');?>" ><?php echo of_get_option('features_class_btn_link');?></a>
                    </div>
                </div>
                
                <?php
                     for($i=1; $i<=of_get_option('features_class_num'); $i++){
                         if($i==1 || $i%4 == 0){echo '<div class="row" >';}
                 ?>
                    <div class="col-xs-3 col-sm-1" >
                        <img src="<?php echo of_get_option('features_img'.$i);?>" alt="//" />
                    </div>
                    <div class="col-xs-9 col-sm-3 fc-container" >
                        <h3 <?php echo colors('h3');?>><?php echo of_get_option('features_title'.$i);?></h3>
                        <p <?php echo colors('p');?>><?php echo insert_br(of_get_option('features_desc'.$i));?></p>
                        <a <?php echo colors('a');?> href="<?php echo of_get_option('features_btn_link'.$i);?>"><?php echo of_get_option('features_btn'.$i);?>...</a>
                    </div>
                 <?php
                        if($i==of_get_option('features_class_num') || $i%3 == 0){echo '</div>';}
                     }
                  ?>
              </div>
        </article>  
        <!-- end Related Classes -->
        <?php }?>
        
        <article class="article-container">
            <div class="container" >
            <div class="row">
            <?php if(of_get_option('show_week_class')){?>
                <div class="col-md-8">
                    <h2 <?php echo colors('h2');?> class="article-title"><?php echo of_get_option('week_class_title');?></h2>
                    <span <?php echo colors('h1s');?> class="line" >
                        <span <?php echo colors('h1s');?> class="sub-line" ></span>
                    </span>
                    
                    <!-- TABS -->
                    <ul class="nav nav-tabs" id="myTab">
                    <?php
                        for($i=1; $i<=of_get_option('week_class_num'); $i++){
                            if($i==1){$active = 'class="active"';}else{$active = '';}
                    ?>
                        <li <?php echo $active;?>><a href="#<?php echo str_replace(' ','_',of_get_option('week_title'.$i));?>" data-toggle="tab"><?php echo of_get_option('week_title'.$i);?></a></li>
                    <?php }?>
                    </ul>
                     
                    <div class="tab-content">
                    <?php
                        for($i=1; $i<=of_get_option('week_class_num'); $i++){
                            if($i==1){$active = 'active';}else{$active = '';}
                    ?>
                        <div class="tab-pane <?php echo $active;?>" id="<?php echo str_replace(' ','_',of_get_option('week_title'.$i));?>">
                            <p <?php echo colors('p');?>><?php echo insert_br(of_get_option('week_desc'.$i));?></p>
                        </div>
                    <?php }?>
                        
                    </div>
                </div>
            <?php }?>
            <?php if(of_get_option('show_test_class')){?>
                <div class="col-md-4">
                    <h2 <?php echo colors('h2');?> class="article-title"><?php echo of_get_option('test_class_title');?></h2>
                    <span <?php echo colors('h1s');?> class="line" >
                        <span <?php echo colors('h1s');?> class="sub-line" ></span>
                    </span>
                    <div id="testimonials" class="carousel slide">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                     <?php
                        for($i=1; $i<=of_get_option('test_class_num'); $i++){
                            if($i==1){$active = 'active';}else{$active = '';}
                    ?>   
                        <div class="item <?php echo $active;?>">
                            <div class="testimonial-container sec" >                                        
                                <p <?php echo colors('p');?>>
                                    <span <?php echo colors('h1s');?> class="img-container" >
                                        <img src="<?php echo of_get_option('test_img'.$i);?>" alt="img">
                                    </span>
                                    "<?php echo of_get_option('test_desc'.$i);?>"
                                </p>
                                <span <?php echo colors('h1s');?> class="border">
                                    <span <?php echo colors('h1s');?> class="back" ></span>
                                </span>
                            </div>
                            <h4 <?php echo colors('h4');?> class="testimonials-name" ><?php echo of_get_option('test_name'.$i);?></h4>
                            <h6 <?php echo colors('h6');?> class="testimonials-position" ><?php echo $i.' '.of_get_option('test_dtl'.$i);?></h6>
                        </div>
                    <?php }?>

                    </div>
                    </div>

                </div> 
            <?php }?>   
            </div>
            </div>
        </article>
       
        
        <?php if(of_get_option('show_sponsor_single_class')) { ?>
        <article class="article-container">
            <div class="container" >

                <!-- arrows -->
                <div class="row" >
                    <div class="col-md-12">
                        <h2 <?php echo colors('h2');?> class="headers"><?php echo of_get_option('sponsor_title'); ?> </h2>
                        <span <?php echo colors('h1s');?> class="line" >
                            <span <?php echo colors('h1s');?> class="sub-line" ></span>
                        </span>
                        <?php if(of_get_option('sponsor_cant')>5) { ?>
                        <a <?php echo colors('a');?> class="slider-control pull-right next" href="#bodybuilding" data-slide="next"></a>
                        <a <?php echo colors('a');?> class="slider-control pull-right prev" href="#bodybuilding" data-slide="prev"></a>
                        <?php } ?>
                    </div>
                </div>
                <!-- end arrows -->

                <div class="row" >
                    <div id="bodybuilding" class="carousel slide">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <?php
                            $begin1='<div class="item active"><ul class="logos" >';
                            $last1 = '</ul></div>';
                            $begin2='<div class="item"><ul class="logos" >';
                            for ($log=1; $log <= of_get_option('sponsor_cant') ; $log++) {

                                if($log!=1){
                                    $begin1 = '';
                                }
                                echo $begin1;
                        ?>
                        <li>
                            <a <?php echo colors('a');?> href="<?php echo of_get_option('sponsor_link'.$log);  ?>">
                                <img src="<?php echo of_get_option('sponsor_text'.$log); ?>" alt="<?php echo $log;?>" />
                            </a>
                        </li>
                        <?php
                                $c=$log%5;
                                if($c==0){
                                    echo $last1;
                                    echo $begin2;
                                }
                            } 
                        ?>
                    </div>
                    </div>
                </div>
            </div>
        </article>
        <?php } ?>
        <!-- end Bodybuilding Supplements --> 

    </section>
    <!-- end content -->

<?php
    get_footer();
?>