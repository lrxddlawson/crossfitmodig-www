<?php 
    get_header();
    the_post();
?>
    <!-- begin Content -->
    <section id="faq" >
        <!-- begin Sub Header -->
        <?php if(of_get_option('show_banner_faq')) { ?>
        <div class="sub-header" style="<?php echo 'background:url('.of_get_option('faq_banner_image').') no-repeat;'?>">
            <div class="container">
                <div class="row" >
                    <ul class="sub-header-container" >
                        <li>
                            <h3 <?php echo colors('h3');?> class="title"><?php echo of_get_option('faq_banner_text') ?></h3>
                        </li>
                        <li>
                            <?php if (of_get_option('show_breadcumbs_faq')) { ?>
                            <ul class="custom-breadcrumb" >
                                <li><h6 <?php echo colors('h6');?>><a <?php echo colors('a');?> href="<?php echo home_url();?>">Home</a></h6></li>
                                <li><i class="separator entypo-play" ></i></li>
                                <li><h6 <?php echo colors('h6');?>><a <?php echo colors('h6');?> href="#">Pages</a></h6></li>
                                <li><i class="separator entypo-play" ></i></li>
                                <li><h6 <?php echo colors('h6');?>>FAQs</h6></li>
                            </ul>                    
                            <?php  } ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <?php }?>
        <!-- begin Sub Header -->

        <!-- begin News -->
        <article class="article-container">
            <div class="container" >
                <div class="row" >
                <?php if(of_get_option('show_faq_acc')){?>
                    <!-- begin Main Colum -->
                    <div class="col-sm-9">
                        <h2 <?php echo colors('h2');?> class="article-title" ><?php echo of_get_option('faq_acc_title')?></h2>
                        <span <?php echo colors('h1s');?> class="line" >
                            <span <?php echo colors('h1s');?> class="sub-line" ></span>
                        </span>
                        
                        <section>
                        <!-- begin Accordion -->
                        <div class="accordion" id="accordion2">
                        <?php
                            $c = 0;
                            $numbers = array(1=>'One','Two','Three','Four','Five','Six','Seven','Height','Nine','Ten','Eleven','Twelve','Thirteen','Twenty','Thirty','Fourty'); 
                            for($i=1; $i<=of_get_option('faq_acc_num'); $i++){
                                $c++
                        ?>
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <h5 <?php echo colors('h5');?>><?php echo of_get_option('faq_acc_o_title'.$i);?></h5>
                                    <a <?php echo colors('a');?> class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse<?php echo $numbers[$c];?>">+</a>
                                </div>
                                <div id="collapse<?php echo $numbers[$c];?>" class="accordion-body collapse <?php if($c==1){echo 'in';}?>">
                                <div class="accordion-inner">
                                    <a <?php echo colors('a');?> class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse<?php echo $numbers[$c];?>">-</a>
                                    <h5 <?php echo colors('h5');?>><?php echo of_get_option('faq_acc_o_subtitle'.$i);?></h5>
                                    <p <?php echo colors('p');?>><?php echo insert_br(of_get_option('faq_acc_o_desc'.$i));?></p>
                                </div>
                                </div>
                            </div>
                        <?php }?>
                        </div>

                        </section>

                    </div>
                    <!-- end Main Colum -->
                    <?php }?>
                    
                    <!-- begin Sidebar -->
                    <aside class="col-sm-3 sidebar">
                        <?php if(of_get_option('show_zone1_r')){?>
                        <h3 <?php echo colors('h3');?> class="article-title" ><?php echo of_get_option('zone1_r_title');?></h3>
                        <span <?php echo colors('h1s');?> class="line" >
                            <span <?php echo colors('h1s');?> class="sub-line" ></span>
                        </span>
                        <ul class="custom-icon-list categories" >
                        <?php for($i=1; $i<=of_get_option('zone1_r_num'); $i++){?>
                            <li><i <?php echo font_awesome_icon_style('vgn_zone1');?> ></i>
                                <a <?php echo colors('a');?> href="<?php echo of_get_option('zone1_r_link'.$i);?>"><?php echo of_get_option('zone1_r_text'.$i);?>
                                    <span <?php echo colors('h1s');?> class="badge badge-gym pull-right">$<?php echo of_get_option('zone1_r_price'.$i);?></span>
                                </a></li>
                        <?php }?>
                        </ul>
                        <?php }?>
                        <?php if(of_get_option('show_zone2_r')){?>
                        <h3 <?php echo colors('h3');?> class="article-title" ><?php echo of_get_option('zone2_r_title');?></h3>
                        <span <?php echo colors('h1s');?> class="line" >
                            <span <?php echo colors('h1s');?> class="sub-line" ></span>
                        </span>

                        <ul class="custom-icon-list categories" >
                        <?php 
                            foreach(get_categories() as $cats){
                                if(of_get_option('zone2_r_cat'.$cats->slug) == 1){
                        ?>
                            <li><i <?php echo font_awesome_icon_style('vgn_zone2');?>></i>
                                <a <?php echo colors('a');?> href="#"><?php echo $cats->name;?></a>
                            </li>
                        <?php }}?>
                        </ul>
                        <?php }?>
                        <?php if(of_get_option('show_zone3_r')){?>
                        <h3 <?php echo colors('h3');?> class="article-title" ><?php echo of_get_option('zone3_r_title');?></h3>
                        <span <?php echo colors('h1s');?> class="line" >
                            <span <?php echo colors('h1s');?> class="sub-line" ></span>
                        </span>
                        <ul class="social" >
                        <?php for($i=1; $i<=of_get_option('zone3_r_num'); $i++){?>
                            <li><a <?php echo colors('a');?> href="<?php echo of_get_option('sl_zone3_r_url'.$i);?>"><i <?php echo font_awesome_icon_style('sl_zone3_r'.$i);?> ></i></a></li>
                        <?php }?>
                        </ul>
                        <?php }?>
                    </aside>
                    <!-- end Sidebar -->
                </div>
            </div>
        </article>
        <!-- end News -->

        <!-- begin Bodybuilding Supplements -->
         <?php if(of_get_option('show_sponsor_faq')) { ?>
        <article class="article-container">
            <div class="container" >

                <!-- arrows -->
                <div class="row" >
                    <div class="col-md-12">
                        <h2 <?php echo colors('h2');?> class="headers"><?php echo of_get_option('sponsor_title'); ?> </h2>
                        <span <?php echo colors('h1s');?> class="line" >
                            <span <?php echo colors('h1s');?> class="sub-line" ></span>
                        </span>
                        <?php if(of_get_option('sponsor_cant')>5) { ?>
                        <a <?php echo colors('a');?> class="slider-control pull-right next" href="#bodybuilding" data-slide="next"></a>
                        <a <?php echo colors('a');?> class="slider-control pull-right prev" href="#bodybuilding" data-slide="prev"></a>
                        <?php } ?>
                    </div>
                </div>
                <!-- end arrows -->

                <div class="row" >
                    <div id="bodybuilding" class="carousel slide">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <?php
                            $begin1='<div class="item active"><ul class="logos" >';
                            $last1 = '</ul></div>';
                            $begin2='<div class="item"><ul class="logos" >';
                        for ($log=1; $log <= of_get_option('sponsor_cant') ; $log++) {

                            if($log!=1)
                               {
                                $begin1 = '';
                               }
                            echo $begin1;
                             ?>
                                <li><a <?php echo colors('a');?> href="<?php echo of_get_option('sponsor_link'.$log);  ?>"><img src="<?php echo of_get_option('sponsor_text'.$log) ?>" alt="<?php echo $log;?>" /></a></li>
                            <?php
                            $c=$log%5;
                            if($c==0)
                            {
                            echo $last1;
                            echo $begin2;

                            }

                            } ?>
                    </div>
                    </div>
                </div>
            </div>
        </article>
        <?php } ?>
        <!-- end Bodybuilding Supplements -->
    </section>

    <!-- end content --><?php get_footer(); ?>